<?php

/*------------------------------------------------------------------------------------------------------------------------
------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';
require_once '_CoreConfig/config.php';
//
// require '_sysconfig/Router.php';
// require '_sysconfig/Appctrl.php';
// require '_sysconfig/View.php';
// require '_sysconfig/Safety_heaven.php';
// require '_sysconfig/SessionMGR.php';
// require '_sysconfig/Model.php';
// require '_sysconfig/MetaModel.php';
// require '_sysconfig/simple_html_dom.php';
spl_autoload_register(function($className){
   require_once '_sysconfig/' . $className . '.php';
});
require '_CoreConfig/db_access.php';
require '_SCLA/_scla_c_del.php';
require '_SCLA/_inv_lang.php';
require '_SCLA/_lang_trans.php';
require '_viewControllers/commonview.php';
SessionMGR::init();
$rout = new Router();
?>
