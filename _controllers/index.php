<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/

class Index extends Appctrl
{
  public $meta_desc;//="D E S C";
  public $meta_title;//="T I T L E";
  public $arr_meta=[];
  function __construct()
  {
    parent::__construct();
    $this->indexModel = $this->model('IndexModel');
    $this->arr_meta = $this->get_page_meta();
    //print_r($this->arr_meta);
    $obj = new StdClass;
    $obj = $this->get_page_meta();
    $this->meta_title = $obj[0]->page_title;
    $this->meta_desc = $obj[0]->page_description;
  }

  function index()
  {
    $this->view->render('index','index');
  }

  public function get_page_meta()
  {
     return $this->indexModel->page_meta_data();
  }

}
 ?>
