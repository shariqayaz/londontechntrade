<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/


class Login extends Appctrl
{
  
  private $conn;
  function __construct()
  {
    parent::__construct();
    SessionMGR::init();
  }

  public function runQuery($sql)
  {
    $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", DB_ACCESS::meta_user(), DB_ACCESS::meta_pass());
    $stmt = $conn_meta->prepare($sql);
    return $stmt;
  }

  function login($param)
  {
    if ($param=="sign-up")
    {
      $this->view->render('sign-up','login');
    }
    else if ($param=="login")
    {
      ///
      if(isset($_SESSION['user_session']))
      {
        //return true;
        header("Location: /recycle");
      }else{
        //session_destroy();
        $this->view->render('login','login');
        return false;
      }
    }
    else if ($param=="logout")
    {
      echo "here";
      $this->doLogout();
      $this->redirect('/recycle');
    }

  }
  
  public function register($uname,$fname,$umail,$upass,$address,$phone_no)
  {
    try
    {
      $new_password = password_hash($upass, PASSWORD_DEFAULT);

      $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", DB_ACCESS::meta_user(), DB_ACCESS::meta_pass());
      $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $stmt = $conn_meta->prepare("INSERT INTO tbl_users(user_name,full_name,user_email,user_pass,address,phone_no,isAdmin,isActive) VALUES(:uname, :fname, :umail, :upass, :address, :phone_no,0,0)");
      $stmt->bindparam(":uname", $uname);
      $stmt->bindparam(":fname", $fname);
      $stmt->bindparam(":umail", $umail);
      $stmt->bindparam(":address", $address);
      $stmt->bindparam(":phone_no", $phone_no);
      $stmt->bindparam(":upass", $new_password);
      $stmt->execute();
      return $stmt;
    }
    catch(PDOException $e)
    {
      echo $e->getMessage();
    }
  }

  public function doLogin($uname,$umail,$upass)
  {
    try
    {
      $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", DB_ACCESS::meta_user(), DB_ACCESS::meta_pass());
      $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $stmt = $conn_meta->prepare("SELECT user_id, user_name, user_email, user_pass FROM tbl_users WHERE user_name=:uname OR user_email=:umail ");
      $stmt->execute(array(':uname'=>$uname, ':umail'=>$umail));
      $userRow=$stmt->fetch(PDO::FETCH_ASSOC);
      if($stmt->rowCount() == 1)
      {
        if(password_verify($upass, $userRow['user_pass']))
        {
          SessionMGR::setWV('user_session',$userRow['user_id']);
          // $_SESSION['user_session'] = $userRow['user_id'];
          return true;
        }
        else
        {
          return false;
        }
      }
    }
    catch(PDOException $e)
    {
      echo $e->getMessage();
    }
  }

  public function is_loggedin()
  {
    return SessionMGR::chkLogin('user_session');
  }

  public function redirect($url)
  {
    header("Location: $url");
  }

  public function doLogout()
  {
    //session_destroy();
    unset($_SESSION['user_session']);
    return true;
  }

}

 ?>
