<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/


class Content extends Appctrl
{
  function __construct()
  {
    parent::__construct();
  }

  function index($param=false)
  {
    $arr_param = explode("^^",$param);
    $arr_param_count = count($arr_param);
    if ($arr_param_count == 1 && strtolower($arr_param[0])=="how-to")
      $this->view->render('how-to',$arr_param[0]);
    else if ($arr_param_count == 2)
      $this->view->render($arr_param[0],$arr_param[1]);
    else echo "ACTION_LESS_CONTENT_CTRL";
  }

}
 ?>
