<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/


class Thankyou extends Appctrl
{
  function __construct()
  {
    parent::__construct();
  }

  function index($param=false)
  {
    $arr_param = explode("^^",$param);
    $arr_param_count = count($arr_param);
    if ($arr_param_count == 1 && strtolower($arr_param[0])=="thankyou")
      $this->view->render('thankyou',$arr_param[0]);
    // else echo "ACTION_LESS_CONTENT_CTRL";
  }

  function selectuser()
  {
      $ctrl_code = 'getusers';
      Model::db_request($ctrl_code,"all");
    
  }

  
  public function register($uname,$fname,$umail,$upass,$address,$phone_no)
  {
    try
    {
      $new_password = password_hash($upass, PASSWORD_DEFAULT);

      $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", DB_ACCESS::meta_user(), DB_ACCESS::meta_pass());
      $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $stmt = $conn_meta->prepare("INSERT INTO tbl_users(user_name,full_name,user_email,user_pass,address,phone_no,isAdmin,isActive) VALUES(:uname, :fname, :umail, :upass, :address, :phone_no,0,0)");
      $stmt->bindparam(":uname", $uname);
      $stmt->bindparam(":fname", $fname);
      $stmt->bindparam(":umail", $umail);
      $stmt->bindparam(":address", $address);
      $stmt->bindparam(":phone_no", $phone_no);
      $stmt->bindparam(":upass", $new_password);
      $stmt->execute();
      return $stmt;
    }
    catch(PDOException $e)
    {
      echo $e->getMessage();
    }
  }

  public function redirect($url)
  {
    header("Location: $url");
  }

}
 ?>
