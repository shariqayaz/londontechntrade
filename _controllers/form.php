<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/

class Forms extends Appctrl
{
  function __construct()
  {
    parent::__construct();
  }

  function index($param=false)
  {
    $arr_param = explode("^^",$param);
    $arr_param_count = count($arr_param);
    //print_r($arr_param);echo $arr_param_count;

    if ($arr_param_count == 1 && strtolower($arr_param[0])=="form")
      echo "1_INVALID_REQUEST_ASK<br>kashan@londontt.com<br>_GO_BACK_<br>ACTION_LESS_FORM_CTRL";
    else if ($arr_param_count == 2 && strtolower($arr_param[0])=="form" && strtolower($arr_param[1])=="emptysellform")
      $this->view->render($arr_param[0],$arr_param[1]);
    else if ($arr_param_count == 2 && strtolower($arr_param[0])=="form" && strtolower($arr_param[1])=="emptyrepairform")
      $this->view->render($arr_param[0],$arr_param[1]);
    else if ($arr_param_count == 2 && strtolower($arr_param[0])=="form" && strtolower($arr_param[1])=="repairsimple")
      $this->view->render($arr_param[0],$arr_param[1]);
    else if ($arr_param_count == 2)
      $this->view->render($arr_param[0],$arr_param[1]);
    else if ($arr_param_count == 3)
      echo $this->FormSubmitAction($arr_param[1],$arr_param[2]);
    else if ($arr_param_count == 4 && strtolower($arr_param[0]) == "form" && strtolower($arr_param[1]) == "blank")
      echo $this->BlankFromSubmit($arr_param[1],$arr_param[2]);
    else if ($arr_param_count == 4 && strtolower($arr_param[0]) == "form" && strtolower($arr_param[1]) == "simple"  && strtolower($arr_param[2]) == "repair")
      echo $this->simpleFormSubmit($arr_param[1],$arr_param[2]);
    else echo $arr_param_count."_1_INVALID_REQUEST_ASK<br>kashan@londontt.com<br>_GO_BACK_<br>ACTION_LESS_FORM_CTRL";
  }

  private function simpleFormSubmit($param1=false,$param2=false)
  {
    $line = date('Y-m-d H:i:s');
    $line .= " - ";
    $line .= strval($_SERVER['REMOTE_ADDR']);
    $line .= " - ";
    $line .= strval($_SERVER['HTTP_USER_AGENT']);
    $line .= " - ";
    $line .= strval($_SERVER['HTTP_REFERER']);

    if (isset($_POST['repair_simple_submit']))
    {
      $problist = "";
      $probcount = count(@$_POST[checkedprob]);
      if ($probcount > 0 && $probcount != 0) {
        foreach (@$_POST[checkedprob] as $key => $value)
        {
          $problist .= $key. ' , ';
        }
      }
      //echo $problist;
      $get_name = $_POST['fullname'];
      $get_model = $_POST['model'];
      $get_email = $_POST['email'];
      $get_mobile = $_POST['mobile'];
      $get_message = $_POST['message'];

      require_once "_sysconfig/Safety_heaven.php";
      $shcall = new SafeHeaven();
      $get_name = $shcall->xss_clean($get_name);
      $get_model = $shcall->xss_clean($get_model);
      $get_email = $shcall->xss_clean($get_email);
      $get_mobile = $shcall->xss_clean($get_mobile);
      $problist = $shcall->xss_clean($problist);
      $get_message = $shcall->xss_clean($get_message);

      $data = $get_name.'-_-'.$get_model.'-_-'.$get_email.'-_-'.$get_mobile.'-_-'.$problist.'-_-'.$get_message.'-_-'.$line;
      $ctrl_code = 'simple_repair_quot';
      $dbreq = new Model();
      $bool = $dbreq->db_request($ctrl_code,$data);
      //
      require_once "_email/_send_email.php";
      $emailsender = new EmailSender();
      $emailsender->emailIT_simple_repair($get_name,$get_email,$get_mobile,$get_model,$problist,$get_message,$line);
      header('location: /');
    }
    else
    {
      echo "NOT_EXPECTED_50_F_CTR";
    }
  }


  private function BlankFromSubmit($param1=false,$param2=false)
  {
    $line = date('Y-m-d H:i:s');
    $line .= " - ";
    $line .= strval($_SERVER['REMOTE_ADDR']);
    $line .= " - ";
    $line .= strval($_SERVER['HTTP_USER_AGENT']);
    $line .= " - ";
    $line .= strval($_SERVER['HTTP_REFERER']);

    if (isset($_POST['submit_blank_repair']))
    {
      $get_name = $_POST['fullname'];
      $get_email = $_POST['email'];
      $get_mobile = $_POST['mobile'];
      $get_address = $_POST['address'];
      $get_brand = $_POST['brand'];
      $get_model = $_POST['model'];
      $get_category = $_POST['category'];
      $get_problem = $_POST['problem'];
      $get_message = $_POST['message'];

      require_once "_sysconfig/Safety_heaven.php";
      $shcall = new SafeHeaven();
      $get_name = $shcall->xss_clean($get_name);
      $get_email = $shcall->xss_clean($get_email);
      $get_mobile = $shcall->xss_clean($get_mobile);
      $get_address = $shcall->xss_clean($get_address);
      $get_brand = $shcall->xss_clean($get_brand);
      $get_model = $shcall->xss_clean($get_model);
      $get_category = $shcall->xss_clean($get_category);
      $get_problem = $shcall->xss_clean($get_problem);
      $get_message = $shcall->xss_clean($get_message);

      $data = $get_name.'-_-'.$get_email.'-_-'.$get_mobile.'-_-'.$get_address.'-_-'.$get_brand.'-_-'.$get_model.'-_-'.$get_category.'-_-'.$get_problem.'-_-'.$get_message.'-_-'.$line;

      $ctrl_code = 'blank_repair_quot';
      $dbreq = new Model();
      $bool = $dbreq->db_request($ctrl_code,$data);
      //
      require_once "_email/_send_email.php";
      $emailsender = new EmailSender();
      $emailsender->emailIT_blank_repair($get_name,$get_email,$get_mobile,$get_address,$get_brand,$get_model,$get_category,$get_problem,$get_message,$line);
      header('location: /');
    }
    else if (isset($_POST['submit_blank_sell']))
    {
      $get_name = $_POST['fullname'];
      $get_email = $_POST['email'];
      $get_mobile = $_POST['mobile'];
      $get_address = $_POST['address'];
      $get_brand = $_POST['brand'];
      $get_model = $_POST['model'];
      $get_category = $_POST['category'];
      $get_capacity = $_POST['capacity'];
      $get_condition = $_POST['condition'];
      $get_network = $_POST['network'];
      $get_color = $_POST['color'];
      $get_demand = $_POST['demand'];
      $get_message = $_POST['message'];

      require_once "_sysconfig/Safety_heaven.php";
      $shcall = new SafeHeaven();
      $get_name = $shcall->xss_clean($get_name);
      $get_email = $shcall->xss_clean($get_email);
      $get_mobile = $shcall->xss_clean($get_mobile);
      $get_address = $shcall->xss_clean($get_address);
      $get_brand = $shcall->xss_clean($get_brand);
      $get_model = $shcall->xss_clean($get_model);
      $get_category = $shcall->xss_clean($get_category);
      $get_capacity = $shcall->xss_clean($get_capacity);
      $get_condition = $shcall->xss_clean($get_condition);
      $get_network = $shcall->xss_clean($get_network);
      $get_color = $shcall->xss_clean($get_color);
      $get_demand = $shcall->xss_clean($get_demand);
      $get_message = $shcall->xss_clean($get_message);

      $data = $get_name.'-_-'.$get_email.'-_-'.$get_mobile.'-_-'.$get_address.'-_-'.$get_brand.'-_-'.$get_model.'-_-'.$get_category.'-_-'.$get_capacity.'-_-'.$get_condition.'-_-'.$get_network.'-_-'.$get_color.'-_-'.$get_demand.'-_-'.$get_message.'-_-'.$line;

      $ctrl_code = 'blank_sell_quot';
      $dbreq = new Model();
      $bool = $dbreq->db_request($ctrl_code,$data);

      require_once "_email/_send_email.php";
      $emailsender = new EmailSender();

      $emailsender->emailIT_blank_sell($get_name,$get_email,$get_mobile,$get_address,$get_brand,$get_model,$get_category,$get_capacity,$get_condition,$get_network,$get_color,$get_demand,$get_message,$line);
      header('location: /');
    }
  }

  private function FormSubmitAction($param1=false,$param2=false)
  {
    $line = date('Y-m-d H:i:s');
    $line .= " - ";
    $line .= strval($_SERVER['REMOTE_ADDR']);
    $line .= " - ";
    $line .= strval($_SERVER['HTTP_USER_AGENT']);
    $line .= " - ";
    $line .= strval($_SERVER['HTTP_REFERER']);

    if(isset($_POST['quickappsubmit']))
    {
      $get_name = $_POST['fullname'];
      $get_email = $_POST['email'];
      $get_mobile = $_POST['mobile'];
      $get_brand = $_POST['brand'];
      $get_model = $_POST['model'];
      $get_message = $_POST['message'];

      require_once "_sysconfig/Safety_heaven.php";
      $shcall = new SafeHeaven();
      $get_name = $shcall->xss_clean($get_name);
      $get_email = $shcall->xss_clean($get_email);
      $get_mobile = $shcall->xss_clean($get_mobile);
      $get_brand = $shcall->xss_clean($get_brand);
      $get_model = $shcall->xss_clean($get_model);
      $get_message = $shcall->xss_clean($get_message);

      require_once "_models/_form_submit.php";
      $sd = new submitForm('appointment',$get_name."^*^".$get_email."^*^".$get_mobile."^*^".$get_name."^*^".$get_email."^*^".$get_mobile."^*^".$get_message."^*^".$line);

      require_once "_email/_send_email.php";
      $emailsender = new EmailSender();
      $emailsender->emailIT($get_name,$get_email,$get_mobile,$get_brand,$get_model,$get_message);
      header('location: /');
    }
  }



}

 ?>
