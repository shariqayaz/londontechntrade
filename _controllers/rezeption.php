<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/

class Rezeption extends Appctrl
{

  function __construct()
  {
    parent::__construct();
  }

  function index($prepairedURL)
  {
    $geturl = explode('^^', $prepairedURL);
    echo "<font color=brown>rezeption.php</font><br>this is reception<br>";
    print_r($geturl);
    $_level_count_ = 0;
    $_level_count_ = count($geturl);

    // First Level statement
    if ($_level_count_==1)
    {
      $this->reception($_level_count_,$geturl[0]);
    }
    // Second Level statement
    else if ($_level_count_ == 2)
    {
      $this->reception($_level_count_,$geturl[0],$geturl[1]);
    }
    // Third Level statement
    else if ($_level_count_ == 3)
    {
      $this->reception($_level_count_,$geturl[0],$geturl[1],$geturl[2]);
    }
    // Forth Level statement
    else if ($_level_count_ == 4)
    {
      $this->reception($_level_count_,$geturl[0],$geturl[1],$geturl[2],$geturl[3]);
    }
  }

  function reception($lvlcount,$lvl1,$lvl2=false,$lvl3=false,$lvl4=false)
  {
    require_once 'profile.php';
    $prof_ile = new _Profile();
    $getlvlcount = $lvlcount;
    $ctrl_name = 'profile';
    // First Level Only
    if ($lvl1 == true && $lvl2 == false && $lvl3 == false && $lvl4 == false && $getlvlcount == 1)
    {
      $getlvl1 = $lvl1;
      echo "<font color=brown>Rezeption controller</font><br>Level One<br>";
      $prof_ile->index($ctrl_name,$getlvl1);
    }
    // Second Level Only
    else if ($lvl1 == true && $lvl2 == true && $lvl3 == false && $lvl4 == false && $getlvlcount == 2)
    {
      $getlvl1 = $lvl1;
      $getlvl2 = $lvl2;
      echo "<font color=brown>Rezeption controller</font><br>Level Two<br>";
      $prof_ile->index($ctrl_name,$getlvl1,$getlvl2);
    }
    // Third Level Only
    else if ($lvl1 == true && $lvl2 == true && $lvl3 == true && $lvl4 == false && $getlvlcount == 3)
    {
      $getlvl1 = $lvl1;
      $getlvl2 = $lvl2;
      $getlvl3 = $lvl3;
      echo "<font color=brown>Rezeption controller</font><br>Level Three<br>";
      $prof_ile->index($ctrl_name,$getlvl1,$getlvl2,$getlvl3);
    }
    // Fourth Level Only
    else if ($lvl1 == true && $lvl2 == true && $lvl3 == true && $lvl4 == true && $getlvlcount == 4)
    {
      $getlvl1 = $lvl1;
      $getlvl2 = $lvl2;
      $getlvl3 = $lvl3;
      $getlvl4 = $lvl4;
      echo "<font color=brown>Rezeption controller</font><br>Level Four<br>";
      $prof_ile->index($ctrl_name,$getlvl1,$getlvl2,$getlvl3,$getlvl4);
    }
  }

}
 ?>
