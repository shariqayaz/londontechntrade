<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/


class _Profile extends Appctrl
{
  function __construct()
  {
    parent::__construct();
    SessionMGR::init();
  }

  function index($ctrl_name,$getlvl1,$getlvl2=false,$getlvl3=false,$getlvl4=false)
  {
    // Rendering Profile , User Profile , City Page , Page , Contry Page, Community page
    $getctrl = $ctrl_name;
    // ONLY FOR LEVEL 1 CASE
    if ($getlvl1 == true && $getlvl2 == false && $getlvl3 == false && $getlvl4 == false)
    {
       $lvl1 = $getlvl1; // First URL Level -  May receive Shariq / Pakistan / Midas /
       $requestdata = $getctrl.'^^'.$lvl1;
       // UAANT COMPONENT START HERE
       $idpdata = SessionMGR::URDR($requestdata);echo "<br>";
       print_r($idpdata);echo "<br>";
       $casdata = SessionMGR::IDP($idpdata);echo "<br>";
       print_r($casdata);echo "<br>";
       $actionset = SessionMGR::CAS($casdata);echo "<br>";
       print_r($actionset);echo "<br>";
       // UAANT COMPONENT END HERE
       // INSTRUCTION START FROM HERE //
       $actionset_arr = explode('||',$actionset);
       $f_instruction = $actionset_arr[0];
       $f_what = $actionset_arr[1];
       $f_requeststr = $actionset_arr[2];
       // // ------------------// //
       if ($f_instruction=='RENDER')
       {
         $this->view->render($getctrl,$f_requeststr,($this->whatson('L1')));
       }
       else
       {
         // not render request if //
       }
       echo "<font color=brown>in controller profile</font><br>Level 1<br>".$lvl1." - ";
    }
    // ONLY FOR LEVEL 2 CASE
    else if ($getlvl1 == true && $getlvl2 == true && $getlvl3 == false && $getlvl4 == false)
    {
       $lvl1 = $getlvl1;
       $lvl2 = $getlvl2; // Second URL Level -  May receive Survey / Vote / Friends / Group / About / Video /
       $requestdata = $getctrl.'^^'.$lvl1.'^^'.$lvl2;
       $idpdata = SessionMGR::URDR($requestdata);echo "<br>";
       print_r($idpdata);echo "<br>";
       $casdata = SessionMGR::IDP($idpdata);echo "<br>";
       print_r($casdata);echo "<br>";
       $actionset = SessionMGR::CAS($casdata);echo "<br>";
       print_r($actionset);echo "<br>";
       // UAANT COMPONENT END HERE
       // INSTRUCTION START FROM HERE //
       $actionset_arr = explode('||',$actionset);
       $f_instruction = $actionset_arr[0];
       $f_what = $actionset_arr[1];
       $f_requeststr = $actionset_arr[2];
       // // ------------------// //
       if ($f_instruction=='RENDER')
       {
         $this->view->render($getctrl,$f_requeststr,$this->whatson('L2',$lvl2));
       }
       else
       {
         // not render request if //
       }
       echo "<font color=brown>in controller profile</font><br>Level 2<br>".$lvl1." -> ".$lvl2." - ";
    }
    // ONLY FOR LEVEL 3 CASE
    else if ($getlvl1 == true && $getlvl2 == true && $getlvl3 == true && $getlvl4 == false)
    {
       $lvl1 = $getlvl1;
       $lvl2 = $getlvl2;
       $lvl3 = $getlvl3;
       $requestdata = $getctrl.'^^'.$lvl1.'^^'.$lvl2.'^^'.$lvl3;
       $idpdata = SessionMGR::URDR($requestdata);echo "<br>";
       print_r($idpdata);echo "<br>";
       $casdata = SessionMGR::IDP($idpdata);echo "<br>";
       print_r($casdata);echo "<br>";
       $actionset = SessionMGR::CAS($casdata);echo "<br>";
       print_r($actionset);echo "<br>";
       // UAANT COMPONENT END HERE
       // INSTRUCTION START FROM HERE //
       $actionset_arr = explode('||',$actionset);
       $f_instruction = $actionset_arr[0];
       $f_what = $actionset_arr[1];
       $f_requeststr = $actionset_arr[2];
       // // ------------------// //
       if ($f_instruction=='RENDER')
       {
         $this->view->render($getctrl,$f_requeststr,$this->whatson('L3',$lvl2,$lvl3));
       }
       else
       {
         // not render request if //
       }
       echo "<font color=brown>in controller profile</font><br>Level 3<br>".$lvl1." -> ".$lvl2." -> ".$lvl3." - ";
    }
    // ONLY FOR LEVEL 4 CASE
    else if ($getlvl1 == true && $getlvl2 == true && $getlvl3 == true && $getlvl4 == true)
    {
       $lvl1 = $getlvl1;
       $lvl2 = $getlvl2;
       $lvl3 = $getlvl3;
       $lvl4 = $getlvl4;
       $requestdata = $getctrl.'^^'.$lvl1.'^^'.$lvl2.'^^'.$lvl3.'^^'.$lvl4;
       $idpdata = SessionMGR::URDR($requestdata);echo "<br>";
       print_r($idpdata);echo "<br>";
       $casdata = SessionMGR::IDP($idpdata);echo "<br>";
       print_r($casdata);echo "<br>";
       $actionset = SessionMGR::CAS($casdata);echo "<br>";
       print_r($actionset);echo "<br>";
       // UAANT COMPONENT END HERE
       // INSTRUCTION START FROM HERE //
       $actionset_arr = explode('||',$actionset);
       $f_instruction = $actionset_arr[0];
       $f_what = $actionset_arr[1];
       $f_requeststr = $actionset_arr[2];
       // // ------------------// //
       if ($f_instruction=='RENDER')
       {
         $this->view->render($getctrl,$f_requeststr,$this->whatson('L4',$lvl2,$lvl3,$lvl4));
       }
       else
       {
         // not render request if //
       }
       echo "<font color=brown>in controller profile</font><br>Level 4<br>".$lvl1." -> ".$lvl2." -> ".$lvl3." -> ".$lvl4." - ";
    }
  }

  function whatson($instlvl,$param1=false,$param2=false,$param3=false)
  {
    $getinstlvl = $instlvl;
    $shobj = new SafeHeaven();
    $param1 = $shobj->xss_clean($param1);
    $param2 = $shobj->xss_clean($param2);
    $param3 = $shobj->xss_clean($param3);
    if ($getinstlvl=='L1' & $param1==false & $param2==false & $param3==false)
    {
      return 'Main';
    }
    else if ($getinstlvl=='L2' & $param1==true)
    {
      return $param1;
    }
    else if ($getinstlvl=='L3' & $param1==true & $param2==true)
    {
      return $param1.'/_/'.$param2;
    }
    else if ($getinstlvl=='L4' & $param1==true & $param2==true & $param3==true)
    {
      return $param1.'/_/'.$param2.'/_/'.$param3;
    }
  }

}

?>
