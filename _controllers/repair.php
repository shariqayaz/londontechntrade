<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/


class Repairing extends Appctrl
{
  function __construct()
  {
    parent::__construct();
  }

  function index($param=false)
  {
    //echo $param;
    $arr_param = explode("^^",$param);
    $arr_param_count = count($arr_param);
    if ($arr_param_count == 1 && strtolower($arr_param[0])=="repair")
      $this->view->render('repair',$arr_param[0]);
    else if ($arr_param_count == 2 && strtolower($arr_param[0])=="repair" && strtolower($arr_param[1])=="appointment")
      $this->repairingAction($arr_param[1]);
    else if ($arr_param_count == 3 && strtolower($arr_param[0])=="repair" && strtolower($arr_param[1])=="appointment" && strtolower($arr_param[2])=="submit")
      $this->repairingAction($arr_param[1],$arr_param[2]);
    else if ($arr_param_count == 3 && strtolower($arr_param[0])=="repair" && strtolower($arr_param[1])=="phones")
      $this->view->render($arr_param[0],$arr_param[1],$arr_param[2]);
    else echo "ACTION_LESS_SUBCTRL_REPAIR";
  }

  private function repairingAction($app,$action=false){
    if (empty($action)) {
      echo "We're Working hard to finished that meanwhile you can contact us on  020 8691 7978 ".$app;
    }else {
    //    echo "ACTION not determined";
    }
  }

  public function loadproblem($what,$param)
  {
    if ($what == "allprob") {
      $shcall = new SafeHeaven();
      $what = $shcall->xss_clean($what);
      $param = $shcall->xss_clean($param);
    }
    //echo $what;
    //echo $param;

    if ($what=="allprob") {
      $ctrl_code = 'allprob';
      Model::db_request($ctrl_code,$param);

    }else if ($what=="else321") {
      $ctrl_code = 'else321';
      Model::db_request($ctrl_code,$param);
      // $frm->index('form^^emptyrepairform');
    }else if ($what=="FAVRecycle") {
      $ctrl_code = 'FAVRecycle';
      Model::db_request($ctrl_code,$param);
    }
  }

  public function loadsingle($param)
  {
    $shcall = new SafeHeaven();
    $param = $shcall->xss_clean($param);

    $ctrl_code = 'get_one';
    Model::db_request($ctrl_code,$param);
    // $frm->index('form^^emptysellform');
    ;
  }


  }
 ?>
