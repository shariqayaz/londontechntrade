<?php
/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.fb.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/

class Account extends Appctrl
{

  function __construct()
  {
    parent::__construct();

  }

  function index($param=false)
  {
    //echo $param;
    $arr_param = explode("^^",$param);
    $arr_param_count = count($arr_param);
  //echo $arr_param_count;
    if ($arr_param_count == 1 && strtolower($arr_param[0])=="account")
      $this->view->render($arr_param[0],$arr_param[0]);
    else if ($arr_param_count == 2 && strtolower($arr_param[0])=="account" && strtolower($arr_param[1])=="settings")
      $this->view->render($arr_param[0],$arr_param[1]);
    else if ($arr_param_count == 2 && strtolower($arr_param[0])=="account" && strtolower($arr_param[1])=="orders")
      $this->view->render($arr_param[0],$arr_param[1]);
    else if ($arr_param_count == 3 && $arr_param[1]=='products')
      $this->view->render($arr_param[0],$arr_param[1],$arr_param[2]);
    else if ($arr_param_count == 4 && $arr_param[1]=='det')
      $this->view->render($arr_param[0],$arr_param[1],$arr_param[2],$arr_param[3]);
    else if ($arr_param_count == 3 && $arr_param[1]=='submitprod')
      $this->update_prod($arr_param[2]);
    else if ($arr_param_count == 3 && $arr_param[1]=='capsubmit')
      $this->insert_newspec($arr_param[2]);
    else if ($arr_param_count == 4 && $arr_param[1]=='submitprod' && $arr_param[2]=='ind')
      $this->update_prodspec($arr_param[3]);
    else echo "ACTION_LESS_OADMIN_SUBCTRL";
  }

  function insert_newspec($param1)
  {
    if (isset($_POST['capsubmit'])) {
      $getcap = $_POST['cap'];
      $ctrlcode = "ins-new-cap";
      $insnewcap = new Model();
      $insnewcap->db_request($ctrlcode,$param1,$getcap);
      header('location: /occoto-admin/');
    }
  }

  function select_products($param1)
  {
    $selprod = new Model();
    $ctrl_code = "fetch";
    $itsretruned = $selprod->db_request($ctrl_code,$param1);
  }

  function select_product($param1)
  {
    //echo $param1;
    $selprod = new Model();
    $ctrl_code = "fetch-ind";
    $itsretruned = $selprod->db_request($ctrl_code,$param1);
  }

  function update_prod($param1)
  {
    if (isset($_POST['color_'.$param1]))
    {
      $get_id = $_POST['id_'.$param1];
      $get_color = $_POST['color_'.$param1];
      $get_mxp = $_POST['mxp_'.$param1];
      $get_img = $_POST['img_'.$param1];
      $get_fav = $_POST['fav_'.$param1];
      $get_prior = $_POST['prior_'.$param1];
      $get_active = $_POST['active_'.$param1];
      $datamerge = $get_color.'-_-'.$get_mxp.'-_-'.$get_img.'-_-'.$get_fav.'-_-'.$get_prior.'-_-'.$get_active;
      //  echo $datamerge;
      $updprod = new Model();
      $ctrl_code = "update_prod";
      $updprod->db_request($ctrl_code,$datamerge,str_replace('_',' ',$param1));
      header('location: /occoto-admin/det/apple/'.$get_id.'-_-'.str_replace('_',' ',$param1));
    }
  }

  function update_prodspec($param1)
  {
    if (isset($_POST['submit_'.$param1]))
    {
      $new_cond_desc = $_POST['new_cond_desc_'.$param1];
      $good_cond_desc = $_POST['good_cond_desc_'.$param1];
      $working_cond_desc = $_POST['working_cond_desc_'.$param1];
      $broken_cond_desc = $_POST['broken_cond_desc_'.$param1];
      $new_cond_unlocked = $_POST['new_cond_unlocked_'.$param1];
      $good_cond_unlocked = $_POST['good_cond_unlocked_'.$param1];
      $working_cond_unlocked = $_POST['working_cond_unlocked_'.$param1];
      $broken_cond_unlocked = $_POST['broken_cond_unlocked_'.$param1];
      $new_cond_o2 = $_POST['new_cond_o2_'.$param1];
      $good_cond_o2 = $_POST['good_cond_o2_'.$param1];
      $working_cond_o2 = $_POST['working_cond_o2_'.$param1];
      $broken_cond_o2 = $_POST['broken_cond_o2_'.$param1];
      $new_cond_o2 = $_POST['new_cond_o2_'.$param1];
      $good_cond_o2 = $_POST['good_cond_o2_'.$param1];
      $working_cond_o2 = $_POST['working_cond_o2_'.$param1];
      $broken_cond_o2 = $_POST['broken_cond_o2_'.$param1];
      $new_cond_ee = $_POST['new_cond_ee_'.$param1];
      $good_cond_ee = $_POST['good_cond_ee_'.$param1];
      $working_cond_ee = $_POST['working_cond_ee_'.$param1];
      $broken_cond_ee = $_POST['broken_cond_ee_'.$param1];
      $new_cond_vodafone = $_POST['new_cond_vodafone_'.$param1];
      $good_cond_vodafone = $_POST['good_cond_vodafone_'.$param1];
      $working_cond_vodafone = $_POST['working_cond_vodafone_'.$param1];
      $broken_cond_vodafone = $_POST['broken_cond_vodafone_'.$param1];
      $new_cond_orange = $_POST['new_cond_orange_'.$param1];
      $good_cond_orange = $_POST['good_cond_orange_'.$param1];
      $working_cond_orange = $_POST['working_cond_orange_'.$param1];
      $broken_cond_orange = $_POST['broken_cond_orange_'.$param1];
      $new_cond_tmobile = $_POST['new_cond_tmobile_'.$param1];
      $good_cond_tmobile = $_POST['good_cond_tmobile_'.$param1];
      $working_cond_tmobile = $_POST['working_cond_tmobile_'.$param1];
      $broken_cond_tmobile = $_POST['broken_cond_tmobile_'.$param1];
      $new_cond_3 = $_POST['new_cond_3_'.$param1];
      $good_cond_3 = $_POST['good_cond_3_'.$param1];
      $working_cond_3 = $_POST['working_cond_3_'.$param1];
      $broken_cond_3 = $_POST['broken_cond_3_'.$param1];
      $new_cond_virgin = $_POST['new_cond_virgin_'.$param1];
      $good_cond_virgin = $_POST['good_cond_virgin_'.$param1];
      $working_cond_virgin = $_POST['working_cond_virgin_'.$param1];
      $broken_cond_virgin = $_POST['broken_cond_virgin_'.$param1];
      $new_cond_tesco = $_POST['new_cond_tesco_'.$param1];
      $good_cond_tesco = $_POST['good_cond_tesco_'.$param1];
      $working_cond_tesco = $_POST['working_cond_tesco_'.$param1];
      $broken_cond_tesco = $_POST['broken_cond_tesco_'.$param1];
      $new_cond_unknown = $_POST['new_cond_unknown_'.$param1];
      $good_cond_unknown = $_POST['good_cond_unknown_'.$param1];
      $working_cond_unknown = $_POST['working_cond_unknown_'.$param1];
      $broken_cond_unknown = $_POST['broken_cond_unknown_'.$param1];

      $datamerge = $new_cond_desc."-_-".
      $good_cond_desc."-_-".
      $working_cond_desc."-_-".
      $broken_cond_desc."-_-".
      $new_cond_unlocked."-_-".
      $good_cond_unlocked."-_-".
      $working_cond_unlocked."-_-".
      $broken_cond_unlocked."-_-".
      $new_cond_o2."-_-".
      $good_cond_o2."-_-".
      $working_cond_o2."-_-".
      $broken_cond_o2."-_-".
      $new_cond_o2."-_-".
      $good_cond_o2."-_-".
      $working_cond_o2."-_-".
      $broken_cond_o2."-_-".
      $new_cond_ee."-_-".
      $good_cond_ee."-_-".
      $working_cond_ee."-_-".
      $broken_cond_ee."-_-".
      $new_cond_vodafone."-_-".
      $good_cond_vodafone."-_-".
      $working_cond_vodafone."-_-".
      $broken_cond_vodafone."-_-".
      $new_cond_orange."-_-".
      $good_cond_orange."-_-".
      $working_cond_orange."-_-".
      $broken_cond_orange."-_-".
      $new_cond_tmobile."-_-".
      $good_cond_tmobile."-_-".
      $working_cond_tmobile."-_-".
      $broken_cond_tmobile."-_-".
      $new_cond_3."-_-".
      $good_cond_3."-_-".
      $working_cond_3."-_-".
      $broken_cond_3."-_-".
      $new_cond_virgin."-_-".
      $good_cond_virgin."-_-".
      $working_cond_virgin."-_-".
      $broken_cond_virgin."-_-".
      $new_cond_tesco."-_-".
      $good_cond_tesco."-_-".
      $working_cond_tesco."-_-".
      $broken_cond_tesco."-_-".
      $new_cond_unknown."-_-".
      $good_cond_unknown."-_-".
      $working_cond_unknown."-_-".
      $broken_cond_unknown;
      $_SESSION['prod_spec_merge'] = $datamerge;
      $updprod = new Model();
      $ctrl_code = "update_prod_spec";
      $updprod->db_request($ctrl_code,$param1);
      $_SESSION['prod_spec_merge']='';
      header('location: /occoto-admin/');
    }
  }

  function info($p1)
  {
    if ($p1 == "phone") {
      $dbreq = new Model();
      $ctrl_code = 'admin';
      $ret = $dbreq->db_request($ctrl_code,$p1);
      echo $ret;
    }else if ($p1 == "address") {
      $dbreq = new Model();
      $ctrl_code = 'admin';
      $ret = $dbreq->db_request($ctrl_code,$p1);
      echo $ret;
    }else if ($p1 == "whatsapp") {
      $dbreq = new Model();
      $ctrl_code = 'admin';
      $ret = $dbreq->db_request($ctrl_code,$p1);
      echo $ret;
    }
    else if ($p1 == "email") {
      $dbreq = new Model();
      $ctrl_code = 'admin';
      $ret = $dbreq->db_request($ctrl_code,$p1);
      echo $ret;
    }
  }
}
 ?>
