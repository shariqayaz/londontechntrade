<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/


class Collections extends Appctrl
{

  function __construct()
  {
    parent::__construct();
  }

  function collections()
  {
    $this->view->render('recycle','recycle');
  }
}
 ?>
