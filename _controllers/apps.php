<?php
/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/

class Apps extends Appctrl
{

  function __construct()
  {
    parent::__construct();
  }

  function index($param=false)
  {
    $arr_param = explode("^^",$param);
    $arr_param_count = count($arr_param);
    if ($arr_param_count == 3)
      $this->view->render($arr_param[0],$arr_param[1],$arr_param[2]);
    else if ($arr_param_count == 4)
      //echo "ACTION_NOT_DETERMINE_APP_GETSTUFF";//$this->view->render($arr_param[0],$arr_param[1],$arr_param[2]);
      $this->view->render($arr_param[0],$arr_param[1],$arr_param[2],$arr_param[3]);
    else echo "ACTION_LESS_APP_SUBCTRL";
  }

  public function livesearch($what,$param) //
  {
    if ($what == "FAVRecycle") {
      $shcall = new SafeHeaven();
      $what = $shcall->xss_clean($what);
      $param = $shcall->xss_clean($param);
    }

    require 'form.php';
    $frm = new Forms();
    //echo $what." ".$param;
    if ($what=="recycle") {
      $ctrl_code = 'LRRecycle';
      Model::db_request($ctrl_code,$param);
      $frm->index('form^^emptysellform');
      exit;
    }else if ($what=="repair") {
      $ctrl_code = 'LRRepair';
      Model::db_request($ctrl_code,$param);
      $frm->index('form^^emptyrepairform');
    }else if ($what=="FAVRecycle") {
      $ctrl_code = 'FAVRecycle';
      Model::db_request($ctrl_code,$param);
    }

  }
}
 ?>
