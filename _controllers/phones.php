<?php
/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/
class phones extends Appctrl
{
public $cartid="";
  function __construct()
  {
    parent::__construct();
    $this->abc = new SafeHeaven();
    $this->getcolors = $this->model('Phone');
    $this->getprice = $this->model('Phone');
    $this->getnetworks = $this->model('Phone');
    $this->getcapacity = $this->model('Phone');
    $this->getcondition = $this->model('Phone');
    $this->getCondDesc = $this->model('Phone');
    $this->getOval = $this->model('Phone');
    //echo " my cart id is ".$this->cartid;
  }

  function index($param=false)
  {
    //echo $param;
    $arr_param = explode("^^",$param);
    $arr_param_count = count($arr_param);
    //echo $arr_param_count;
    if ($arr_param_count == 2 && strtolower($arr_param[0])=="phones")
      $this->view->render('phones',$arr_param[1]);
    else if ($arr_param_count == 3 && strtolower($arr_param[2])=="submitit")
      $this->view->render('phones/submit',$arr_param[2]);
    else if ($arr_param_count == 4 && strtolower($arr_param[1])=="conddesc")
    {
      //  echo '1'.$arr_param[0].' '.'2'.$arr_param[1].' '.'3'.$arr_param[2].' '.'4'.$arr_param[3];
      $obj1 = new StdClass;
      $obj1 = $this->get_conddesc($arr_param[2],$arr_param[3]);
      foreach ($obj1 as $intIndex => $objRecord1){
        echo $obj1->description1;
      }
    }
    else if ($arr_param_count == 3 && strtolower($arr_param[1])=="getoval")
    {
      //echo '1'.$arr_param[0].' '.'2'.$arr_param[1].' '.'3'.$arr_param[2].' '.'4'.$arr_param[3];
      $splittoArr = explode('-_-',$arr_param[2]);
       // $arr_param[2] = prod id, capacity id, condition id, network id
      $obj1 = new StdClass;
      $obj1 = $this->get_price($splittoArr[0],$splittoArr[1],$splittoArr[2],$splittoArr[3]);
      if (empty($obj1)) {
        echo "0.00";
        echo "<input type='text' name='offerprice' value='0.00'>";
        echo "<input type='hidden' name='spid' value='' required>";
      }
      else {
      if (isset($obj1)) {
        foreach ($obj1 as $intIndex => $objRecord1) {
          echo $objRecord1->offer_price;
          echo "<input type='text' name='offerprice' value='".$objRecord1->offer_price."'>";
          echo "<input type='hidden' name='spid' value='".$objRecord1->rec_cart_id."'>";
        }
      }else {
        echo "0.00";
      }
      }
    }
    else if ($arr_param_count == 3 && strtolower($arr_param[1])=="cartadd")
    {
      $this->addCart();
    }
    else echo "ACTION_LESS_PHONES_SUBCTRL";

    //var_dump($this->getCondDesc);
  }

  public function addCart()
  {
    if($_SERVER['REQUEST_METHOD']=='POST'){
    // Sanitize POST Array
    $_POST = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);
    if(isset($_POST['submit']))
    {
        if (empty($_POST['spid'])){
          // INSERT ZERO RATE into DB and forward user to calback function
          unset($_POST['prodid']);
          unset($_POST['model']);
          unset($_POST['condradio']);
          unset($_POST['networkradio']);
          unset($_POST['color']);
          header('location: '.$_SERVER[HTTP_REFERER]);
        }
        else
        {

          $get_pid = $_POST['prodid'];
          $get_model = $_POST['model'];
          $get_cap = $_POST['capradio'];
          $get_con = $_POST['condradio'];
          $get_network = $_POST['networkradio'];
          $get_color = $_POST['color'];
          $get_spid = $_POST['spid'];
          $getofferprice = $_POST['offerprice'];
          $getspecid = $_POST['prodid'];
          //
          $shcall = new SafeHeaven();
          $prodid = $shcall->xss_clean($getspecid);
          $get_model = $shcall->xss_clean($get_model);
          $get_cap = $shcall->xss_clean($get_cap);
          $get_con = $shcall->xss_clean($get_con);
          $get_network = $shcall->xss_clean($get_network);
          $get_color = $shcall->xss_clean($get_color);
          $getofferprice = $shcall->xss_clean($getofferprice);
          $getspecid = $shcall->xss_clean($getspecid);
          // prepare data to insert into session
          $newProduct = array(
            'user_id' => $user_id,
            'product_id' => $get_pid,
            'model' => $get_model,
            'cap' => $get_cap,
            'con' => $get_con,
            'net' => $get_network,
            'col' => $get_color,
            'price' => floatval($getofferprice),
            'qty' =>1
           );
           $_SESSION['sell_cart'][]= $newProduct;
           unset($_POST['submit']);
           header('location: /ship-mode');
          //array_push($_SESSION['sell_cart'],$get_color,$get_network,$get_model);
          // carry forward to shipment method
        }
      }
      else if(isset($_POST['addProd']))
      {
        if (empty($_POST['spid'])){
          // INSERT ZERO RATE into DB and forward user to calback function
          unset($_POST['prodid']);
          unset($_POST['model']);
          unset($_POST['condradio']);
          unset($_POST['networkradio']);
          unset($_POST['color']);
          header('location: '.$_SERVER[HTTP_REFERER]);
        }
        else
        {
          $get_pid = $_POST['prodid'];
          $get_model = $_POST['model'];
          $get_cap = $_POST['capradio'];
          $get_con = $_POST['condradio'];
          $get_network = $_POST['networkradio'];
          $get_color = $_POST['color'];
          $get_spid = $_POST['spid'];
          $getofferprice = $_POST['offerprice'];
          $getspecid = $_POST['prodid'];
          //
          $shcall = new SafeHeaven();
          $prodid = $shcall->xss_clean($getspecid);
          $get_model = $shcall->xss_clean($get_model);
          $get_cap = $shcall->xss_clean($get_cap);
          $get_con = $shcall->xss_clean($get_con);
          $get_network = $shcall->xss_clean($get_network);
          $get_color = $shcall->xss_clean($get_color);
          $getofferprice = $shcall->xss_clean($getofferprice);
          $getspecid = $shcall->xss_clean($getspecid);
          // prepare data to insert into session
          $newProduct = array(
            'user_id' => $user_id,
            'product_id' => $get_pid,
            'model' => $get_model,
            'cap' => $get_cap,
            'con' => $get_con,
            'net' => $get_network,
            'col' => $get_color,
            'price' => floatval($getofferprice),
            'qty' =>1
           );
           $_SESSION['sell_cart'][]= $newProduct;
           unset($_POST['prodid']);
           header('location: /recycle');
          //array_push($_SESSION['sell_cart'],$get_color,$get_network,$get_model);
          // carry forward to shipment method
        }
      }
    }
  }

  public function get_colors($param=false)
  {
    $param = $this->abc->xss_clean($param);
    return $this->getcolors->getprodcolors($param);
  }

  public function get_networks($param)
  {
    $param = $this->abc->xss_clean($param);
    return $this->getnetworks->getAvailableNetwork($param);
  }

  public function get_price($prodid,$capid,$condid,$networkid,$colid=false)
  {
    $prodid=$this->abc->xss_clean($prodid);
    $capid=$this->abc->xss_clean($capid);
    $condid=$this->abc->xss_clean($condid);
    $networkid=$this->abc->xss_clean($networkid);
    $colid=$this->abc->xss_clean($colid);
    return $this->getOval->getprice($prodid,$capid,$condid,$networkid);
  }

  public function get_condition($param)
  {
    $param = $this->abc->xss_clean($param);
    return $this->getcondition->getAvailableCondition($param);
  }

  public function get_capacity($param)
  {
    $param = $this->abc->xss_clean($param);
    return $this->getcondition->getAvailableCapacity($param);
  }

  public function get_conddesc($param,$param2)
  {
    $param = $this->abc->xss_clean($param);
    $param2 = $this->abc->xss_clean($param2);
    return $this->getCondDesc->getConditionDescription($param,$param2);
  }

}

?>
