<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/


class Finished extends Appctrl
{
  function __construct()
  {
    parent::__construct();
  }

  function index($param=false)
  {
    $arr_param = explode("^^",$param);
    $arr_param_count = count($arr_param);
    if ($arr_param_count == 1 && strtolower($arr_param[0])=="finished")
      $this->view->render('finished',$arr_param[0]);
    else echo "ACTION_LESS_FINISH_SUBCTRL";
  }

}
 ?>
