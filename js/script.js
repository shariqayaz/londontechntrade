function showUser(str) {
  if (str == "") {
    document.getElementById(str).innerHTML = "";
    return;
  } else {
    if (window.XMLHttpRequest) {
      // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else {
      // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById('apple').innerHTML = this.responseText;
      }
    };
    xmlhttp.open("GET", "/tabload/" + str + "", true);
    xmlhttp.send();
  }
}
//
$(document).ready(function () {
  $('.carousel').carousel({
    interval: 2000
  })
});
//
function recyclesearchResult(str) {
  str = str.replace(/#/g, '');
  str = str.replace(/</g, '');
  str = str.replace(/>/g, '');
  str = str.replace(/!/g, '');
  str = str.replace(/~/g, '');
  str = str.replace(/^/g, '');
  str = str.replace(/%/g, '');
  //alert('repair '+str);
  if (str.length == 0) {
    document.getElementById("livesearchrecycle").innerHTML = "";
    document.getElementById("livesearchrecycle").style.border = "0px";
    return;
  }

  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("livesearchrecycle").innerHTML = this.responseText;
      document.getElementById("livesearchrecycle").style.border = "1px solid white";
    }
  }
  xmlhttp.open("GET", "/app/recycle/search/" + str, true);
  //alert(xmlhttp);
  xmlhttp.send();
}

function repairsearchResult(str) {
  str = str.replace(/#/g, '');
  str = str.replace(/</g, '');
  str = str.replace(/>/g, '');
  str = str.replace(/!/g, '');
  str = str.replace(/~/g, '');
  str = str.replace(/^/g, '');
  str = str.replace(/%/g, '');
  //alert('repair '+str);
  if (str.length == 0) {
    document.getElementById("livesearchrepair").innerHTML = "";
    document.getElementById("livesearchrepair").style.border = "0px";
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("livesearchrepair").innerHTML = this.responseText;
      document.getElementById("livesearchrepair").style.border = "1px solid white";
    }
  }
  xmlhttp.open("GET", "/app/repair/search/" + str, true);
  xmlhttp.send();
}
//
var strCond;
var strCap;
var strNet;

function getCheckedValue() {
  var radioCond = document.getElementsByName('condradio');
  var radioCap = document.getElementsByName('capradio');
  var radioNetwork = document.getElementsByName('networkradio');
  var prodid = document.getElementsByName('prodid');
  var strprodid = (prodid[0].value);

  // Conditions Check
  for (i = 0; i < radioCond.length; i++) {
    if (radioCond[i].checked) {
      strCond = (radioCond[i].value);
    }
  }
  // Capacity check
  for (i = 0; i < radioCap.length; i++) {
    if (radioCap[i].checked) {
      strCap = (radioCap[i].value);
    }
  }
  // Network check
  for (i = 0; i < radioNetwork.length; i++) {
    if (radioNetwork[i].checked) {
      strNet = (radioNetwork[i].value);
    }
  }
  //  Capacity Condition network
  var str = strprodid + "-_-" + strCap + "-_-" + strCond + "-_-" + strNet;
  //alert(str);
  if (str == "") {
    document.getElementById(str).innerHTML = "";
    return;
  } else {
    if (window.XMLHttpRequest) {
      // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else {
      // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById('price').innerHTML = this.responseText;
      }
    };
    xmlhttp.open("GET", "/phones/getoval/" + str + "", true);
    xmlhttp.send();
  }
}

function getProbs() {
  var strCond;
  var radioCond = document.getElementsByName('condradio');
  var productid = document.getElementsByName('prodid');

  //var productid = ();
  // Conditions Check
  for (i = 0; i < radioCond.length; i++) {
    if (radioCond[i].checked) {
      strCond = (radioCond[i].value);
    }
  }

  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
  } else {
    // code for IE6, IE5
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById('condition_desc').innerHTML = this.responseText;
    }
  };
  //alert(strCond+productid[0].value);
  xmlhttp.open("GET", "/phones/condDesc/" + productid[0].value + "/" + strCond, true);
  xmlhttp.send();
}