<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/


class SCLACD extends Appctrl
{

  public function __construct()
  {
    parent::__construct();

    $langcode = ENV_lang::langret();
    // Create display layout order into Session
    if ($langcode=='ur' || $langcode=='fa' || $langcode=='ar')
    {
      $setsess = new SessionMGR;
      $setsess->setWV('lout','RTL');
    } else {
      $setsess = new SessionMGR;
      $setsess->setWV('lout','LTR');
    }
  }

  public function cdel($cname)
  {
    // get language code // fix length of language code otherwise force fully set it with "en"
    $langcode = ENV_lang::langret();
    // get data against key from langcode filter :)
    $returntrans = SCLACD::trans_lang($langcode,$cname);
    return $returntrans;
  }

  private static function trans_lang($lcode,$cname)
  {
    $getlcode = $lcode;
    $getcname = $cname;
    $transarg = new Lang_Translator();
    $transcontent = $transarg->trans_ret($getlcode,$getcname);
    return $transcontent;
  }
}

//
 ?>
