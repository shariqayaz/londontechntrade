<?php
/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/


class ENV_lang extends SCLACD
{
  public $langcode = '';
  public function __construct()
  {
    parent::__construct();
    $langcode = ENV_lang::langret();
  }

  // get default language or get language from cookies
  public function langret()
  {
    $cookie_name = 'lang';
    if(!isset($_COOKIE[$cookie_name])) {
    $baslang =substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
      if ($baslang=='zh' ||  $baslang=='hi' ||  $baslang=='en' ||  $baslang=='ur' ||  $baslang=='fr' ||  $baslang=='ar' ||  $baslang=='bn' ||  $baslang=='ja'
       ||  $baslang=='ko' ||  $baslang=='pl' ||  $baslang=='sp' ||  $baslang=='ru' ||  $baslang=='fa' ||  $baslang=='tr' ||  $baslang=='nl' ||  $baslang=='it' ||  $baslang=='pt'
        ||  $baslang=='sv' ||  $baslang=='id' ||  $baslang=='de') // || strlen($baslang)<3
      {
        $cookie_value = $baslang;
        // echo "<br>set from available language<br>$cookie_value";
        setcookie($cookie_name, $cookie_value, time() + (86400 * 30), '/'); // 86400 = 1 day
      }
      else
      {
        echo "other language";
        $baslang = 'en';
        $cookie_value = $baslang;
        setcookie($cookie_name, $cookie_value, time() + (86400 * 30), '/'); // 86400 = 1 day
      }
    } else {
      $baslang = $_COOKIE[$cookie_name];
      // echo "passed<br>";
      // echo $baslang;
    }
    return $baslang;
  }
}
?>
