<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/


/**
 *  META DATABASE REFLECTION - DATA REFLECTION
 */
class Lang_Translator extends DB_ACCESS
{
  public static function trans_ret($syslang,$cname)
  {
    $getsyslang = $syslang;
    $getcname = $cname;
    //
    $user_id = DB_ACCESS::scla_user();
    $user_pass = DB_ACCESS::scla_pass();
    try
    {
      $conn_meta = new PDO("mysql:host=192.168.0.152;dbname=scladb;charset=UTF8", "$user_id", "$user_pass");
      $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $st_meta = $conn_meta->prepare("SELECT col_".$getsyslang." from `scla_tbl` WHERE `cname` = :name");
      $st_meta->bindParam(':name', $getcname);
      $st_meta->execute();
      $rows = $st_meta->fetch(PDO::FETCH_ASSOC);
      // send true / false
      if ($st_meta->rowCount() > 0)
      {
        //echo $rows['col_'.$getsyslang];
        return $rows['col_'.$getsyslang];;
      }
      else
      {
        //echo $rows['col_'.$getsyslang];
        echo "not found content in language archive";
        //return $rows['col_'.$getsyslang];;

      }
    }
    catch (PDOException $e)
    {
      throw new Exception('Could not connect to database');
      echo 'Connection failed: ' . $e->getMessage(); // SHOWING DATABASE NAME
    }
  }
}
 ?>
