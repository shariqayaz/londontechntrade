<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/


class EmailSender
{

  public function emailIT($get_name,$get_email,$get_mobile,$get_brand,$get_model,$get_message)
  {
    require_once '_controllers/occotoadmin.php';
    $subject = "Quick Appointment for repairing | unlocking | LONDON TECHNTRADE";

    $message = "
    <html>
    <head>
    <title>Quick Appointment</title>
    </head>
    <body style='font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',sans-serif;'>
      <div style='text-align:center;margin:0px'>
        <font style='font-size: 22px;padding-left:0px;padding-right:0px;font-weight:bold;margin-right:0px;color:#1655a6'>
          LONDON
        </font>
        <font style='font-size: 22px;padding-left:5px;padding-right:5px;font-weight:bold;margin:0px;color:white;background:#1655a6'>
          TECHNTRADE
        </font>
      </div>
    <h2 style='width:100%;background:#1655a6;padding:5px;margin-botton:-10px;text-transform:uppercase;text-align:center;font-weight:bold;color:black'>REPAIR enquiry</h2>

    <b>Dear esteemed customer,</b><br><br>

    Thank you for your enquiry.<br><br>

    Our representative will catch you shortly.<br><br>

    However, if your inquiry needs urgent attention, you can contact our 24/7 customer care on ".OccotoAdmin::info('whatsapp')." or Dial:02086917978.<br><br><br>

    <b>Best Regards,</b><br>
    Kashan<br><br>

    <b>Manager</b><br>Customer Care department <br><br>
    <U>LONDON TECHNTRADE</U><br>113 Loampit Vale, London SE13 7TG, UK <br>
    STORE TIMING 9AM - 10PM
    <br>
    <br><hr>";
    $message.= "<h1 class='heading main'><span>Your given contact details</span></h1><table id='customers'>
    <tr>
      <th>Name</th>
      <th>Email</th>
      <th>Mobile</th>
      <th>Brand</th>
      <th>Model</th>
      <th>Message</th>
    <tr>
    <tr>
      <td>".$get_name."</td>
      <td>".$get_email."</td>
      <td>".$get_mobile."</td>
      <td>".$get_brand."</td>
      <td>".$get_model."</td>
      <td>".$get_message."</td>
    <tr>
    </table><br><hr>
    <h5>We will contact you shortly.<br><br>Thank you for choosing us</h5>
    <br>
    <img alt='LONDON Techntrade' src='http://londontt.com/wp-content/themes/fixit/assets/phone-repair/images/logo.png'>
    <h4 style='background:;padding:5px;margin-botton:-10px;text-transform:uppercase;text-align:left'>store time 9AM - 10PM<br>Need help? - 020 8691 7978</h4>
    <br><h5>this message generated from http://londontt.com/</h5>
    </body>
    </html>";

    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'p3plcpnl0143.prod.phx3.secureserver.net';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'noreply@londontt.com';                 // SMTP username
        $mail->Password = 'Extream_ds_365';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('londontechntrade@gmail.com', 'Kashan London Techntrade');
        $mail->addAddress($get_email, $get_name);     // Add a recipient
        //$mail->addAddress('ellen@example.com');               // Name is optional
        $mail->addReplyTo('londontechntrade@gmail.com', 'Kashan');
        //$mail->addCC('gr8shariq@gmail.com');
        $mail->addBCC('londontechntrade@gmail.com');

        // //Attachments
        // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $message;
        $mail->AltBody = $message;

        $mail->send();
        echo 'Message has been sent';
        header('location: /');
    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
  }

  public function emailIT_blank_sell($get_name,$get_email,$get_mobile,$get_address,$get_brand,$get_model,$get_category,$get_capacity,$get_condition,$get_network,$get_color,$get_demand,$get_message,$line)
  {
    require_once '_controllers/occotoadmin.php';
    $subject = "Quick Appointment for selling | recycling | LONDON TECHNTRADE";

    $message = "
    <html>
    <head>
    <title>QUOT REQUEST</title>
    </head>
    <body style='font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',sans-serif;'>
      <div style='text-align:center;margin:0px'>
        <font style='font-size: 22px;padding-left:0px;padding-right:0px;font-weight:bold;margin-right:0px;color:#1655a6'>
          LONDON
        </font>
        <font style='font-size: 22px;padding-left:5px;padding-right:5px;font-weight:bold;margin:0px;color:white;background:#1655a6'>
          TECHNTRADE
        </font>
      </div>
    <h2 style='width:100%;background:#1655a6;padding:5px;margin-botton:-10px;text-transform:uppercase;text-align:center;font-weight:bold;color:black'>REPAIR enquiry</h2>

    <b>Dear esteemed customer,</b><br><br>

    Thank you for your enquiry.<br><br>

    Our representative will catch you shortly.<br><br>

    However, if your inquiry needs urgent attention, you can contact our 24/7 customer care on ".OccotoAdmin::info('whatsapp')." or Dial:02086917978.<br><br><br>

    <b>Best Regards,</b><br>
    Kashan<br><br>

    <b>Manager</b><br>Customer Care department <br><br>
    <U>LONDON TECHNTRADE</U><br>113 Loampit Vale, London SE13 7TG, UK <br>
    STORE TIMING 9AM - 10PM
    <br>
    <br><hr>";
    $message.= "<h1 class='heading main'><span>Your given contact details</span></h1><table id='customers'>
    <tr>
      <th>Name</th>
      <th>Email</th>
      <th>Mobile</th>
      <th>Brand</th>
      <th>Model</th>
      <th>Message</th>
    <tr>
    <tr>
      <td>".$get_name."</td>
      <td>".$get_email."</td>
      <td>".$get_mobile."</td>
      <td>".$get_brand."</td>
      <td>".$get_model."</td>
      <td>".$get_message."</td>
    <tr>
    </table><br><hr>
    Brand: ".$get_brand."<br>Model: ".$get_model."<br>Category: ".$get_category."<br>Capacity: ".$get_capacity."<br>Condition: ".$get_condition."<br>Network: ".$get_network."<br>Color: ".$get_color."<br>Demand: ".$get_demand."<br>Message: ".$get_message."
    <h5>We will contact you shortly.<br><br>Thank you for choosing us</h5>
    <br>
    <img alt='LONDON Techntrade' src='http://londontt.com/wp-content/themes/fixit/assets/phone-repair/images/logo.png'>
    <h4 style='background:;padding:5px;margin-botton:-10px;text-transform:uppercase;text-align:left'>store time 9AM - 10PM<br>Need help? - 020 8691 7978</h4>
    <br><h5>this message generated from http://londontt.com/</h5>
    </body>
    </html>";

    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'p3plcpnl0143.prod.phx3.secureserver.net';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'noreply@londontt.com';                 // SMTP username
        $mail->Password = 'Extream_ds_365';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('londontechntrade@gmail.com', 'Kashan London Techntrade');
        $mail->addAddress($get_email, $get_name);     // Add a recipient
        //$mail->addAddress('ellen@example.com');               // Name is optional
        $mail->addReplyTo('londontechntrade@gmail.com', 'Kashan');
        //$mail->addCC('gr8shariq@gmail.com');
        $mail->addBCC('londontechntrade@gmail.com');

        // //Attachments
        // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $message;
        $mail->AltBody = $message;

        $mail->send();
        echo 'Message has been sent';
        header('location: /');
    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
  }

  public function emailIT_simple_repair($get_name,$get_email,$get_mobile,$get_model,$problist,$get_message,$line)
  {
    require_once '_controllers/occotoadmin.php';
    $subject .= "Repair Enquiry for ".$get_model;

    $message.= "
    <html>
      <head>
        <meta charset='utf-8'>
        <title>LONDON TECHNTRADE:Repair Enquiry for ".$get_model."</title>
      </head>

    <body style='font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',sans-serif;'>
      <div style='text-align:center;margin:0px'>
        <font style='font-size: 22px;padding-left:0px;padding-right:0px;font-weight:bold;margin-right:0px;color:#1655a6'>
          LONDON
        </font>
        <font style='font-size: 22px;padding-left:5px;padding-right:5px;font-weight:bold;margin:0px;color:white;background:#1655a6'>
          TECHNTRADE
        </font>
      </div>
    <h2 style='width:100%;background:#1655a6;padding:5px;margin-botton:-10px;text-transform:uppercase;text-align:center;font-weight:bold;color:black'>REPAIR enquiry</h2>

    <b>Dear esteemed customer,</b><br><br>

    Thank you for your enquiry.<br><br>

    Our representative will catch you shortly.<br><br>

    However, if your inquiry needs urgent attention, you can contact our 24/7 customer care on ".OccotoAdmin::info('whatsapp')." or Dial:02086917978.<br><br><br>

    <b>Best Regards,</b><br>
    Kashan<br><br>

    <b>Manager</b><br>Customer Care department <br><br>
    <U>LONDON TECHNTRADE</U><br>113 Loampit Vale, London SE13 7TG, UK <br>
    STORE TIMING 9AM - 10PM
    <br>
    <br><hr>
    <h4>Your Enquiry Details</h4>
    <table id='customers'>
    <tr>
      <th>Name</th>
      <th>Email</th>
      <th>Mobile</th>
      <th>Model</th>
      <th>Message</th>
    <tr>
    <tr>
      <td>".$get_name."</td>
      <td>".$get_email."</td>
      <td>".$get_mobile."</td>
      <td>".$get_model."</td>
      <td>".$get_message."</td>
    <tr>
    </table><br><br>
    <div style='text-align:center'>
    <h4>problem you marked</h4></div>
    <div style='text-align:center;background:lightsilver;color:black'>
    ".$problist."
    </div>

    <hr><h5>this message generated from http://londontt.com/repair/phones/".$get_model."</h5>
    </body>
    </html>";


    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'p3plcpnl0143.prod.phx3.secureserver.net';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'noreply@londontt.com';                 // SMTP username
        $mail->Password = 'Extream_ds_365';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('londontechntrade@gmail.com', 'Kashan London Techntrade');
        $mail->addAddress($get_email, $get_name);     // Add a recipient
        //$mail->addAddress('ellen@example.com');               // Name is optional
        $mail->addReplyTo('londontechntrade@gmail.com', 'Kashan');
        //$mail->addCC('gr8shariq@gmail.com');
        $mail->addBCC('londontechntrade@gmail.com');

        // //Attachments
        // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $message;
        $mail->AltBody = $message;

        $mail->send();
        echo 'Message has been sent';
    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
  }

  public function emailIT_blank_repair($get_name,$get_email,$get_mobile,$get_address,$get_brand,$get_model,$get_category,$get_problem,$get_message,$line)
  {
    require_once '_controllers/occotoadmin.php';
    $subject = "Quick Appointment for repairing | unlocking | LONDON TECHNTRADE";

    $message = "
    <html>
    <head>
    <title>QUOT REQUEST FOR REPAIRING ".$get_model."</title>

    </head>
    <body style='font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',sans-serif;'>
      <div style='text-align:center;margin:0px'>
        <font style='font-size: 22px;padding-left:0px;padding-right:0px;font-weight:bold;margin-right:0px;color:#1655a6'>
          LONDON
        </font>
        <font style='font-size: 22px;padding-left:5px;padding-right:5px;font-weight:bold;margin:0px;color:white;background:#1655a6'>
          TECHNTRADE
        </font>
      </div>
    <h2 style='width:100%;background:#1655a6;padding:5px;margin-botton:-10px;text-transform:uppercase;text-align:center;font-weight:bold;color:black'>REPAIR enquiry</h2>

    <b>Dear esteemed customer,</b><br><br>

    Thank you for your enquiry.<br><br>

    Our representative will catch you shortly.<br><br>

    However, if your inquiry needs urgent attention, you can contact our 24/7 customer care on ".OccotoAdmin::info('whatsapp')." or Dial:02086917978.<br><br><br>

    <b>Best Regards,</b><br>
    Kashan<br><br>

    <b>Manager</b><br>Customer Care department <br><br>
    <U>LONDON TECHNTRADE</U><br>113 Loampit Vale, London SE13 7TG, UK <br>
    STORE TIMING 9AM - 10PM
    <br>
    <br><hr>";
    $message.= "<h1 class='heading main'><span>Your given contact details</span></h1><table id='customers'>
    <tr>
      <th>Name</th>
      <th>Email</th>
      <th>Mobile</th>
      <th>Brand</th>
      <th>Model</th>
      <th>Message</th>
    <tr>
    <tr>
      <td>".$get_name."</td>
      <td>".$get_email."</td>
      <td>".$get_mobile."</td>
      <td>".$get_brand."</td>
      <td>".$get_model."</td>
      <td>".$get_message."</td>
    <tr>
    </table><br><hr>
    ".$get_category."<br>".$get_problem."
    <h5>We will contact you shortly.<br><br>Thank you for choosing us</h5>
    <br>
    <img alt='LONDON Techntrade' src='http://londontt.com/wp-content/themes/fixit/assets/phone-repair/images/logo.png'>
    <h4 style='background:;padding:5px;margin-botton:-10px;text-transform:uppercase;text-align:left'>store time 9AM - 10PM<br>Need help? - 020 8691 7978</h4>
    <br><h5>this message generated from http://londontt.com/</h5>
    </body>
    </html>";

    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'p3plcpnl0143.prod.phx3.secureserver.net';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'noreply@londontt.com';                 // SMTP username
        $mail->Password = 'Extream_ds_365';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('londontechntrade@gmail.com', 'Kashan London Techntrade');
        $mail->addAddress($get_email, $get_name);     // Add a recipient
        //$mail->addAddress('ellen@example.com');               // Name is optional
        $mail->addReplyTo('londontechntrade@gmail.com', 'Kashan');
        //$mail->addCC('gr8shariq@gmail.com');
        $mail->addBCC('londontechntrade@gmail.com');

        // //Attachments
        // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $message;
        $mail->AltBody = $message;

        $mail->send();
        echo 'Message has been sent';
    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
  }


}

 ?>
