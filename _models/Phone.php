<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/
class Phone //model
{
  private $db;
  function __construct()
  {
    $this->db = new Database();
  }

  function getprodcolors($prodid)
  {
    $abc = new SafeHeaven();
    $param = $abc->xss_clean($prodid);
    $this->db->query("select C.color_name,C.color_code,C.color_id,CP.product_id from color C INNER JOIN color_productbind CP ON CP.color_id=C.color_id WHERE CP.product_id=".$prodid.";");
    return $this->db->resultSet();
  }

  function getprice($prodid,$capid,$condid,$networkid)
  {
    $abc = new SafeHeaven();
    $prodid = $abc->xss_clean($prodid);
    $capid = $abc->xss_clean($capid);
    $condid = $abc->xss_clean($condid);
    $networkid = $abc->xss_clean($networkid);

    $this->db->query("SELECT id as rec_cart_id,Offer_price from recyleapp_price WHERE prod_id=:prodid AND capacity_id=:capid AND condition_id=:condid AND network_id=:networkid;");
    $this->db->bind(':prodid',$prodid);
    $this->db->bind(':capid',$capid);
    $this->db->bind(':condid',$condid);
    $this->db->bind(':networkid',$networkid);
    return $this->db->resultSet();
  }

  function getAvailableNetwork($param)
  {
    $abc = new SafeHeaven();
    $param = $abc->xss_clean($param);
    $this->db->query("select distinct(N.network_id),N.network_name,N.network_img from recyleapp_price RP INNER JOIN networks N on N.network_id=RP.network_id WHERE prod_id=:prodid;");
    $this->db->bind(':prodid',$param);
    return $this->db->resultSet();
  }

  function getAvailableCapacity($param)
  {
    $abc = new SafeHeaven();
    $param = $abc->xss_clean($param);
    $this->db->query("SELECT capacity,specific_product_id FROM product_capacity where product_id=:prodid order by capacity");
    $this->db->bind(':prodid',$param);
    return $this->db->resultSet();
  }

  function getAvailableCondition($param)
  {
    $abc = new SafeHeaven();
    $param = $abc->xss_clean($param);
    $this->db->query("select distinct(C.condition_id),C.condition_name,C.conditions from recyleapp_price RP INNER JOIN conditions C ON C.condition_id=RP.condition_id WHERE RP.prod_id=:prodid;");
    $this->db->bind(':prodid',$param);
    return $this->db->resultSet();
  }

  function getConditionDescription($param,$param2=false)
  {
    $abc = new SafeHeaven();
    $param = $abc->xss_clean($param);
  if (isset($param2)) {
      $param2 = $abc->xss_clean($param2);
    }
    $this->db->query("select TD.description1 from products P
INNER JOIN template_description_bind TDB ON TDB.prod_id=P.product_id
INNER JOIN template_description TD ON TD.template_id=TDB.template_id
INNER JOIN conditions C ON C.condition_id=TD.condition_id
WHERE P.product_id=:prodid AND C.condition_id=:condname");
$this->db->bind(':prodid',$param);
$this->db->bind(':condname',$param2);
    return $this->db->single();
  }

}

?>
