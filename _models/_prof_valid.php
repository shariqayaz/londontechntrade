<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/


/**
 * Validate Profile true false
 */
class Prof_Validate_mod
{
  public $ret_action = false;

  public function profileval($requeststr)
  {
    $requeststr_arr = explode('^^',$requeststr);
    $ctrl_code = 'profile-valid';
    $reflectarg = new Meta_Reflector();
    $bool = $reflectarg->reflect_arg($ctrl_code,$requeststr_arr[0]);
    return $bool;
  }
}

?>
