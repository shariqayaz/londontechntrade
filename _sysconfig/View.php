<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/

class View {

	function __construct() {
		//echo 'this is the view';
  }

	public function render($get_what,$get_requeststr,$subrequest=false,$extrastr=false)
	{
		$getwhatrender = $get_what;
		$requeststr = $get_requeststr;
		$subrequest_render = $subrequest;
		$extrarequest = $extrastr;
		// //
		// echo '<h1>'.$getwhatrender.'</h1>';// = $get_what;
		// echo '<h1>'.$requeststr.'</h1>';// = $get_requeststr;
		// echo $subrequest_render;// = $subrequest;
		// echo $extrarequest;// = $extraxstr;
		// echo $subrequest_render;
	  // echo "<font color=brown>Sysconfig-View</font><br>".$get_requeststr."<br>";
		///////////////////////////// Profile ////////////////////////////////
		if ($getwhatrender == 'profile' && $getwhatrender != '')
		{
			require_once '_views/'.$getwhatrender.'/index.php';
		}
		///////////////////////// Collections ////////////////////////////////
		else if ($getwhatrender == 'recycle' && $getwhatrender != '')
		{
			require_once '_views/'.$getwhatrender.'/index.php';
		}
		else if ($getwhatrender == 'account' && $requeststr != 'orders')
		{
			require_once '_views/'.$getwhatrender.'/index.php';
		}
		else if ($getwhatrender == 'account' && $requeststr == 'orders')
		{
			require_once '_views/'.$getwhatrender.'/index.php';
		}
		else if ($getwhatrender == 'tabload' && $getwhatrender != '')
		{
			require_once '_views/'.$getwhatrender.'/index.php';
		}
		else if ($getwhatrender == 'phones' && $getwhatrender != '')
		{
			require_once '_views/'.$getwhatrender.'/index.php';
		}
		else if ($getwhatrender == 'phones/submit' && $getwhatrender != '')
		{
			require_once '_views/'.$getwhatrender.'/index.php';
		}
		else if ($getwhatrender == 'login' && $getwhatrender != '')
		{
			require_once '_views/'.$getwhatrender.'/index.php';
		}
		else if ($getwhatrender == 'sign-up' && $getwhatrender != '')
		{
			require_once '_views/'.$getwhatrender.'/index.php';
		}
		else if ($getwhatrender == 'dashboard' && $getwhatrender != '')
		{
			require_once '_views/'.$getwhatrender.'/index.php';
		}
		else if ($getwhatrender == 'index' && $getwhatrender != '')
		{
			require_once '_views/index/index.php';
		}
		else if ($getwhatrender == 'occoto-admin' && $requeststr == 'occoto-admin')
		{
			require_once '_views/occotoadmin/index.php';
		}
		else if ($getwhatrender == 'occoto-admin' && $requeststr == 'product management')
		{
			require_once '_views/occotoadmin/pd.php';
		}
		else if ($getwhatrender == 'occoto-admin' && $requeststr == 'general information')
		{
			require_once '_views/occotoadmin/gen_info.php';
		}
		else if ($getwhatrender == 'occoto-admin' && $requeststr == 'tabload')
		{
			require_once '_views/occotoadmin/tabload.php';
		}
		else if ($getwhatrender == 'apps' && $requeststr == 'getoval')
		{
			//echo "string";
			require_once '_views/apps/price.php';
		}
		else if ($getwhatrender == 'app' && $requeststr == 'recycle')
		{
			require_once '_views/apps/recyclesr.php';
		}
		else if ($getwhatrender == 'app' && $requeststr == 'repair')
		{
			require_once '_views/apps/repairsr.php';
		}
		else if ($getwhatrender == 'shipment' && $requeststr == 'ship-mode')
		{
			require_once '_views/shipment/index.php';
		}
		else if ($getwhatrender == 'finished' && $requeststr == 'finished')
		{
			require_once '_views/finished/index.php';
		}
		else if ($getwhatrender == 'thankyou' && $getwhatrender == 'thankyou')
		{
			require_once '_views/thankyou/index.php';
		}
		else if ($getwhatrender == 'repair' && $requeststr == 'repair')
		{
			require_once '_views/repair/index.php';
		}
		else if ($getwhatrender == 'repair' && $requeststr == 'phones')
		{
			require_once '_views/repairphones/index.php';
		}
		else if ($getwhatrender == 'common' && $requeststr == 'headvone')
		{
			require_once '_views/common/headvone.php';
		}
		else if ($getwhatrender == 'common' && $requeststr == 'headvonehome')
		{
			require_once '_views/common/headvonehome.php';
		}
		else if ($getwhatrender == 'common' && $requeststr == 'headerinfo')
		{
			require_once '_views/common/headerinfo.php';
		}
		else if ($getwhatrender == 'common' && $requeststr == 'footvone')
		{
			require_once '_views/common/footvone.php';
		}
		else if ($getwhatrender == 'form' && $requeststr == 'emptyrepairform')
		{
			require_once '_views/forms/blankform.php';
		}
		else if ($getwhatrender == 'form' && $requeststr == 'emptysellform')
		{
			require_once '_views/forms/blankform.php';
		}
		else if ($getwhatrender == 'form' && $requeststr == 'repairsimple')
		{
			require_once '_views/forms/simplerepairform.php';
		}
		else if ($getwhatrender == 'Shop' && $requeststr == 'Shop')
		{
			require_once '_views/shop/index.php';
		}
		else if ($getwhatrender == 'contact' && $requeststr == 'contact')
		{
			require_once '_views/contact/index.php';
		}
		else if ($getwhatrender == 'howto' && $requeststr == 'howto')
		{
			require_once '_views/howto/index.php';
		}
		else if ($getwhatrender == 'occoto-admin' && $requeststr == 'products')
		{
			require_once '_views/occotoadmin/products.php';
		}
		else if ($getwhatrender == 'occoto-admin' && $requeststr == 'det')
		{
			require_once '_views/occotoadmin/proddet.php';
		}
		else {
			echo 'other ctrls';
		}

	}

}
?>
