<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/

class SessionMGR
{
    public static function init()
    {
        @session_start();
        if (!isset($_SESSION["sell_cart"]) || count($_SESSION["sell_cart"]) < 1){
          $_SESSION['sell_cart']=array();
        }
    }

    // UAANT Component - User Request & Data Receiver -- URDR
    public static function URDR($data)
    {
      echo '<font color=brown>SessionMGR</font><font color=red><b><br>get in urdr<br></b></font>';
      $shfunction = new SafeHeaven();
      $shfiltereddata = $shfunction->xss_clean($data);
      // get data into array;
      $getdata_arr = explode('^^',$shfiltereddata);
      $getdata_arr_count = count($getdata_arr);
      $ctrl_name = $getdata_arr[0];
      // determin the url levels
      if ($getdata_arr_count < 6 && $getdata_arr_count == 2 && $getdata_arr_count != 3 && $getdata_arr_count != 4 && $getdata_arr_count != 5 )
      {
        echo "<font color=brown>SessionMGR</font><br>VALUE PASSED FROM LEVEL 1: URDR: ";
        $userrequest = $ctrl_name.'^^'.$getdata_arr_count.'^^'.$getdata_arr[1];
        return $userrequest;
      }
      else if ($getdata_arr_count < 6 && $getdata_arr_count != 2 && $getdata_arr_count == 3 && $getdata_arr_count != 4 && $getdata_arr_count != 5 )
      {
        echo "<font color=brown>SessionMGR</font><br>VALUE PASSED FROM LEVEL 2: URDR: ";
        $userrequest = $ctrl_name.'^^'.$getdata_arr_count.'^^'.$getdata_arr[1].'^^'.$getdata_arr[2];
        return $userrequest;
      }
      else if ($getdata_arr_count < 6 && $getdata_arr_count != 2 && $getdata_arr_count != 3 && $getdata_arr_count == 4 && $getdata_arr_count != 5 )
      {
        echo "<font color=brown>SessionMGR</font><br>VALUE PASSED FROM LEVEL 3: URDR: ";
        $userrequest = $ctrl_name.'^^'.$getdata_arr_count.'^^'.$getdata_arr[1].'^^'.$getdata_arr[2].'^^'.$getdata_arr[3];
        return $userrequest;
      }
      else if ($getdata_arr_count < 6 && $getdata_arr_count != 2 && $getdata_arr_count != 3 && $getdata_arr_count != 4 && $getdata_arr_count == 5 )
      {
        echo "<font color=brown>SessionMGR</font><br>VALUE PASSED FROM LEVEL 4: URDR: ";
        $userrequest = $ctrl_name.'^^'.$getdata_arr_count.'^^'.$getdata_arr[1].'^^'.$getdata_arr[2].'^^'.$getdata_arr[3].'^^'.$getdata_arr[4];
        return $userrequest;
      } else { echo 'unexpected error'; }
    }

    // UAANT Component - Instruction & Data Preparation - IDP
    public static function IDP($urdrdata)
    {
      echo '<font color=brown>SessionMGR</font><font color=red><b><br>get in IDP<br></b></font>';
      $geturdr_arr = explode('^^',$urdrdata);
      // Get Controller name;
      $ctrl_name = $geturdr_arr[0];
      // GET Level COUNT
      $level_int = intval($geturdr_arr[1]);
      $level_int --; // LEVEL COUNTER - REMOVE NON LEVEL COUNT
      switch ($ctrl_name)
      {
        // PAGE, User Profile, Friend Page, Company Page, Community Page, ORG name Case //
        case strtolower('profile'):
          echo "<font color=brown>SessionMGR</font>Case 1 - Profile Case: ";
          $instruction = 'RENDER';
          $whatlevel = $level_int;
          $what = $ctrl_name;
          if ($level_int==1)
          {
            $requeststr = $geturdr_arr[2];
            // check for valid request
            require_once '_models/_prof_valid.php';
            $requeststrval = new Prof_Validate_mod();
            if ($requeststrval->profileval($requeststr) == true)
            {
              $idpreturn = $instruction.'??'.$what.'??'.$requeststr;
            }
            else
            {
              echo '<font color=brown>SessionMGR</font>not available';
              exit;
              header('location: /collections/');
              exit;
            }
          }
          else if ($level_int==2)
          {
            $requeststr = $geturdr_arr[2].'^^'.$geturdr_arr[3];
            // check for valid request
            require_once '_models/_prof_valid.php';
            $requeststrval = new Prof_Validate_mod();
            if ($requeststrval->profileval($requeststr) == true)
            {
              $idpreturn = $instruction.'??'.$what.'??'.$requeststr;
            }
            else
            {
              echo '<font color=brown>SessionMGR</font>not available';
              exit;
              header('location: /collections/');
              exit;
            }
          }
          else if ($level_int==3)
          {
            $requeststr = $geturdr_arr[2].'^^'.$geturdr_arr[3].'^^'.$geturdr_arr[4];
            // check for valid request
            require_once '_models/_prof_valid.php';
            $requeststrval = new Prof_Validate_mod();
            if ($requeststrval->profileval($requeststr) == true)
            {
              $idpreturn = $instruction.'??'.$what.'??'.$requeststr;
            }
            else
            {
              echo '<font color=brown>SessionMGR</font>not available';
              exit;
              header('location: /collections/');
              exit;
            }
          }
          else if ($level_int==4)
          {
            $requeststr = $geturdr_arr[2].'^^'.$geturdr_arr[3].'^^'.$geturdr_arr[4].'^^'.$geturdr_arr[5];
            // check for valid request
            require_once '_models/_prof_valid.php';
            $requeststrval = new Prof_Validate_mod();
            if ($requeststrval->profileval($requeststr) == true)
            {
              $idpreturn = $instruction.'??'.$what.'??'.$requeststr;
            }
            else
            {
              echo '<font color=brown>SessionMGR</font>not available';
              exit;
              header('location: /collections/');
              exit;
            }

          }
          break;
        // Main Page / Friends Page //
        case strtolower('mainpage'):
          echo '<font color=brown>SessionMGR</font>Case 2 - Main Page Case: ';
          break;
        // Collection Page / Collection Controller //
        case strtolower('collection'):
          echo '<font color=brown>SessionMGR</font>Case 3 - Collection Page Case: ';
          break;
        // Main Page / Friends Page Group Page //
        case strtolower('group'):
          echo '<font color=brown>SessionMGR</font>Case 4 - Profile Case - Group Case:  ';
          break;
        default:
          echo '<font color=brown>SessionMGR</font>Unknown Case';
          // REDIRECT TO ERROR PAGE
          break;
      }
      return $idpreturn;
    }

    public function CAS($idpdata)
    {
      echo '<font color=brown>SessionMGR</font><font color=red><b><br>get in CAS<br></b></font>';
      $getidpdata = $idpdata;
      // FILTERED DATA
      $shcall = new SafeHeaven();
      $filteredidpdata = $shcall->xss_clean($getidpdata);
      ///////////////////////////////////////////////////
      $get_iwr_arr = explode('??',$filteredidpdata);
      $get_request_str_arr = explode('^^',$get_iwr_arr[2]);
      $gisession = new SessionMGR();
      // level 1 $get_request_str_arr[0] | Level 2 $get_request_str_arr[1] | Level 3 $get_request_str_arr[2] | Level 4 $get_request_str_arr[3]
      $gisession->setWV('profile-name',$get_request_str_arr[0]);
      $actionset = $get_iwr_arr[0].'||'.$get_iwr_arr[1].'||'.$get_request_str_arr[0];
      return $actionset;
    }

    public static function setWV($key, $value)
    {
        if ($key != '') {
          $_SESSION[$key] = $value;
        }
    }

    public static function getWV($key)
    {
        if (isset($_SESSION[$key]))
        return $_SESSION[$key];
    }

    public static function destroy()
    {
       session_destroy();
    }

    public static function chkLogin($data)
    {
      if (isset($_SESSION[$data]) && $_SESSION[$data]==true)
      {
        return true;
      }
      else
      {
        //$setSession = new SessionMGR();
        //$setSession->setWV('isLogin',false);
        return false;
      }
    }

}
?>
