<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/


class Routfilter
{
  public $strdis = '';
  public $strlendis = '';
  public $ret_action = false;
  function rout_filter($coun_t,$get_na_v,$nav_leve,$var_action)
  {
    // LEVEL 1
    if ($nav_leve == 1)
    {
    //  echo "<font color=brown>rout_filter</font><br> GOING FROM LEVEL 1 <br>";
      // filter counter
      if (is_string($coun_t) && !ctype_digit($coun_t))
      {
        return false;
        // Critical Error Page Forward and Log the Event
        exit;
      }
      if (!is_int((int) $coun_t))
      {
        return false;
        // Critical Error Page Forward and Log the Event
        exit;
      }
      // filter counter
      if ($coun_t > 50 || $coun_t < 3 && $nav_leve==1)
      {
        $this->ret_action = false;
        // Length issue Error
        //exit;
      }
      else if ($coun_t < 3 || $coun_t > 50 && $nav_leve==1)
      {
        $this->ret_action = false;
        // Length issue Error
        //exit;
      }
      else if ($coun_t < 51 && $coun_t > 2 && $nav_leve==1)
      {
        $this->ret_action = true;
      }
      // charactor filter
      if ($this->ret_action==true)
      {
        $forbid = '/[@$%^*!\|,`=~<>{}[\]()\+;()]+|(?:)/i';
        $str = preg_replace( $forbid, '', $get_na_v);
        $this->strdis = $str;
        $this->finalcount = strlen($str);
      }
      else if ($this->ret_action==false)
      {
        return $this->ret_action;
        // Due to Length or filter issue
        exit;
      }
      return $this->ret_action;
    }
    // LEVEL 2
    else if ($nav_leve == 2)
    {
      // filter counter
      if (is_string($coun_t) && !ctype_digit($coun_t))
      {
        return false;
        // Critical Error Page Forward and Log the Event
        exit;
      }
      if (!is_int((int) $coun_t))
      {
        return false;
        // Critical Error Page Forward and Log the Event
        exit;
      }
      // filter counter
      $var_get = $var_action;
      // ROUND ONE
      if ($var_get == 'one'.'2')
      {
        // filter counter
        if ($coun_t > 50 || $coun_t < 3 && $nav_leve==2)
        {
          $this->ret_action = false;
          return $this->ret_action;
          // Length issue Error
          exit;
        }
        else if ($coun_t < 3 || $coun_t > 50 && $nav_leve==2)
        {
          $this->ret_action = false;
          return $this->ret_action;
          // Length issue Error
          exit;
        }
        else if ($coun_t < 51 && $coun_t > 2 && $nav_leve==2)
        {
          $this->ret_action = true;
        }
      }
      // ROUND 2
      else if ($var_get == 'two'.'2')
      {
        // filter counter
        if ($coun_t > 100 || $coun_t < 2 && $nav_leve==2)
        {
          $this->ret_action = false;
          // Length issue Error
          //exit;
        }
        else if ($coun_t < 3 && $coun_t > 50 && $nav_leve==2)
        {
          $this->ret_action = false;
          // Length issue Error
          //exit;
        }
        else if ($coun_t < 51 && $coun_t > 2 && $nav_leve==2)
        {
          $this->ret_action = true;
        }
      }
      // charactor filter
      if ($this->ret_action==true)
      {
        $forbid = '/[@$%^*!\|,`=~<>{}[\]()\+;()]+|(?:)/i';
        $str = preg_replace( $forbid, '', $get_na_v);
        $this->strdis = $str;
        $this->finalcount = strlen($str);
        return $this->ret_action;
      }
      else if ($this->ret_action==false)
      {
        return $this->ret_action;
        // Due to Length or filter issue
        exit;
      }
    }
    // LEVEL 3
    else if ($nav_leve == 3)
    {
      // filter counter
      if (is_string($coun_t) && !ctype_digit($coun_t))
      {
        return false;
        // Critical Error Page Forward and Log the Event
        exit;
      }
      if (!is_int((int) $coun_t))
      {
        return false;
        // Critical Error Page Forward and Log the Event
        exit;
      }
      // filter counter
      $var_get = $var_action;
      // ROUND ONE
      if ($var_get == 'one'.'3')
      {
        // filter counter
        if ($coun_t > 50 || $coun_t < 2 && $nav_leve==3)
        {
          $this->ret_action = true;
          return $this->ret_action;
          // Length issue Error
          exit;
        }
        else if ($coun_t < 3 || $coun_t > 50 && $nav_leve==3)
        {
          $this->ret_action = true;
          return $this->ret_action;
          // Length issue Error
          exit;
        }
        else if ($coun_t < 51 && $coun_t > 2 && $nav_leve==3)
        {
          $this->ret_action = true;
        }
      }
      // ROUND 2
      else if ($var_get == 'two'.'3')
      {
        // filter counter
        if ($coun_t > 100 || $coun_t < 2 && $nav_leve==3)
        {
          $this->ret_action = true;
          // Length issue Error
          //exit;
        }
        else if ($coun_t < 3 && $coun_t > 50 && $nav_leve==3)
        {
          $this->ret_action = true;
          // Length issue Error
          //exit;
        }
        else if ($coun_t < 51 && $coun_t > 2 && $nav_leve==3)
        {
          $this->ret_action = true;
        }
      }
      // ROUND 3
      else if ($var_get == 'three'.'3')
      {
        // filter counter
        if ($coun_t > 100 || $coun_t < 2 && $nav_leve==3)
        {
          $this->ret_action = true;
          // Length issue Error
          //exit;
        }
        else if ($coun_t < 3 && $coun_t > 50 && $nav_leve==3)
        {
          $this->ret_action = true;
          // Length issue Error
          //exit;
        }
        else if ($coun_t < 51 && $coun_t > 2 && $nav_leve==3)
        {
          $this->ret_action = true;
        }
      }
      // charactor filter
      if ($this->ret_action==true)
      {
        $forbid = '/[@$%^*!\|,`=~<>{}[\]()\+;()]+|(?:)/i';
        $str = preg_replace( $forbid, '', $get_na_v);
        $this->strdis = $str;
        $this->finalcount = strlen($str);
        return $this->ret_action;
      }
      else if ($this->ret_action==false)
      {
        return $this->ret_action;
        // Due to Length or filter issue
        exit;
      }
    }
    // LEVEL 4
    else if ($nav_leve == 4)
    {
      // filter counter
      if (is_string($coun_t) && !ctype_digit($coun_t))
      {
        return false;
        // Critical Error Page Forward and Log the Event
        exit;
      }
      if (!is_int((int) $coun_t))
      {
        return false;
        // Critical Error Page Forward and Log the Event
        exit;
      }
      // filter counter
      $var_get = $var_action;
      // ROUND ONE
      if ($var_get == 'one'.'4')
      {
        // filter counter
        if ($coun_t > 150 || $coun_t < 0 && $nav_leve==4)
        {
          $this->ret_action = false;
          return $this->ret_action;
          // Length issue Error
          exit;
        }
        else if ($coun_t < 1 || $coun_t > 150 && $nav_leve==4)
        {
          $this->ret_action = false;
          return $this->ret_action;
          // Length issue Error
          exit;
        }
        else if ($coun_t < 51 && $coun_t > 0 && $nav_leve==4)
        {
          $this->ret_action = true;
        }
      }
      // ROUND 2
      else if ($var_get == 'two'.'4')
      {
        // filter counter
        if ($coun_t > 150 || $coun_t < 1 && $nav_leve==4)
        {
          $this->ret_action = false;
          // Length issue Error
          //exit;
        }
        else if ($coun_t < 1 && $coun_t > 150 && $nav_leve==4)
        {
          $this->ret_action = false;
          // Length issue Error
          //exit;
        }
        else if ($coun_t < 151 && $coun_t > 0 && $nav_leve==4)
        {
          $this->ret_action = true;
        }
      }
      // ROUND 3
      else if ($var_get == 'three'.'4')
      {
        // filter counter
        if ($coun_t > 150 || $coun_t < 0 && $nav_leve==4)
        {
          $this->ret_action = false;
          // Length issue Error
          //exit;
        }
        else if ($coun_t < 0 && $coun_t > 150 && $nav_leve==4)
        {
          $this->ret_action = false;
          // Length issue Error
          //exit;
        }
        else if ($coun_t < 151 && $coun_t > 0 && $nav_leve==4)
        {
          $this->ret_action = true;
        }
      }
      // ROUND 4
      else if ($var_get == 'four'.'4')
      {
        // filter counter
        if ($coun_t > 100 || $coun_t < 0 && $nav_leve==4)
        {
          $this->ret_action = false;
          // Length issue Error
          //exit;
        }
        else if ($coun_t < 0 && $coun_t > 150 && $nav_leve==4)
        {
          $this->ret_action = false;
          // Length issue Error
          //exit;
        }
        else if ($coun_t < 151 && $coun_t > 0 && $nav_leve==4)
        {
          $this->ret_action = true;
        }
      }
      // charactor filter
      if ($this->ret_action==true)
      {
        $forbid = '/[@$%^*!\|,`=~<>{}[\]()\+;()]+|(?:)/i';
        $str = preg_replace( $forbid, '', $get_na_v);
        $this->strdis = $str;
        $this->finalcount = strlen($str);
        return $this->ret_action;
      }
      else if ($this->ret_action==false)
      {
        return $this->ret_action;
        // Due to Length or filter issue
        exit;
      }
    }
    // OUT OF LEVEL - ERROR in LEVEL - WRONG LEVEL
    else if ($nav_leve > 4)
    {
      echo "<br> GOING FROM - LEVEL <br>";
      echo "ERROR : LEVEL OUT";
    }
  }

  function strdispatch()
  {
    $sh_string = new SafeHeaven();
    $shpassed = $sh_string->xss_clean($this->strdis);
    $shpassed = $this->strdis;
    //$this->strdis =
    return $shpassed;
  }

  function strlendispatch()
  {
    return strlen($this->strdis);
  }

}
 ?>
