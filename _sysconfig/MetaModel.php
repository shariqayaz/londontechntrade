<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/


/**
 *  META DATABASE REFLECTION - DATA REFLECTION
 */
class Meta_Reflector // extends DB_ACCESS
{
  public static function reflect_arg($what,$arg_1,$arg_2=false,$arg_3=false,$arg_4=false)
  {
    $getwhat = $what;
    $get_arg_1 = $arg_1;
    // multiple arg condition will be configured in future
    //
    //
    switch ($what) {
      case 'profile-valid':
        $user_id = DB_ACCESS::meta_user();
        $user_pass = DB_ACCESS::meta_pass();
        try
        {
          $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", "$user_id", "$user_pass");
          $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $st_meta = $conn_meta->prepare("SELECT r_id,id from `pub_prof_db_tbl` WHERE `r_id` = :name");
          $st_meta->bindParam(':name', $get_arg_1);
          $st_meta->execute();
          $rows = $st_meta->fetch(PDO::FETCH_ASSOC);
          // send true / false
          if ($st_meta->rowCount() > 0)
          {
            echo $rows['r_id'];
            return true;
          }
          else
          {
            return false;
          }
        }
        catch (PDOException $e)
        {
          throw new Exception('Could not connect to database');
          echo 'Connection failed: ' . $e->getMessage(); // SHOWING DATABASE NAME
        }

        break;
      case 'submit_req':
        $user_id = DB_ACCESS::meta_user();
        $user_pass = DB_ACCESS::meta_pass();
        try
        {
          $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", "$user_id", "$user_pass");
          $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $st_meta = $conn_meta->prepare("SELECT r_id,id from `pub_prof_db_tbl` WHERE `r_id` = :name");
          $st_meta->bindParam(':name', $get_arg_1);
          $st_meta->execute();
          $rows = $st_meta->fetch(PDO::FETCH_ASSOC);
          // send true / false
          if ($st_meta->rowCount() > 0)
          {
            echo $rows['r_id'];
            return true;
          }
          else
          {
            return false;
          }
        }
        catch (PDOException $e)
        {
          throw new Exception('Could not connect to database');
          echo 'Connection failed: ' . $e->getMessage(); // SHOWING DATABASE NAME
        }

        break;
      // case 'profile-valid':
      //   # code...
      //   break;
      default:
        # code...
        break;
    }
  }
}
 ?>
