<?php

/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/


class SafeHeaven
{
  // File Name nevel allow characters
  public $filename_bad_chars =	array(
    '../', '<!--', '-->', '<', '>',
    "'", '"', '&', '$', '#',
    '{', '}', '[', ']', '=',
    ';', '?', '%20', '%22',
    '%3c',		// <
    '%253c',	// <
    '%3e',		// >
    '%0e',		// >
    '%28',		// (
    '%29',		// )
    '%2528',	// (
    '%26',		// &
    '%24',		// $
    '%3f',		// ?
    '%3b',		// ;
    '%3d'		// =
  );

  // characters set is UTF8
  public $charset = 'UTF-8';

  // creating rand hash to secure URL - XSS
  protected $_xss_hash;

  // Never allow string
  protected $_never_allowed_str =	array(
    'document.cookie' => '[removed]',
    'document.write'  => '[removed]',
    '.parentNode'     => '[removed]',
    '.innerHTML'      => '[removed]',
    '-moz-binding'    => '[removed]',
    '<!--'            => '&lt;!--',
    '-->'             => '--&gt;',
    '<![CDATA['       => '&lt;![CDATA[',
    '<comment>'	  => '&lt;comment&gt;',
    '<%'              => '&lt;&#37;'
  );

  // Never Allow regular expression string
  protected $_never_allowed_regex = array(
    'javascript\s*:',
    '(document|(document\.)?window)\.(location|on\w*)',
    'expression\s*(\(|&\#40;)', // CSS and IE
    'vbscript\s*:', // IE, surprise!
    'wscript\s*:', // IE
    'jscript\s*:', // IE
    'vbs\s*:', // IE
    'Redirect\s+30\d',
    "([\"'])?data\s*:[^\\1]*?base64[^\\1]*?,[^\\1]*?\\1?"
  );

  	// --------------------------------------------------------------------

  	/**
  	 * Get random bytes
  	 *
  	 * @param	int	$length	Output length
  	 * @return	string
  	 */
  	public function get_random_bytes($length)
  	{
  		if (empty($length) OR ! ctype_digit((string) $length))
  		{
  			return FALSE;
  		}

  		if (function_exists('random_bytes'))
  		{
  			try
  			{
  				// The cast is required to avoid TypeError
  				return random_bytes((int) $length);
  			}
  			catch (Exception $e)
  			{
  				// If random_bytes() can't do the job, we can't either ...
  				// There's no point in using fallbacks.
  				log_message('error', $e->getMessage());
  				return FALSE;
  			}
  		}

  		// Unfortunately, none of the following PRNGs is guaranteed to exist ...
  		if (defined('MCRYPT_DEV_URANDOM') && ($output = mcrypt_create_iv($length, MCRYPT_DEV_URANDOM)) !== FALSE)
  		{
  			return $output;
  		}


  		if (is_readable('/dev/urandom') && ($fp = fopen('/dev/urandom', 'rb')) !== FALSE)
  		{
  			// Try not to waste entropy ...
  			is_php('5.4') && stream_set_chunk_size($fp, $length);
  			$output = fread($fp, $length);
  			fclose($fp);
  			if ($output !== FALSE)
  			{
  				return $output;
  			}
  		}

  		if (function_exists('openssl_random_pseudo_bytes'))
  		{
  			return openssl_random_pseudo_bytes($length);
  		}

  		return FALSE;
  	}


  	/**
  	 * XSS Hash
  	 *
  	 * Generates the XSS hash if needed and returns it.
  	 *
  	 * @see		CI_Security::$_xss_hash
  	 * @return	string	XSS hash
  	 */
  	public function xss_hash()
  	{
  		if ($this->_xss_hash === NULL)
  		{
  			$rand = $this->get_random_bytes(16);
  			$this->_xss_hash = ($rand === FALSE)
  				? md5(uniqid(mt_rand(), TRUE))
  				: bin2hex($rand);
  		}

  		return $this->_xss_hash;
  	}


    	// --------------------------------------------------------------------

    	/**
    	 * HTML Entities Decode
    	 *
    	 * A replacement for html_entity_decode()
    	 *
    	 * The reason we are not using html_entity_decode() by itself is because
    	 * while it is not technically correct to leave out the semicolon
    	 * at the end of an entity most browsers will still interpret the entity
    	 * correctly. html_entity_decode() does not convert entities without
    	 * semicolons, so we are left with our own little solution here. Bummer.
    	 *
    	 * @link	http://php.net/html-entity-decode
    	 *
    	 * @param	string	$str		Input
    	 * @param	string	$charset	Character set
    	 * @return	string
    	 */
    	public function entity_decode($str, $charset = NULL)
    	{
    		if (strpos($str, '&') === FALSE)
    		{
    			return $str;
    		}

    		static $_entities;

    		isset($charset) OR $charset = $this->charset;
    		$flag = is_php('5.4')
    			? ENT_COMPAT | ENT_HTML5
    			: ENT_COMPAT;

    		if ( ! isset($_entities))
    		{
    			$_entities = array_map('strtolower', get_html_translation_table(HTML_ENTITIES, $flag, $charset));

    			// If we're not on PHP 5.4+, add the possibly dangerous HTML 5
    			// entities to the array manually
    			if ($flag === ENT_COMPAT)
    			{
    				$_entities[':'] = '&colon;';
    				$_entities['('] = '&lpar;';
    				$_entities[')'] = '&rpar;';
    				$_entities["\n"] = '&NewLine;';
    				$_entities["\t"] = '&Tab;';
    			}
    		}

    		do
    		{
    			$str_compare = $str;

    			// Decode standard entities, avoiding false positives
    			if (preg_match_all('/&[a-z]{2,}(?![a-z;])/i', $str, $matches))
    			{
    				$replace = array();
    				$matches = array_unique(array_map('strtolower', $matches[0]));
    				foreach ($matches as &$match)
    				{
    					if (($char = array_search($match.';', $_entities, TRUE)) !== FALSE)
    					{
    						$replace[$match] = $char;
    					}
    				}

    				$str = str_replace(array_keys($replace), array_values($replace), $str);
    			}

    			// Decode numeric & UTF16 two byte entities
    			$str = html_entity_decode(
    				preg_replace('/(&#(?:x0*[0-9a-f]{2,5}(?![0-9a-f;])|(?:0*\d{2,4}(?![0-9;]))))/iS', '$1;', $str),
    				$flag,
    				$charset
    			);

    			if ($flag === ENT_COMPAT)
    			{
    				$str = str_replace(array_values($_entities), array_keys($_entities), $str);
    			}
    		}
    		while ($str_compare !== $str);
    		return $str;
    	}


  /**
   * HTML Entity Decode Callback
   * @used-by	Security::xss_clean()
  */
  protected function _decode_entity($match)
  {
    // Protect GET variables in URLs
    // 901119URL5918AMP18930PROTECT8198
    $match = preg_replace('|\&([a-z\_0-9\-]+)\=([a-z\_0-9\-/]+)|i', $this->xss_hash().'\\1=\\2', $match[0]);

    // Decode, then un-protect URL GET vars
    return str_replace(
      $this->xss_hash(),
      '&',
      $this->entity_decode($match, $this->charset)
    );
  }

  // --------------------------------------------------------------------

  /**
   * JS Link Removal
   *
   * Callback method for xss_clean() to sanitize links.
   *
   * This limits the PCRE backtracks, making it more performance friendly
   * and prevents PREG_BACKTRACK_LIMIT_ERROR from being triggered in
   * PHP 5.2+ on link-heavy strings.
   *
   * @used-by	CI_Security::xss_clean()
   * @param	array	$match
   * @return	string
   */
  protected function _js_link_removal($match)
  {
    return str_replace(
      $match[1],
      preg_replace(
        '#href=.*?(?:(?:alert|prompt|confirm)(?:\(|&\#40;)|javascript:|livescript:|mocha:|charset=|window\.|document\.|\.cookie|<script|<xss|d\s*a\s*t\s*a\s*:)#si',
        '',
        $this->_filter_attributes($match[1])
      ),
      $match[0]
    );
  }


  protected function _convert_attribute($match)
  {
    return str_replace(array('>', '<', '\\'), array('&gt;', '&lt;', '\\\\'), $match[0]);
  }

  public function xss_clean($str)
	{
		// Remove Invisible Characters
		$str = $this->remove_invisible_characters($str);

    // Decode url
		if (stripos($str, '%') !== false)
		{
			do
			{
				$oldstr = $str;
				$str = rawurldecode($str);
				$str = preg_replace_callback('#%(?:\s*[0-9a-f]){2,}#i', array($this, '_urldecodespaces'), $str);
			}
			while ($oldstr !== $str);
			unset($oldstr);
		}

    // UTF only and numeric and ~ - _ signs
		$str = preg_replace_callback("/[^a-z0-9>]+[a-z0-9]+=([\'\"]).*?\\1/si", array($this, '_convert_attribute'), $str);
		$str = preg_replace_callback('/<\w+.*/si', array($this, '_decode_entity'), $str);

		// Remove Invisible Characters Again!
		$str = $this->remove_invisible_characters($str);

		// Convert All tabs into spaces
		$str = str_replace("\t", ' ', $str);

		// Remove Strings that are never allowed
		$str = $this->_do_never_allowed($str);

    // spaces b/w word and undo
		$words = array(
			'javascript', 'expression', 'vbscript', 'jscript', 'wscript',
			'vbs', 'script', 'base64', 'applet', 'alert', 'document',
			'write', 'cookie', 'window', 'confirm', 'prompt', 'eval'
		);

		foreach ($words as $word)
		{
			$word = implode('\s*', str_split($word)).'\s*';

			// We only want to do this when it is followed by a non-word character
			// That way valid stuff like "dealer to" does not become "dealerto"
			$str = preg_replace_callback('#('.substr($word, 0, -3).')(\W)#is', array($this, '_compact_exploded_words'), $str);
		}

		// remove inline js , other unwantd codes
		do
		{
			$original = $str;

			if (preg_match('/<a/i', $str))
			{
				$str = preg_replace_callback('#<a(?:rea)?[^a-z0-9>]+([^>]*?)(?:>|$)#si', array($this, '_js_link_removal'), $str);
			}

			if (preg_match('/<img/i', $str))
			{
				$str = preg_replace_callback('#<img[^a-z0-9]+([^>]*?)(?:\s?/?>|$)#si', array($this, '_js_img_removal'), $str);
			}

			if (preg_match('/script|xss/i', $str))
			{
        // redirect to collection  OR  Error page
        // Redirect code HERE
        // atleast disable javascript from Router
				$str = preg_replace('#</*(?:script|xss).*?>#si', '[r e m o v e d]', $str);
			}
		}
		while ($original !== $str);
		unset($original);

		/*
		 * Sanitize naughty HTML elements
		 *
		 * If a tag containing any of the words in the list
		 * below is found, the tag gets converted to entities.
		 *
		 * So this: <blink>
		 * Becomes: &lt;blink&gt;
		 */
		$pattern = '#'
			.'<((?<slash>/*\s*)((?<tagName>[a-z0-9]+)(?=[^a-z0-9]|$)|.+)' // tag start and name, followed by a non-tag character
			.'[^\s\042\047a-z0-9>/=]*' // a valid attribute character immediately after the tag would count as a separator
			// optional attributes
			.'(?<attributes>(?:[\s\042\047/=]*' // non-attribute characters, excluding > (tag close) for obvious reasons
			.'[^\s\042\047>/=]+' // attribute characters
			// optional attribute-value
				.'(?:\s*=' // attribute-value separator
					.'(?:[^\s\042\047=><`]+|\s*\042[^\042]*\042|\s*\047[^\047]*\047|\s*(?U:[^\s\042\047=><`]*))' // single, double or non-quoted value
				.')?' // end optional attribute-value group
			.')*)' // end optional attributes group
			.'[^>]*)(?<closeTag>\>)?#isS';

		// Note: It would be nice to optimize this for speed, BUT
		//       only matching the naughty elements here results in
		//       false positives and in turn - vulnerabilities!
		do
		{
			$old_str = $str;
			$str = preg_replace_callback($pattern, array($this, '_sanitize_naughty_html'), $str);
		}
		while ($old_str !== $str);
		unset($old_str);

		/*
		 * Sanitize naughty scripting elements
		 *
		 * Similar to above, only instead of looking for
		 * tags it looks for PHP and JavaScript commands
		 * that are disallowed. Rather than removing the
		 * code, it simply converts the parenthesis to entities
		 * rendering the code un-executable.
		 *
		 * For example:	eval('some code')
		 * Becomes:	eval&#40;'some code'&#41;
		 */
		$str = preg_replace(
			'#(alert|prompt|confirm|cmd|passthru|eval|exec|expression|system|fopen|fsockopen|file|file_get_contents|readfile|unlink)(\s*)\((.*?)\)#si',
			'\\1\\2&#40;\\3&#41;',
			$str
		);

		// Final clean up
		// This adds a bit of extra precaution in case
		// something got through the above filters
		$str = $this->_do_never_allowed($str);

		/*
		 * Images are Handled in a Special Way
		 * - Essentially, we want to know that after all of the character
		 * conversion is done whether any unwanted, likely XSS, code was found.
		 * However, if the string post-conversion does not matched the
		 * string post-removal of XSS, then it fails, as there was unwanted XSS
		 * code found and removed/changed during processing.
		 */
		return $str;
	}

  function remove_invisible_characters($str, $url_encoded = TRUE)
  {
    $non_displayables = array();

    // every control character except newline (dec 10),
    // carriage return (dec 13) and horizontal tab (dec 09)
    if ($url_encoded)
    {
      $non_displayables[] = '/%0[0-8bcef]/i';	// url encoded 00-08, 11, 12, 14, 15
      $non_displayables[] = '/%1[0-9a-f]/i';	// url encoded 16-31
    }

    $non_displayables[] = '/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/S';	// 00-08, 11, 12, 14-31, 127

    do
    {
      $str = preg_replace($non_displayables, '', $str, -1, $count);
    }
    while ($count);

    return $str;
  }

  protected function _compact_exploded_words($matches)
  {
    return preg_replace('/\s+/s', '', $matches[1]).$matches[2];
  }

  protected function _sanitize_naughty_html($matches)
  {
    static $naughty_tags    = array(
      'alert', 'area', 'prompt', 'confirm', 'applet', 'audio', 'basefont', 'base', 'behavior', 'bgsound',
      'blink', 'body', 'embed', 'expression', 'form', 'frameset', 'frame', 'head', 'html', 'ilayer',
      'iframe', 'input', 'button', 'select', 'isindex', 'layer', 'link', 'meta', 'keygen', 'object',
      'plaintext', 'style', 'script', 'textarea', 'title', 'math', 'video', 'svg', 'xml', 'xss'
    );
  }

  protected function _do_never_allowed($str)
	{
		$str = str_replace(array_keys($this->_never_allowed_str), $this->_never_allowed_str, $str);

		foreach ($this->_never_allowed_regex as $regex)
		{
			$str = preg_replace('#'.$regex.'#is', '[removed]', $str);
		}

		return $str;
	}

}

?>
