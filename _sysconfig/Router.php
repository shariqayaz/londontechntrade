<?php

/* -----------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ----------------------------------
--------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  -----------------------------------------
--------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com-----------------------------
------------------------------------------------------------------------------------------------------------------------ */

class Router
{
  //public $nvarlength_pub;
  function __construct()
  {
    // get navigation into array
    $get_nav_url = isset($_GET['navr']) ? $_GET['navr'] : null;
    $get_nav_url = rtrim($get_nav_url, '/');
    $shcall = new SafeHeaven();
    $get_nav_url = $shcall->xss_clean($get_nav_url);
    $get_nav_url = explode('/', $get_nav_url);
    //print_r($get_nav_url);
    // $this->nvarlength_pub = count($get_nav_url);
    $nvarlength = count($get_nav_url);
    // echo $this->nvarlength_pub;
    if ($nvarlength < 5)
    {
      // URL ROUT LEVELS - ZERO ROUT LEVEL
      if (empty($get_nav_url[0]) && empty($get_nav_url[1]) && empty($get_nav_url[2]) && empty($get_nav_url[3]))
      {
        $file = '_controllers/index.php';
        if (file_exists($file))
        {
          require $file;
          $ctrls = new Index();
          echo $ctrls->index();
        }
        else
        {
          // collection page unavailble
          echo '42L_err_';exit;header('location: /');
          exit;
          // error reporter / page
          $this->error();
        }
    		exit;
      }
      // URL ROUT LEVELS - FIRST ROUT LEVEL
      else if (isset($get_nav_url[0]) && empty($get_nav_url[1]) && empty($get_nav_url[2]) && empty($get_nav_url[3]))
      {
        //echo "<font color=brown>router</font>First Level<br>";
        if ($nvarlength == count($get_nav_url) && $nvarlength==1)
        {
          $finalstr = '';
          $finalstrlen = 0;
          // remover
          $p_geturl_0 = str_replace(";","",$get_nav_url[0]);
          $p_geturl_0 = str_replace("%","",$get_nav_url[0]);
          $p_geturl_0 = str_replace("&","",$get_nav_url[0]);
          $p_geturl_0 = str_replace("%5E","",$get_nav_url[0]);
          $p_geturl_0 = str_replace("^","",$get_nav_url[0]);
          $get_url_count = strlen($p_geturl_0);
          // ROUTER FILTER FILE - Filter Class File
          require_once '_sysconfig/Rout_filter.php';
          $r_filter = new Routfilter();
          if ($r_filter->rout_filter($get_url_count,$p_geturl_0,$nvarlength,'one'.$nvarlength)==true)
          {
            //echo "<font color=brown>router</font> first passed <br>";
            $finalstr = $r_filter->strdispatch();
            //echo $r_filter->strlendispatch();
            $finalstrlen = $r_filter->strlendispatch();
            //echo $r_filter->strdis;
            //echo $r_filter->strlendis;
            //echo "<br>";
            /*
            calling controllers | action time | filtered url gathered - FIRST LEVEL ONLY//
            */
            if ($finalstr=='recycle' && strlen($finalstr) == 7 && 7 == $finalstrlen && $finalstr!='tabload' || $finalstr=='RECYCLE' )
            {
              $file = '_controllers/collections.php';
          		if (file_exists($file))
              {
          			require $file;
                $ctrls = new collections();
                echo $ctrls->collections(strtolower($finalstr));
          		}
              else
              {
                // collection page unavailble
                echo '91L_err_';exit;header('location: /');
            		exit;
                // error reporter / page
          			$this->error();
          		}
            }
            if ($finalstr=='repair' && strlen($finalstr) == 6 && 6 == $finalstrlen && $finalstr!='tabload' || $finalstr=='repairing' )
            {
              $file = '_controllers/repair.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Repairing();
                echo $ctrls->Index(strtolower($finalstr));
              }
              else
              {
                // collection page unavailble
                echo 'L_err_';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if ($finalstr=='tabload' && strlen($finalstr) == 7 && 7 == $finalstrlen && $finalstr!='collections')
            {
              $file = '_controllers/tabload.php';
          		if (file_exists($file))
              {
          			require $file;
                $ctrls = new tabload();
                echo $ctrls->index(strtolower($finalstr));
          		}
              else
              {
                // index page not availble
                echo 'L_err_';exit;header('location: /');
            		exit;
                // error reporter / page
          			$this->error();
          		}
            }
            else if ($finalstr=='phones' && strlen($finalstr) == 6 && 6 == $finalstrlen && $finalstr!='collections')
            {
              $file = '_controllers/phones.php';
          		if (file_exists($file))
              {
          			require $file;
                $ctrls = new phones();
                echo $ctrls->index(strtolower($finalstr));
          		}
              else
              {
                // index page not availble
                echo 'L_err_';exit;header('location: /');
            		exit;
                // error reporter / page
          			$this->error();
          		}
            }
            else if ($finalstr=='login' && strlen($finalstr) == 5 && 5 == $finalstrlen && $finalstr!='index')
            {
              $file = '_controllers/login.php';
          		if (file_exists($file))
              {
          			require_once($file);
                $ctrls = new Login();
                echo $ctrls->login(strtolower($finalstr));
          		}
              else
              {
                // System Settings folder is unavailable
                echo 'L_err_';exit;header('location: /');
            		exit;
                // error reporter / page
          			$this->error();
          		}
            }
            else if ($finalstr=='logout' && strlen($finalstr) == 6 && 6 == $finalstrlen && $finalstr!='index')
            {
              $file = '_controllers/login.php';
          		if (file_exists($file))
              {
                require_once($file);
                $ctrls = new Login();
                echo $ctrls->login(strtolower($finalstr));
          		}
              else
              {
                // System Settings folder is unavailable
                echo 'L_err_';exit;header('location: /');
            		exit;
                // error reporter / page
          			$this->error();
          		}
            }
            else if ($finalstr=='sign-up' && strlen($finalstr) == 7 && 7 == $finalstrlen && $finalstr!='index')
            {
              $file = '_controllers/login.php';
          		if (file_exists($file))
              {
          			require_once $file;
                $ctrls = new login();
                echo $ctrls->login(strtolower($finalstr));
          		}
              else
              {
                // System Settings folder is unavailable
                echo 'L_err_';exit;header('location: /');
            		exit;
                // error reporter / page
          			$this->error();
          		}
            }
            else if ($finalstr=='index' && strlen($finalstr) == 5 && 5 == $finalstrlen && $finalstr!='sign-up')
            {
              $file = '_controllers/index.php';
          		if (file_exists($file))
              {
          			require $file;
                $ctrls = new Index();
                echo $ctrls->index(strtolower($finalstr));
          		}
              else
              {
                // System Settings folder is unavailable
                echo 'L_err_';exit;header('location: /');
            		exit;
                // error reporter / page
          			$this->error();
          		}
            }
            else if ($finalstr=='occoto-admin' && strlen($finalstr) == 12 && 12 == $finalstrlen && $finalstr!='sign-up' && $finalstr!='collections')
            {
              $file = '_controllers/occotoadmin.php';
          		if (file_exists($file))
              {
          			require $file;
                $ctrls = new OccotoAdmin();
                echo $ctrls->index(strtolower($finalstr));
          		}
              else
              {
                // System Settings folder is unavailable
                echo 'L_err_';exit;header('location: /');
            		exit;
                // error reporter / page
          			$this->error();
          		}
            }
            else if ($finalstr=='ship-mode' && strlen($finalstr) == 9 && 9 == $finalstrlen && $finalstr!='sign-up' && $finalstr!='collections')
            {
              $file = '_controllers/shipment.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Ship();
                echo $ctrls->index(strtolower($finalstr));
              }
              else
              {
                // System Settings folder is unavailable
                echo 'L_err_';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if ($finalstr=='finished' && strlen($finalstr) == 8 && 8 == $finalstrlen && $finalstr!='sign-up' && $finalstr!='collections')
            {
              $file = '_controllers/finished.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Finished();
                echo $ctrls->index(strtolower($finalstr));
              }
              else
              {
                // System Settings folder is unavailable
                echo 'L_err_';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if ($finalstr=='how-its-work' && strlen($finalstr) == 12 && 12 == $finalstrlen && $finalstr!='sign-up')
            {
              $file = '_controllers/content.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Content();
                echo $ctrls->index(strtolower($finalstr));
              }
              else
              {
                // System Settings folder is unavailable
                echo '289_err_';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if ($finalstr=='form' && strlen($finalstr) == 4 && 4 == $finalstrlen && $finalstr!='sign-up')
            {
              $file = '_controllers/form.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Forms();
                echo $ctrls->index(strtolower($finalstr));
              }
              else
              {
                // System Settings folder is unavailable
                echo 'L_err_';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if ($finalstr=='shop' && strlen($finalstr) == 4 && 4 == $finalstrlen && $finalstr!='sign-up')
            {
              $file = '_controllers/shop.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Shop();
                echo $ctrls->index(strtolower($finalstr));
              }
              else
              {
                // System Settings folder is unavailable
                echo '325_L_err_';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if ($finalstr=='contact' && strlen($finalstr) == 7 || $finalstr=='contactus' && strlen($finalstr) == 9)
            {
              $file = '_controllers/contact.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Contact();
                echo $ctrls->index(strtolower($finalstr));
              }
              else
              {
                // System Settings folder is unavailable
                echo '325_CTRL_MISSING_err';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if ($finalstr=='account' && strlen($finalstr) == 7)
            {
              $file = '_controllers/account.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Account();
                echo $ctrls->index(strtolower($finalstr));
              }
              else
              {
                // System Settings folder is unavailable
                echo '325_CTRL_MISSING_err';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if ($finalstr=='thankyou' && strlen($finalstr) == 8)
            {
              $file = '_controllers/thankyou.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new thankyou();
                echo $ctrls->index(strtolower($finalstr));
              }
              else
              {
                // System Settings folder is unavailable
                echo '361_CTRL_MISSING_err';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if ($finalstr=='howto' && strlen($finalstr) == 5)
            {
              $file = '_controllers/howto.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Howto();
                echo $ctrls->index(strtolower($finalstr));
              }
              else
              {
                // System Settings folder is unavailable
                echo '361_CTRL_MISSING_err';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if ($finalstr!='index' && $finalstr!='recycle' && $finalstr!='login' && $finalstr!='login' && $finalstr!='phones' && $finalstr!='tabload')
            {
              header('location: /index/');
              exit;
              $file = '_controllers/rezeption.php';
          		if (file_exists($file))
              {
          			require $file;
                $ctrls = new rezeption();
                echo $ctrls->index(strtolower($finalstr));
          		}
              else
              {
                echo '326_err_';exit;header('location: /');
            		exit;
                // error reporter / page
          			$this->error();
          		}
            }
          }
          else if ($r_filter->rout_filter($get_url_count,$p_geturl_0,$nvarlength,'one'.$nvarlength)==false)
          {
            echo "<font color=brown>router</font> first failed ";
            $finalstr = $r_filter->strdispatch();
            $finalstrlen = $r_filter->strlendispatch();
            header('location: /'.strtolower($final_firststr).'/');
          }
        }
        else {
          echo '342_err_';exit;header('location: /');
          exit;
          //error reporter / page
          $this->error();
        }
      }
      // URL ROUT LEVELS - SECOND ROUT LEVEL
      else if (isset($get_nav_url[0]) && isset($get_nav_url[1]) && empty($get_nav_url[2]) && empty($get_nav_url[3]))
      {
        //echo "<font color=brown>router</font>Second Level<br>";
        if ($nvarlength == count($get_nav_url) && $nvarlength==2)
        {
          $final_firststr = '';
          $final_firststrlen = 0;
          $final_secondstr = '';
          $final_secondstrlen = 0;
          //_ FIRST FILTER REMOVER - ROUND 1
          $p_geturl_0 = str_replace(";","",$get_nav_url[0]);
          $p_geturl_0 = str_replace("%","",$get_nav_url[0]);
          $p_geturl_0 = str_replace("&","",$get_nav_url[0]);
          $p_geturl_0 = str_replace("%5E","",$get_nav_url[0]);
          $p_geturl_0 = str_replace("^","",$get_nav_url[0]);
          $get_url_count_0 = strlen($p_geturl_0);
          //_ FIRST FILTER REMOVER - ROUND 2
          $p_geturl_1 = str_replace(";","",$get_nav_url[1]);
          $p_geturl_1 = str_replace("%","",$get_nav_url[1]);
          $p_geturl_1 = str_replace("&","",$get_nav_url[1]);
          $p_geturl_1 = str_replace("%5E","",$get_nav_url[1]);
          $p_geturl_1 = str_replace("^","",$get_nav_url[1]);
          $get_url_count_1 = strlen($p_geturl_1);
          //_ ROUTER FILTER FILE - Filter Class File
          require_once '_sysconfig/Rout_filter.php';
          $r_filter = new Routfilter();
          // First Round
          if ($r_filter->rout_filter($get_url_count_0,$p_geturl_0,$nvarlength,'one'.$nvarlength)==true)
          {
            $final_firststr = $r_filter->strdispatch();
            $final_firststrlen = $r_filter->strlendispatch();
            /*
            calling controllers | action time | filtered url gathered - LEVEL SECOND ROUND FIRST//
            */
            if (strtolower($final_firststr)=='collections' && strlen(strtolower($final_firststr)) == 11 && 11 == $final_firststrlen && strtolower($final_firststr)!='index')
            {
              $file = '_controllers/collections.php';
              if (file_exists($file))
              { /* First Round - not need to do on first round */ }
              else
              {
                // collection page unavailble
                echo 'L_err_';exit;header('location: /');
                exit;
                // error reporter // page
                $this->error();
              }
            }
            if ($final_firststr=='repair' && strlen($final_firststr) == 6 && 6 == $final_firststrlen && $final_firststr!='tabload' || $final_firststr=='repairing' )
            {
              $file = '_controllers/repair.php';
              if (file_exists($file))
              {
                // require $file;
                // $ctrls = new Repairing();
                // echo $ctrls->Index(strtolower($finalstr));
              }
              else
              {
                // collection page unavailble
                echo '_err_';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='tabload' && strlen(strtolower($final_firststr)) == 7 && 7 == $final_firststrlen && strtolower($final_firststr)!='collections')
            {
              $file = '_controllers/tabload.php';
              if (file_exists($file))
              {/* First Round - not need to do on first round */}
              else
              {
                // index page not availble
                echo 'F_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='account' && strlen(strtolower($final_firststr)) == 7 && 7 == $final_firststrlen && strtolower($final_firststr)!='collections')
            {
              $file = '_controllers/account.php';
              if (file_exists($file))
              {/* First Round - not need to do on first round */}
              else
              {
                // index page not availble
                echo 'F_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='phones' && strlen($final_firststr) == 6 && 6 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/phones.php';
          		if (file_exists($file))
              {/* First Round - not need to do on first round */}
              else
              {
                // index page not availble
                echo 'F_err_';exit;header('location: /');
            		exit;
                // error reporter / page
          			$this->error();
          		}
            }
            else if (strtolower($final_firststr)=='login' && strlen(strtolower($final_firststr)) == 8 && 8 == $final_firststrlen && strtolower($final_firststr)!='index' || $final_firststr=='login' && strlen($final_firststr) == 7 && 7 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/login.php';
              if (file_exists($file))
              {/* First Round - not need to do on first round */}
              else
              {
                // System Settings folder is unavailable
                echo 'F_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='sign-up' && strlen($final_firststr) == 7 && 7 == $final_firststrlen && $final_firststr!='index' || $final_firststr=='login' && strlen($final_firststr) == 7 && 7 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/login.php';
          		if (file_exists($file))
              {
          			// require $file;
                // $ctrls = new dashboard();
                // echo $ctrls->login(strtolower($finalstr));
          		}
              else
              {
                // System Settings folder is unavailable
                echo 'F_err_';exit;header('location: /');
            		exit;
                // error reporter / page
          			$this->error();
          		}
            }
            else if ($final_firststr=='occoto-admin' && strlen($final_firststr) == 12 && 12 == $final_firststrlen && $final_firststr!='index' || $final_firststr=='login' && strlen($final_firststr) == 12 && 12 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/occotoadmin.php';
          		if (file_exists($file))
              {
          			// require $file;
                // $ctrls = new dashboard();
                // echo $ctrls->login(strtolower($finalstr));
          		}
              else
              {
                // System Settings folder is unavailable
                echo 'F_err_';exit;header('location: /');
            		exit;
                // error reporter / page
          			$this->error();
          		}
            }
            else if ($final_firststr=='form' && strlen($final_firststr) == 4 && 4 == $final_firststrlen && $final_firststr!='sign-up')
            {
              $file = '_controllers/form.php';
              if (file_exists($file))
              {
                // require $file;
                // $ctrls = new Forms();
                // echo $ctrls->index(strtolower($finalstr));
              }
              else
              {
                // System Settings folder is unavailable
                echo 'F_err_';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)!='index' && strtolower($final_firststr)!='collections' && strtolower($final_firststr)!='login' && strtolower($final_firststr)!='login')
            {
              exit;
              $file = '_controllers/rezeption.php';
              if (file_exists($file))
              {/* First Round - not need to do on first round */}
              else
              {
                echo 'F_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
          }
          // Failed on Round One
          else if ($r_filter->rout_filter($get_url_count_0,$p_geturl_0,$nvarlength,'one'.$nvarlength)==false)
          {
            echo "<font color=brown>router</font> first failed ";
            $final_secondstr = $r_filter->strdispatch();
            $final_secondstrlen = $r_filter->strlendispatch();
          }
          // Second Round
          if ($r_filter->rout_filter($get_url_count_1,$p_geturl_1,$nvarlength,'two'.$nvarlength)==true)
          {
            $final_secondstr = $r_filter->strdispatch();
            $final_secondstrlen = $r_filter->strlendispatch();
            /*
            calling controllers | action time | filtered url gathered - LEVEL SECOND ROUND FIRST//
            */
            // Prepare URL - First Round + ^^ + Second Round -------
            $prepare_URL = $final_firststr.'^^'.$final_secondstr; //
            // Prepare URL - First Round + ^^ + Second Round -------
            if (strtolower($final_firststr)=='collections' && strlen(strtolower($final_firststr)) == 11 && 11 == $final_firststrlen && strtolower($final_firststr)!='index')
            {
              $file = '_controllers/collections.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new collections();
                echo $ctrls->collections(strtolower($prepare_URL));
              }
              else
              {
                // collection page unavailble
                echo 'F_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='tabload' && strlen($final_firststr) == 7 && 7 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/tabload.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new tabload();
                echo $ctrls->index(strtolower($final_secondstr));
              }
              else
              {
                // index page not availble
                echo 'F_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            if ($final_firststr=='repair' && strlen($final_firststr) == 6 && 6 == $final_firststrlen && $final_firststr!='tabload' || $final_firststr=='repairing' )
            {
              $file = '_controllers/repair.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Repairing();
                echo $ctrls->Index(strtolower($prepare_URL));
              }
              else
              {
                // collection page unavailble
                echo 'F_err_';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='phones' && strlen($final_firststr) == 6 && 6 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/phones.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new phones();
                echo $ctrls->index(strtolower($prepare_URL));
              }
              else
              {
                // index page not availble
                echo 'F_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='account' && strlen($final_firststr) == 7 && 7 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/account.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new account();
                echo $ctrls->index(strtolower($prepare_URL));
              }
              else
              {
                // index page not availble
                echo 'F_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='login' && strlen($final_firststr) == 5 && 5 == $final_firststrlen && $final_firststr!='index' || $final_firststr=='login' && strlen($final_firststr) == 7 && 7 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/login.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Login();
                echo $ctrls->login(strtolower($final_firststr));
              }
              else
              {
                // System Settings folder is unavailable
                echo 'F_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='sign-up' && strlen($final_firststr) == 7 && 7 == $final_firststrlen && $final_firststr!='index' || $final_firststr=='login' && strlen($final_firststr) == 7 && 7 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/login.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Login();
                echo $ctrls->login(strtolower($final_firststr));
              }
              else
              {
                // System Settings folder is unavailable
                echo 'F_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='occoto-admin' && strlen($final_firststr) == 12 && 12 == $final_firststrlen && $final_firststr!='index' || $final_firststr=='login' && strlen($final_firststr) == 12 && 12 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/occotoadmin.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new OccotoAdmin();
                //echo $prepare_URL;
                echo $ctrls->index(strtolower($prepare_URL));
              }
              else
              {
                // System Settings folder is unavailable
                echo 'F_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='form' && strlen($final_firststr) == 4 && 4 == $final_firststrlen && $final_firststr!='sign-up')
            {
              $file = '_controllers/form.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Forms();
                echo $ctrls->index(strtolower($prepare_URL));
              }
              else
              {
                // System Settings folder is unavailable
                echo 'F_err_';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr!='index' && $final_firststr!='collections' && $final_firststr!='login' && $final_firststr!='login')
            {
              $file = '_controllers/rezeption.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new rezeption();
                echo $ctrls->index(strtolower($prepare_URL));
              }
              else
              {
                echo 'F_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
          }
          // Failed on Round Two
          else if ($r_filter->rout_filter($get_url_count_1,$p_geturl_1,$nvarlength,'two'.$nvarlength)==false)
          {
            echo "<font color=brown>router</font> second failed ";
            $final_secondstr = $r_filter->strdispatch();
            $final_secondstrlen = $r_filter->strlendispatch();
            header('location: /'.strtolower($final_firststr).'/');
          }
        }
        else
        {
          echo 'F_err_';exit;header('location: /');
          exit;
          //error reporter / page
          $this->error();
        }
      }
      // URL ROUT LEVELS - THIRD ROUT LEVEL
      else if (isset($get_nav_url[0]) && isset($get_nav_url[1]) && isset($get_nav_url[2]) && empty($get_nav_url[3]))
      {
        //echo "<font color=brown>router</font>Third Level<br>";
        if ($nvarlength == count($get_nav_url) && $nvarlength==3)
        {
          $final_firststr = '';
          $final_firststrlen = 0;
          $final_secondstr = '';
          $final_secondstrlen = 0;
          $final_thirdstr = '';
          $final_thirdstrlen = 0;
          //_ FIRST FILTER REMOVER - ROUND 1
          $p_geturl_0 = str_replace(";","",$get_nav_url[0]);
          $p_geturl_0 = str_replace("%","",$get_nav_url[0]);
          $p_geturl_0 = str_replace("&","",$get_nav_url[0]);
          $p_geturl_0 = str_replace("%5E","",$get_nav_url[0]);
          $p_geturl_0 = str_replace("^","",$get_nav_url[0]);
          $get_url_count_0 = strlen($p_geturl_0);
          //_ FIRST FILTER REMOVER - ROUND 2
          $p_geturl_1 = str_replace(";","",$get_nav_url[1]);
          $p_geturl_1 = str_replace("%","",$get_nav_url[1]);
          $p_geturl_1 = str_replace("&","",$get_nav_url[1]);
          $p_geturl_1 = str_replace("%5E","",$get_nav_url[1]);
          $p_geturl_1 = str_replace("^","",$get_nav_url[1]);
          $get_url_count_1 = strlen($p_geturl_1);
          //_ FIRST FILTER REMOVER - ROUND 3
          $p_geturl_2 = str_replace(";","",$get_nav_url[2]);
          $p_geturl_2 = str_replace("%","",$get_nav_url[2]);
          $p_geturl_2 = str_replace("&","",$get_nav_url[2]);
          $p_geturl_2 = str_replace("%5E","",$get_nav_url[2]);
          $p_geturl_2 = str_replace("^","",$get_nav_url[2]);
          $get_url_count_2 = strlen($p_geturl_2);
          //_ ROUTER FILTER FILE - Filter Class File
          require_once '_sysconfig/Rout_filter.php';
          $r_filter = new Routfilter();
          // First Round
          if ($r_filter->rout_filter($get_url_count_0,$p_geturl_0,$nvarlength,'one'.$nvarlength)==true)
          {
            //echo "<font color=brown>router</font> first passed <br>";
            $final_firststr = $r_filter->strdispatch();
            $final_firststrlen = $r_filter->strlendispatch();
            /*
            calling controllers | action time | filtered url gathered - LEVEL SECOND ROUND FIRST//
            */
            if ($final_firststr=='collections' && strlen($final_firststr) == 11 && 11 == $final_firststrlen && $final_firststr!='index')
            {
              $file = '_controllers/collections.php';
              if (file_exists($file))
            { /* First Round - not need to do on first round */ }
              else
              {
                // collection page unavailble
                echo 'F_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='index' && strlen($final_firststr) == 5 && 5 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/index.php';
              if (file_exists($file))
              {/* First Round - not need to do on first round */}
              else
              {
                // index page not availble
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            if ($final_firststr=='repair' && strlen($final_firststr) == 6 && 6 == $final_firststrlen && $final_firststr!='tabload' || $final_firststr=='repairing' )
            {
              $file = '_controllers/repair.php';
              if (file_exists($file))
              {
                // require $file;
                // $ctrls = new Repairing();
                // echo $ctrls->Index(strtolower($prepare_URL));
              }
              else
              {
                // collection page unavailble
                echo '_err_';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='phones' && strlen($final_firststr) == 6 && 6 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/phones.php';
              if (file_exists($file))
              {
                // require $file;
                // $ctrls = new phones();
                // echo $ctrls->index(strtolower($final_thirdstr));
              }
              else
              {
                // index page not availble
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='login' && strlen($final_firststr) == 5 && 5 == $final_firststrlen && $final_firststr!='index' || $final_firststr=='login' && strlen($final_firststr) == 7 && 7 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/login.php';
              if (file_exists($file))
              {/* First Round - not need to do on first round */}
              else
              {
                // System Settings folder is unavailable
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='occoto-admin' && strlen($final_firststr) == 12 && 12 == $final_firststrlen && $final_firststr!='index' || $final_firststr=='login' && strlen($final_firststr) == 7 && 7 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/occotoadmin.php';
              if (file_exists($file))
              {/* First Round - not need to do on first round */}
              else
              {
                // System Settings folder is unavailable
                //echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='form' && strlen($final_firststr) == 4 && 4 == $final_firststrlen && $final_firststr!='sign-up')
            {
              $file = '_controllers/form.php';
              if (file_exists($file))
              {
                // require $file;
                // $ctrls = new Forms();
                // echo $ctrls->index(strtolower($prepare_URL));
              }
              else
              {
                // System Settings folder is unavailable
                echo '_err_';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr!='index' && $final_firststr!='collections' && $final_firststr!='login' && $final_firststr!='login')
            {
              $file = '_controllers/rezeption.php';
              if (file_exists($file))
              {/* First Round - not need to do on first round */}
              else
              {
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
          }
          // Failed on Round One
          else if ($r_filter->rout_filter($get_url_count_0,$p_geturl_0,$nvarlength,'one'.$nvarlength)==false)
          {
            echo "<font color=brown>router</font> first failed ";
            $final_secondstr = $r_filter->strdispatch();
            $final_secondstrlen = $r_filter->strlendispatch();
          }
          // Second Round
          if ($r_filter->rout_filter($get_url_count_1,$p_geturl_1,$nvarlength,'two'.$nvarlength)==true)
          {
            //echo "<font color=brown>router</font> second passed  <br>";
            $final_secondstr = $r_filter->strdispatch();
            $final_secondstrlen = $r_filter->strlendispatch();
            /*
            calling controllers | action time | filtered url gathered - LEVEL SECOND ROUND FIRST//
            */
            // Prepare URL - First Round + ^^ + Second Round -------
            //$prepare_URL = $final_firststr.'^^'.$final_secondstr; //
            // Prepare URL - First Round + ^^ + Second Round -------
            if (strtolower($final_firststr)=='collections' && strlen(strtolower($final_firststr)) == 11 && 11 == $final_firststrlen && strtolower($final_firststr)!='index')
            {
              $file = '_controllers/collections.php';
              if (file_exists($file))
              {
                // forward to third round
                //require $file;
                //$ctrls = new collections();
                //echo $ctrls->collections($prepare_URL);
              }
              else
              {
                // collection page unavailble
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='index' && strlen(strtolower($final_firststr)) == 5 && 5 == $final_firststrlen && strtolower($final_firststr)!='collections')
            {
              $file = '_controllers/index.php';
              if (file_exists($file))
              {
                // forward to third round
                // require $file;
                // $ctrls = new Index();
                // echo $ctrls->index($final_firststr);
              }
              else
              {
                // index page not availble
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            if ($final_firststr=='repair' && strlen($final_firststr) == 6 && 6 == $final_firststrlen && $final_firststr!='tabload' || $final_firststr=='repairing' )
            {
              $file = '_controllers/repair.php';
              if (file_exists($file))
              {
                // require $file;
                // $ctrls = new Repairing();
                // echo $ctrls->Index(strtolower($prepare_URL));
              }
              else
              {
                // collection page unavailble
                echo '_err_';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='phones' && strlen($final_firststr) == 6 && 6 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/phones.php';
              if (file_exists($file))
              {
                // require $file;
                // $ctrls = new phones();
                // echo $ctrls->index(strtolower($final_thirdstr));
              }
              else
              {
                // index page not availble
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='login' && strlen(strtolower($final_firststr)) == 5 && 5 == $final_firststrlen && strtolower($final_firststr)!='index' || strtolower($final_firststr)=='login' && strlen(strtolower($final_firststr)) == 7 && 7 == $final_firststrlen && strtolower($final_firststr)!='collections')
            {
              $file = '_controllers/login.php';
              if (file_exists($file))
              {
                // forward to third round
                // require $file;
                // $ctrls = new Login();
                // echo $ctrls->login($final_firststr);
              }
              else
              {
                // System Settings folder is unavailable
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='occoto-admin' && strlen(strtolower($final_firststr)) == 12 && 12 == $final_firststrlen && strtolower($final_firststr)!='index' || strtolower($final_firststr)=='login' && strlen(strtolower($final_firststr)) == 7 && 7 == $final_firststrlen && strtolower($final_firststr)!='collections')
            {
              $file = '_controllers/occotoadmin.php';
              if (file_exists($file))
              {
                // forward to third round
                // require $file;
                // $ctrls = new Login();
                // echo $ctrls->login($final_firststr);
              }
              else
              {
                // System Settings folder is unavailable
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='form' && strlen($final_firststr) == 4 && 4 == $final_firststrlen && $final_firststr!='sign-up')
            {
              $file = '_controllers/form.php';
              if (file_exists($file))
              {
                // require $file;
                // $ctrls = new Forms();
                // echo $ctrls->index(strtolower($prepare_URL));
              }
              else
              {
                // System Settings folder is unavailable
                echo '_err_';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)!='index' && strtolower($final_firststr)!='collections' && strtolower($final_firststr)!='login' && strtolower($final_firststr)!='login')
            {
              $file = '_controllers/rezeption.php';
              if (file_exists($file))
              {
                // forward to third round
                // require $file;
                // $ctrls = new rezeption();
                // echo $ctrls->index($prepare_URL);
              }
              else
              {
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
          }
          // Failed on Round Two
          else if ($r_filter->rout_filter($get_url_count_1,$p_geturl_1,$nvarlength,'two'.$nvarlength)==false)
          {
            //echo "<font color=brown>router</font> second failed ";
            $final_secondstr = $r_filter->strdispatch();
            $final_secondstrlen = $r_filter->strlendispatch();
            header('location: /'.strtolower($final_firststr).'/');
          }
          // Third Round
          if ($r_filter->rout_filter($get_url_count_2,$p_geturl_2,$nvarlength,'three'.$nvarlength)==true)
          {
            //echo "<font color=brown>router</font> third passed  <br>";
            $final_thirdstr = $r_filter->strdispatch();
            $final_thirdstrlen = $r_filter->strlendispatch();
            /*
            calling controllers | action time | filtered url gathered - LEVEL SECOND ROUND FIRST//
            */
            // Prepare URL - First Round + ^^ + Second Round -------
            $prepare_URL = $final_firststr.'^^'.$final_secondstr.'^^'.$final_thirdstr; //
            // Prepare URL - First Round + ^^ + Second Round -------
            if (strtolower($final_firststr)=='collections' && strlen(strtolower($final_firststr)) == 11 && 11 == $final_firststrlen && strtolower($final_firststr)!='index')
            {
              $file = '_controllers/collections.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new collections();
                echo $ctrls->collections(strtolower($prepare_URL));
              }
              else
              {
                // collection page unavailble
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='index' && strlen(strtolower($final_firststr)) == 5 && 5 == $final_firststrlen && strtolower($final_firststr)!='collections')
            {
              $file = '_controllers/index.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Index();
                echo $ctrls->index(strtolower($final_firststr));
              }
              else
              {
                // index page not availble
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            if ($final_firststr=='repair' && strlen($final_firststr) == 6 && 6 == $final_firststrlen && $final_firststr!='tabload' || $final_firststr=='repairing' )
            {
              $file = '_controllers/repair.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Repairing();
                echo $ctrls->Index(strtolower($prepare_URL));
              }
              else
              {
                // collection page unavailble
                echo '_err_';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='phones' && strlen($final_firststr) == 6 && 6 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/phones.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new phones();
                echo $ctrls->index(strtolower($prepare_URL));
              }
              else
              {
                // index page not availble
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='login' && strlen(strtolower($final_firststr)) == 5 && 5 == $final_firststrlen && strtolower($final_firststr)!='index' || strtolower($final_firststr)=='login' && strlen(strtolower($final_firststr)) == 7 && 7 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/login.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Login();
                echo $ctrls->login(strtolower($final_firststr));
              }
              else
              {
                // System Settings folder is unavailable
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='occoto-admin' && strlen(strtolower($final_firststr)) == 12 && 12 == $final_firststrlen && strtolower($final_firststr)!='index' || strtolower($final_firststr)=='login' && strlen(strtolower($final_firststr)) == 12 && 12 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/occotoadmin.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new OccotoAdmin();
                echo $ctrls->index(strtolower($prepare_URL));
              }
              else
              {
                // System Settings folder is unavailable
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='apps' && strlen(strtolower($final_firststr)) == 4 && 4 == $final_firststrlen && strtolower($final_firststr)!='index' || strtolower($final_firststr)=='login' && strlen(strtolower($final_firststr)) == 12 && 12 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/apps.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Apps();
                // echo $prepare_URL;
                // exit;
                echo $ctrls->index(strtolower($prepare_URL));
              }
              else
              {
                // System Settings folder is unavailable
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='form' && strlen($final_firststr) == 4 && 4 == $final_firststrlen && $final_firststr!='sign-up')
            {
              $file = '_controllers/form.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Forms();
                echo $ctrls->index(strtolower($prepare_URL));
              }
              else
              {
                // System Settings folder is unavailable
                echo '1221_err_';exit;header('location: /');
                exit;
                // error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)!='index' && strtolower($final_firststr)!='collections' && strtolower($final_firststr)!='login' && strtolower($final_firststr)!='login')
            {
              echo "Please input valid.(LTTSECURITY_SAY)";exit;
              $file = '_controllers/rezeption.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new rezeption();
                echo $ctrls->index(strtolower($prepare_URL));
              }
              else
              {
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
          }
          // Failed on Round Three
          else if ($r_filter->rout_filter($get_url_count_2,$p_geturl_2,$nvarlength,'three'.$nvarlength)==false)
          {
            echo "<font color=brown>router</font> second failed ";
            $final_secondstr = $r_filter->strdispatch();
            $final_secondstrlen = $r_filter->strlendispatch();
            header('location: /'.$final_firststr.'/'.$final_secondstr);
          }
        }
        else {
          echo '1256_err_';exit;header('location: /');
          exit;
          //error reporter / page
          $this->error();
        }
      }
      // URL ROUT LEVELS - FORTH ROUT LEVEL
      else if (isset($get_nav_url[0]) && isset($get_nav_url[1]) && isset($get_nav_url[2]) && isset($get_nav_url[3]))
      {
        //echo "<font color=brown>router</font>Forth Level<br>";
        if ($nvarlength == count($get_nav_url) && $nvarlength==4)
        {
          $final_firststr = '';
          $final_firststrlen = 0;
          $final_secondstr = '';
          $final_secondstrlen = 0;
          $final_thirdstr = '';
          $final_thirdstrlen = 0;
          $final_fourstr = '';
          $final_fourstrlen = 0;
          //_ FIRST FILTER REMOVER - ROUND 1
          $p_geturl_0 = str_replace(";","",$get_nav_url[0]);
          $p_geturl_0 = str_replace("%","",$get_nav_url[0]);
          $p_geturl_0 = str_replace("&","",$get_nav_url[0]);
          $p_geturl_0 = str_replace("%5E","",$get_nav_url[0]);
          $p_geturl_0 = str_replace("^","",$get_nav_url[0]);
          $get_url_count_0 = strlen($p_geturl_0);
          //_ FIRST FILTER REMOVER - ROUND 2
          $p_geturl_1 = str_replace(";","",$get_nav_url[1]);
          $p_geturl_1 = str_replace("%","",$get_nav_url[1]);
          $p_geturl_1 = str_replace("&","",$get_nav_url[1]);
          $p_geturl_1 = str_replace("%5E","",$get_nav_url[1]);
          $p_geturl_1 = str_replace("^","",$get_nav_url[1]);
          $get_url_count_1 = strlen($p_geturl_1);
          //_ FIRST FILTER REMOVER - ROUND 3
          $p_geturl_2 = str_replace(";","",$get_nav_url[2]);
          $p_geturl_2 = str_replace("%","",$get_nav_url[2]);
          $p_geturl_2 = str_replace("&","",$get_nav_url[2]);
          $p_geturl_2 = str_replace("%5E","",$get_nav_url[2]);
          $p_geturl_2 = str_replace("^","",$get_nav_url[2]);
          $get_url_count_2 = strlen($p_geturl_2);
          //_ FIRST FILTER REMOVER - ROUND 4
          $p_geturl_3 = str_replace(";","",$get_nav_url[3]);
          $p_geturl_3 = str_replace("%","",$get_nav_url[3]);
          $p_geturl_3 = str_replace("&","",$get_nav_url[3]);
          $p_geturl_3 = str_replace("%5E","",$get_nav_url[3]);
          $p_geturl_3 = str_replace("^","",$get_nav_url[3]);
          $get_url_count_3 = strlen($p_geturl_3);
          //_ ROUTER FILTER FILE - Filter Class File
          require_once '_sysconfig/Rout_filter.php';
          $r_filter = new Routfilter();
          // First Round
          if ($r_filter->rout_filter($get_url_count_0,$p_geturl_0,$nvarlength,'one'.$nvarlength)==true)
          {
            //"<font color=brown>router</font> first passed <br>";
            $final_firststr = $r_filter->strdispatch();
            $final_firststrlen = $r_filter->strlendispatch();
            /*
            calling controllers | action time | filtered url gathered - LEVEL SECOND ROUND FIRST//
            */
            if (strtolower($final_firststr)=='collections' && strlen(strtolower($final_firststr)) == 11 && 11 == $final_firststrlen && strtolower($final_firststr)!='index')
            {
              $file = '_controllers/collections.php';
              if (file_exists($file))
            { /* First Round - not need to do on first round */ }
              else
              {
                // collection page unavailble
                echo '1324_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='index' && strlen(strtolower($final_firststr)) == 5 && 5 == $final_firststrlen && strtolower($final_firststr)!='collections')
            {
              $file = '_controllers/index.php';
              if (file_exists($file))
              {/* First Round - not need to do on first round */}
              else
              {
                // index page not availble
                echo '1338_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='login' && strlen($final_firststr) == 5 && 5 == $final_firststrlen && strtolower($final_firststr)!='index' || strtolower($final_firststr)=='login' && strlen($final_firststr) == 7 && 7 == $final_firststrlen && strtolower($final_firststr)!='collections')
            {
              $file = '_controllers/login.php';
              if (file_exists($file))
              {/* First Round - not need to do on first round */}
              else
              {
                // System Settings folder is unavailable
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='app' && strlen($final_firststr) == 3 && 3 == $final_firststrlen && strtolower($final_firststr)!='index' || strtolower($final_firststr)=='login' && strlen($final_firststr) == 7 && 7 == $final_firststrlen && strtolower($final_firststr)!='collections')
            {
              $file = '_controllers/login.php';
              if (file_exists($file))
              {/* First Round - not need to do on first round */}
              else
              {
                // System Settings folder is unavailable
                echo '1366_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='form' && strlen($final_firststr) == 4 && 4 == $final_firststrlen && strtolower($final_firststr)!='index')
            {
              $file = '_controllers/form.php';
              if (file_exists($file))
              {/* First Round - not need to do on first round */}
              else
              {
                // System Settings folder is unavailable
                echo '1380_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)!='index' && strtolower($final_firststr)!='collections' && strtolower($final_firststr)!='login' && strtolower($final_firststr)!='login')
            {
              $file = '_controllers/rezeption.php';
              if (file_exists($file))
              {/* First Round - not need to do on first round */}
              else
              {
                echo '1393_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
          }
          // Failed on Round One
          else if ($r_filter->rout_filter($get_url_count_0,$p_geturl_0,$nvarlength,'one'.$nvarlength)==false)
          {
            echo "<font color=brown>router</font> first failed ";
            $final_secondstr = $r_filter->strdispatch();
            $final_secondstrlen = $r_filter->strlendispatch();
          }
          // Second Round
          if ($r_filter->rout_filter($get_url_count_1,$p_geturl_1,$nvarlength,'two'.$nvarlength)==true)
          {
            //echo " second passed  <br>";
            $final_secondstr = $r_filter->strdispatch();
            $final_secondstrlen = $r_filter->strlendispatch();
            /*
            calling controllers | action time | filtered url gathered - LEVEL SECOND ROUND FIRST//
            */
            // Prepare URL - First Round + ^^ + Second Round -------
            //$prepare_URL = $final_firststr.'^^'.$final_secondstr; //
            // Prepare URL - First Round + ^^ + Second Round -------
            if (strtolower($final_firststr)=='collections' && strlen($final_firststr) == 11 && 11 == $final_firststrlen && strtolower($final_firststr)!='index')
            {
              $file = '_controllers/collections.php';
              if (file_exists($file))
              {
                // forward to third round
                //require $file;
                //$ctrls = new collections();
                //echo $ctrls->collections($prepare_URL);
              }
              else
              {
                // collection page unavailble
                echo '1432_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='index' && strlen($final_firststr) == 5 && 5 == $final_firststrlen && strtolower($final_firststr)!='collections')
            {
              $file = '_controllers/index.php';
              if (file_exists($file))
              {
                // forward to third round
                // require $file;
                // $ctrls = new Index();
                // echo $ctrls->index($final_firststr);
              }
              else
              {
                // index page not availble
                echo '1451_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='login' && strlen($final_firststr) == 5 && 5 == $final_firststrlen && strtolower($final_firststr)!='index' || strtolower($final_firststr)=='login' && strlen($final_firststr) == 7 && 7 == $final_firststrlen && strtolower($final_firststr)!='collections')
            {
              $file = '_controllers/login.php';
              if (file_exists($file))
              {
                // forward to third round
                // require $file;
                // $ctrls = new Login();
                // echo $ctrls->login($final_firststr);
              }
              else
              {
                // System Settings folder is unavailable
                echo '1470_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='app' && strlen($final_firststr) == 3 && 3 == $final_firststrlen && strtolower($final_firststr)!='index' || strtolower($final_firststr)=='login' && strlen($final_firststr) == 7 && 7 == $final_firststrlen && strtolower($final_firststr)!='collections')
            {
              $file = '_controllers/login.php';
              if (file_exists($file))
              {
                // forward to third round
                // require $file;
                // $ctrls = new Login();
                // echo $ctrls->login($final_firststr);
              }
              else
              {
                // System Settings folder is unavailable
                echo '1489_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='form' && strlen($final_firststr) == 4 && 4 == $final_firststrlen && strtolower($final_firststr)!='index')
            {
              $file = '_controllers/form.php';
              if (file_exists($file))
              {
                // forward to third round
                // require $file;
                // $ctrls = new Login();
                // echo $ctrls->login($final_firststr);
              }
              else
              {
                // System Settings folder is unavailable
                echo '1508_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)!='index' && strtolower($final_firststr)!='collections' && strtolower($final_firststr)!='login' && strtolower($final_firststr)!='login')
            {
              $file = '_controllers/rezeption.php';
              if (file_exists($file))
              {
                // forward to third round
                // require $file;
                // $ctrls = new rezeption();
                // echo $ctrls->index($prepare_URL);
              }
              else
              {
                echo '1526_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
          }
          // Failed on Round Two
          else if ($r_filter->rout_filter($get_url_count_1,$p_geturl_1,$nvarlength,'two'.$nvarlength)==false)
          {
            echo "<font color=brown>router</font> second failed ";
            $final_secondstr = $r_filter->strdispatch();
            $final_secondstrlen = $r_filter->strlendispatch();
            header('location: /'.strtolower($final_firststr).'/');
          }
          // Third Round
          if ($r_filter->rout_filter($get_url_count_2,$p_geturl_2,$nvarlength,'three'.$nvarlength)==true)
          {
            //echo "<font color=brown>router</font> third passed  <br>";
            $final_thirdstr = $r_filter->strdispatch();
            $final_thirdstrlen = $r_filter->strlendispatch();
            /*
            calling controllers | action time | filtered url gathered - LEVEL SECOND ROUND FIRST//
            */
            // Prepare URL - First Round + ^^ + Second Round -------
            //$prepare_URL = $final_firststr.'^^'.$final_secondstr.'^^'.$final_thirdstr; //
            // Prepare URL - First Round + ^^ + Second Round -------
            if (strtolower($final_firststr)=='collections' && strlen($final_firststr) == 11 && 11 == $final_firststrlen && strtolower($final_firststr)!='index')
            {
              $file = '_controllers/collections.php';
              if (file_exists($file))
              {
                // forward to round 4
                // require $file;
                // $ctrls = new collections();
                // echo $ctrls->collections($prepare_URL);
              }
              else
              {
                // collection page unavailble
                echo '1566_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='index' && strlen($final_firststr) == 5 && 5 == $final_firststrlen && strtolower($final_firststr)!='collections')
            {
              $file = '_controllers/index.php';
              if (file_exists($file))
              {
                // forward to round 4
                // require $file;
                // $ctrls = new Index();
                // echo $ctrls->index($final_firststr);
              }
              else
              {
                // index page not availble
                echo '1585_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='login' && strlen($final_firststr) == 5 && 5 == $final_firststrlen && strtolower($final_firststr)!='index' || strtolower($final_firststr)=='login' && strlen($final_firststr) == 7 && 7 == $final_firststrlen && strtolower($final_firststr)!='collections')
            {
              $file = '_controllers/login.php';
              if (file_exists($file))
              {
                // forward to round 4
                // require $file;
                // $ctrls = new Login();
                // echo $ctrls->login($final_firststr);
              }
              else
              {
                // System Settings folder is unavailable
                echo '1604_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='app' && strlen($final_firststr) == 3 && 3 == $final_firststrlen && strtolower($final_firststr)!='index')
            {
              $file = '_controllers/apps.php';
              if (file_exists($file))
              {
                // forward to round 4
                // require $file;
                // $ctrls = new Login();
                // echo $ctrls->login($final_firststr);
              }
              else
              {
                // System Settings folder is unavailable
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='form' && strlen($final_firststr) == 4 && 4 == $final_firststrlen && strtolower($final_firststr)!='index')
            {
              $file = '_controllers/form.php';
              if (file_exists($file))
              {
                // forward to round 4
                // require $file;
                // $ctrls = new Login();
                // echo $ctrls->login($final_firststr);
              }
              else
              {
                // System Settings folder is unavailable
                echo '_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)!='index' && strtolower($final_firststr)!='collections' && strtolower($final_firststr)!='login' && strtolower($final_firststr)!='login')
            {
              //echo "here";
              $file = '_controllers/rezeption.php';
              if (file_exists($file))
              {
                // forward to round 4
                // require $file;
                // $ctrls = new rezeption();
                // echo $ctrls->index($prepare_URL);
              }
              else
              {
                echo '1661L_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
          }
          // Failed on Round Three
          else if ($r_filter->rout_filter($get_url_count_2,$p_geturl_2,$nvarlength,'three'.$nvarlength)==false)
          {
            echo "<font color=brown>router</font> three failed ";
            $final_secondstr = $r_filter->strdispatch();
            $final_secondstrlen = $r_filter->strlendispatch();
            header('location: /'.strtolower($final_firststr).'/'.strtolower($final_secondstr));
          }
          // Forth Round
          if ($r_filter->rout_filter($get_url_count_3,$p_geturl_3,$nvarlength,'four'.$nvarlength)==true)
          {
            //echo "<font color=brown>router</font> forth passed  <br>";
            $final_fourstr = $r_filter->strdispatch();
            $final_fourstrlen = $r_filter->strlendispatch();
            /*
            calling controllers | action time | filtered url gathered - LEVEL SECOND ROUND FIRST//
            */
            // Prepare URL - First Round + ^^ + Second Round ------- thrid and fourth
            $prepare_URL = $final_firststr.'^^'.$final_secondstr.'^^'.$final_thirdstr.'^^'.$final_fourstr; //
            // Prepare URL - First Round + ^^ + Second Round -------
            if (strtolower($final_firststr)=='collections' && strlen($final_firststr) == 11 && 11 == $final_firststrlen && strtolower($final_firststr)!='index')
            {
              $file = '_controllers/collections.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new collections();
                echo $ctrls->collections(strtolower($prepare_URL));
              }
              else
              {
                // collection page unavailble
                echo '1700_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='index' && strlen($final_firststr) == 5 && 5 == $final_firststrlen && strtolower($final_firststr)!='collections')
            {
              $file = '_controllers/index.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Index();
                echo $ctrls->index(strtolower($final_firststr));
              }
              else
              {
                // index page not availble
                echo '1718_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if ($final_firststr=='phones' && strlen($final_firststr) == 6 && 6 == $final_firststrlen && $final_firststr!='collections')
            {
              $file = '_controllers/phones.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new phones();
                echo $ctrls->index(strtolower($prepare_URL));
              }
              else
              {
                // index page not availble
                echo '1736_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='login' && strlen($final_firststr) == 5 && 5 == $final_firststrlen && strtolower($final_firststr)!='index' || strtolower($final_firststr)=='login' && strlen($final_firststr) == 7 && 7 == $final_firststrlen && strtolower($final_firststr)!='collections')
            {
              $file = '_controllers/login.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Login();
                echo $ctrls->login(strtolower($final_firststr));
              }
              else
              {
                // System Settings folder is unavailable
                echo '1754_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='app' && strlen($final_firststr) == 3 && 3 == $final_firststrlen && strtolower($final_firststr)!='index')
            {
              $file = '_controllers/apps.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Apps();
                echo $ctrls->index(strtolower($prepare_URL));
              }
              else
              {
                // System Settings folder is unavailable
                echo '1772_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='form' && strlen($final_firststr) == 4 && 4 == $final_firststrlen && strtolower($final_firststr)!='index')
            {
              $file = '_controllers/form.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new Forms();
                echo $ctrls->index(strtolower($prepare_URL));
              }
              else
              {
                echo "1789";exit;

                // System Settings folder is unavailable
                echo '1792L_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)=='occoto-admin' && strlen($final_firststr) == 12 && 12 == $final_firststrlen && strtolower($final_firststr)!='index')
            {
              $file = '_controllers/occotoadmin.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new OCCOTOADMIN();
                echo $ctrls->index(strtolower($prepare_URL));
              }
              else
              {
                echo "1789";exit;

                // System Settings folder is unavailable
                echo '1792L_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
            else if (strtolower($final_firststr)!='index' && strtolower($final_firststr)!='collections' && strtolower($final_firststr)!='login' && strtolower($final_firststr)!='login')
            {
              echo "ERROR_FOUND_ON_QUERY_REZ_BLOCK".$prepare_URL;exit;
              $file = '_controllers/rezeption.php';
              if (file_exists($file))
              {
                require $file;
                $ctrls = new rezeption();
                echo $ctrls->index(strtolower($prepare_URL));
              }
              else
              {
                echo "1808";exit;
                echo '1811L_err_';exit;header('location: /');
                exit;
                //error reporter / page
                $this->error();
              }
            }
          }
          // Failed on Round Four
          else if ($r_filter->rout_filter($get_url_count_3,$p_geturl_3,$nvarlength,'four'.$nvarlength)==false)
          {
            echo "<font color=brown>router</font> second failed ";
            $final_secondstr = $r_filter->strdispatch();
            $final_secondstrlen = $r_filter->strlendispatch();
            header('location: /'.strtolower($final_firststr).'/'.strtolower($final_secondstr));
          }
        }
        else {
          echo "1828: INVALID KEYWORD";exit;
          echo 'L_err_';exit;header('location: /');
          exit;
          //error reporter / page
          $this->error();
        }
      }
      // URL ROUT LEVELS - UN EXPECTED HERE - ZERO ROUT LEVEL
      else
      {
        echo "EXCEED 4";exit;
        echo "Zero Level<br>";
        echo "going to load main page";
        echo '1841L_err_';exit;header('location: /');
    		exit;
      }
    }
    // redirect 4+ level after to collections filhal
    else
    {
      //redirect default
      echo '1849: INVALID KEYWORD';exit;
      header('location: /');
      exit;
    }
  }
}

?>
