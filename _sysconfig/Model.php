<?php
/*------------------------------------------------------------------------------------------------------------------------
-------------------------------  RAD PHP - Rapid Application Development PHP framework  ---------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------  RAD PHP - AUTHOR: Muhammad Shariq Ayaz  ----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------  www.instagram.com/shariqayaztech | gr8shariq@gmail.com----------------------------------------
_________________________________________________________________________________________________________________________*/
/**
 *  META DATABASE REFLECTION - DATA REFLECTION
 */
class Model // extends DB_ACCESS
{
  public static function db_request($what,$arg_1,$arg_2=false,$arg_3=false,$arg_4=false)
  {
    $getwhat = $what;
    echo $arg_1;
    echo $what;
    echo $arg_1;
    echo $arg_2;
    echo $arg_3;
    echo $arg_4;
    $get_arg_1 = $arg_1;
    $str_arr = "";
    if ($getwhat=='chkPrice')
      $str_arr = explode('-_-',$get_arg_1);
    else if ($getwhat=='blank_sell_quot')
      $str_arr = explode('-_-',$get_arg_1);
    else if ($getwhat=='blank_repair_quot')
      $str_arr = explode('-_-',$get_arg_1);
    else if ($getwhat=='simple_repair_quot')
      $str_arr = explode('-_-',$get_arg_1);
    else if ($getwhat=='update_prod')
      $str_arr = explode('-_-',$get_arg_1);
    else if ($getwhat=='INST')
      $str_arr = $get_arg_1;
    else
      $exp_incomingdata = explode("^*^",$get_arg_1);
    //print_r ($exp_incomingdata);
    // multiple arg condition will be configured in future
    // ins-new-cap
    //
    switch ($getwhat) {
      case 'ins-new-cap':
        $user_id = DB_ACCESS::meta_user();
        $user_pass = DB_ACCESS::meta_pass();
        try
        {
          $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", "$user_id", "$user_pass");
          $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $st_insexec = $conn_meta->prepare("INSERT INTO  product_spec(prod_id, cap, new_cond_desc, good_cond_desc, working_cond_desc, broken_cond_desc, new_cond_unlocked, good_cond_unlocked, working_cond_unlocked, broken_cond_unlocked, new_cond_o2, good_cond_o2, working_cond_o2, broken_cond_o2, new_cond_ee, good_cond_ee, working_cond_ee, broken_cond_ee, new_cond_vodafone, good_cond_vodafone, working_cond_vodafone, broken_cond_vodafone, new_cond_orange, good_cond_orange, working_cond_orange, broken_cond_orange, new_cond_tmobile, good_cond_tmobile, working_cond_tmobile, broken_cond_tmobile, new_cond_3, good_cond_3, working_cond_3, broken_cond_3, new_cond_virgin, good_cond_virgin, working_cond_virgin, broken_cond_virgin, new_cond_tesco, good_cond_tesco, working_cond_tesco, broken_cond_tesco, new_cond_unknown, good_cond_unknown, working_cond_unknown, broken_cond_unknown)
          VALUES (:prodid,:cap,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')");
          $st_insexec->bindParam(':prodid', $arg_1, PDO::PARAM_STR);
          $st_insexec->bindParam(':cap', $arg_2, PDO::PARAM_STR);
          $st_insexec->execute();
          //var_dump($st_insexec);
        }
        catch (PDOException $e)
        {
          throw new Exception('Could not connect to database');
          echo 'Connection failed: ' . $e->getMessage(); // SHOWING DATABASE NAME
        }

        break;
        /////////////////////////////////////////////////////////ins-new-cap///////////////////////////////////////////////////////////////////////// - break INST
        case 'submit_req':
        $user_id = DB_ACCESS::meta_user();
        $user_pass = DB_ACCESS::meta_pass();
        try
        {
          // $order_tracking = rand();
          $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", "$user_id", "$user_pass");
          $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          // $st_insexec = $conn_meta->prepare("INSERT INTO `sell_request`(`s_model`, `s_cap`, `s_cond`, `s_name`, `s_email`, `s_mobile`, `s_address`, `s_message`, `s_ip`, `order_tracking`, `paymethod`, `paypalemail`, `bafullname`, `banumber`, `basortcode`) VALUES (:model,:cap,:cond,:name,:email,:mobile,:address,:message,:ip,:order_tracking,:paymethod,:paypalemail,:bafullname,:banumber,:basortcode)");
          $st_insexec = $conn_meta->prepare("INSERT INTO `sell_request`(`s_model`, `s_cap`, `s_cond`, `s_name`, `s_email`, `s_mobile`, `s_address`, `s_message`, `s_ip`, `paymethod`, `paypalemail`, `bafullname`, `banumber`, `basortcode`, `order_tracking`) VALUES (:model,:cap,:cond,:name,:email,:mobile,:address,:message,:ip,:paymethod,:paypalemail,:bafullname,:banumber,:basortcode,:order_tracking)");
          $st_insexec->bindParam(':model', $exp_incomingdata[0], PDO::PARAM_STR);
          $st_insexec->bindParam(':cap', $exp_incomingdata[1], PDO::PARAM_STR);
          $st_insexec->bindParam(':cond', $exp_incomingdata[2], PDO::PARAM_STR);
          $st_insexec->bindParam(':name', $exp_incomingdata[3], PDO::PARAM_STR);
          $st_insexec->bindParam(':email', $exp_incomingdata[4], PDO::PARAM_STR);
          $st_insexec->bindParam(':mobile', $exp_incomingdata[5], PDO::PARAM_STR);
          $st_insexec->bindParam(':address', $exp_incomingdata[6], PDO::PARAM_STR);
          $st_insexec->bindParam(':message', $exp_incomingdata[7], PDO::PARAM_STR);
          $st_insexec->bindParam(':ip', $exp_incomingdata[8], PDO::PARAM_STR);
          $st_insexec->bindParam(':paymethod', $exp_incomingdata[9], PDO::PARAM_STR);
          $st_insexec->bindParam(':paypalemail', $exp_incomingdata[10], PDO::PARAM_STR);
          $st_insexec->bindParam(':bafullname', $exp_incomingdata[11], PDO::PARAM_STR);
          $st_insexec->bindParam(':banumber', $exp_incomingdata[12], PDO::PARAM_STR);
          $st_insexec->bindParam(':basortcode', $exp_incomingdata[13], PDO::PARAM_STR);
          $st_insexec->bindParam(':order_tracking', $exp_incomingdata[14], PDO::PARAM_STR);
          $st_insexec->execute();
          var_dump($st_insexec);
        }
        catch (PDOException $e)
        {
          throw new Exception('Could not connect to database');
          echo 'Connection failed: ' . $e->getMessage(); // SHOWING DATABASE NAME
        }

        break; ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - break
        case 'INST':
        $user_id = DB_ACCESS::meta_user();
        $user_pass = DB_ACCESS::meta_pass();
        try
        {
          // $order_tracking = rand();
          $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", "$user_id", "$user_pass");
          $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $st_insexec = $conn_meta->prepare("INSERT INTO `order_track`(`order_track`) VALUES (:order_tracking)");
          $st_insexec->bindParam(':order_tracking',$str_arr , PDO::PARAM_STR);
          $st_insexec->execute();
          //var_dump($st_insexec);
        }
        catch (PDOException $e)
        {
          throw new Exception('Could not connect to database');
          echo 'Connection failed: ' . $e->getMessage(); // SHOWING DATABASE NAME
        }

        break; ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - break
        case 'blank_sell_quot':
          $user_id = DB_ACCESS::meta_user();
          $user_pass = DB_ACCESS::meta_pass();
          try
          {
            $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", "$user_id", "$user_pass");
            $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $st_insexec = $conn_meta->prepare("INSERT INTO `quot_request_sell`(`q_fullname`, `q_email`, `q_mobile`, `q_address`, `q_brand`, `q_model`, `q_category`, `q_capacity`, `q_condition`, `q_network`, `q_color`, `q_demand`, `q_message`, `s_ip`) VALUES (:fullname,:email,:mobile,:address,:brand,:model,:category,:capacity,:condition,:network,:color,:demand,:message,:ip)");
            $st_insexec->bindParam(':fullname', $str_arr[0], PDO::PARAM_STR);
            $st_insexec->bindParam(':email', $str_arr[1], PDO::PARAM_STR);
            $st_insexec->bindParam(':mobile', $str_arr[2], PDO::PARAM_STR);
            $st_insexec->bindParam(':address', $str_arr[3], PDO::PARAM_STR);
            $st_insexec->bindParam(':brand', $str_arr[4], PDO::PARAM_STR);
            $st_insexec->bindParam(':model', $str_arr[5], PDO::PARAM_STR);
            $st_insexec->bindParam(':category', $str_arr[6], PDO::PARAM_STR);
            $st_insexec->bindParam(':capacity', $str_arr[7], PDO::PARAM_STR);
            $st_insexec->bindParam(':condition', $str_arr[8],PDO::PARAM_STR);
            $st_insexec->bindParam(':network', $str_arr[9],PDO::PARAM_STR);
            $st_insexec->bindParam(':color', $str_arr[10],PDO::PARAM_STR);
            $st_insexec->bindParam(':demand', $str_arr[11],PDO::PARAM_STR);
            $st_insexec->bindParam(':message', $str_arr[12],PDO::PARAM_STR);
            $st_insexec->bindParam(':ip', $str_arr[13],PDO::PARAM_STR);
            $st_insexec->execute();
          }
          catch (PDOException $e)
          {
            throw new Exception('Could not connect to database');
            echo 'Connection failed: BLACK_QUOT' . $e->getMessage(); // SHOWING DATABASE NAME
          }

          break; ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - break
          case 'blank_repair_quot':
            $user_id = DB_ACCESS::meta_user();
            $user_pass = DB_ACCESS::meta_pass();
            try
            {
              $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", "$user_id", "$user_pass");
              $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              $st_insexec = $conn_meta->prepare("INSERT INTO `quot_request_repair`(`q_fullname`, `q_email`, `q_mobile`, `q_address`, `q_brand`, `q_model`, `q_category`, `q_problem`, `q_message`, `s_ip`) VALUES (:fullname,:email,:mobile,:address,:brand,:model,:category,:problem,:message,:ip)");
              $st_insexec->bindParam(':fullname', $str_arr[0], PDO::PARAM_STR);
              $st_insexec->bindParam(':email', $str_arr[1], PDO::PARAM_STR);
              $st_insexec->bindParam(':mobile', $str_arr[2], PDO::PARAM_STR);
              $st_insexec->bindParam(':address', $str_arr[3], PDO::PARAM_STR);
              $st_insexec->bindParam(':brand', $str_arr[4], PDO::PARAM_STR);
              $st_insexec->bindParam(':model', $str_arr[5], PDO::PARAM_STR);
              $st_insexec->bindParam(':category', $str_arr[6], PDO::PARAM_STR);
              $st_insexec->bindParam(':problem', $str_arr[7], PDO::PARAM_STR);
              $st_insexec->bindParam(':message', $str_arr[8], PDO::PARAM_STR);
              $st_insexec->bindParam(':ip', $str_arr[9], PDO::PARAM_STR);
              $st_insexec->execute();
              //var_dump($st_insexec);
            }
            catch (PDOException $e)
            {
              throw new Exception('Could not connect to database');
              echo 'Connection failed: BLANK_QUOT';// . $e->getMessage(); // SHOWING DATABASE NAME
            }

          break; ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - break
          case 'simple_repair_quot':
            $user_id = DB_ACCESS::meta_user();
            $user_pass = DB_ACCESS::meta_pass();
            try
            {
              $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", $user_id, $user_pass);
              $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              $st_insexec = $conn_meta->prepare("INSERT INTO `quot_simple_repair`(`q_fullname`, `q_model`,`q_email`, `q_mobile`, `q_problem`, `q_message`, `s_ip`) VALUES (:fullname,:model,:email,:mobile,:problem,:message,:ip)");
              $st_insexec->bindParam(':fullname', $str_arr[0], PDO::PARAM_STR);
              $st_insexec->bindParam(':model', $str_arr[1], PDO::PARAM_STR);
              $st_insexec->bindParam(':email', $str_arr[2], PDO::PARAM_STR);
              $st_insexec->bindParam(':mobile', $str_arr[3], PDO::PARAM_STR);
              $st_insexec->bindParam(':problem', $str_arr[4], PDO::PARAM_STR);
              $st_insexec->bindParam(':message', $str_arr[5], PDO::PARAM_STR);
              $st_insexec->bindParam(':ip', $str_arr[6], PDO::PARAM_STR);
              $st_insexec->execute();
              //var_dump($st_insexec);
            }
            catch (PDOException $e)
            {
              throw new Exception('Could not connect to database');
              echo 'Connection failed: BLANK_QUOT'; // $e->getMessage(); // SHOWING DATABASE NAME
            }

          break; ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - break
          case 'appointment-submit':
          $user_id = DB_ACCESS::meta_user();
          $user_pass = DB_ACCESS::meta_pass();
          try
          {
            $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", "$user_id", "$user_pass");
            $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $st_insexec = $conn_meta->prepare("INSERT INTO `appointment_request`(`s_name`, `s_email`, `s_mobile`, `s_brand`, `s_model`, `s_message`, `s_ip`) VALUES (:name,:email,:mobile,:s_brand,:s_model,:message,:ip)");
            $st_insexec->bindParam(':name', $exp_incomingdata[0], PDO::PARAM_STR);
            $st_insexec->bindParam(':email', $exp_incomingdata[1], PDO::PARAM_STR);
            $st_insexec->bindParam(':mobile', $exp_incomingdata[2], PDO::PARAM_STR);
            $st_insexec->bindParam(':s_brand', $exp_incomingdata[3], PDO::PARAM_STR);
            $st_insexec->bindParam(':s_model', $exp_incomingdata[4], PDO::PARAM_STR);
            $st_insexec->bindParam(':message', $exp_incomingdata[6], PDO::PARAM_STR);
            $st_insexec->bindParam(':ip', $exp_incomingdata[7]);
            $st_insexec->execute();
          }
          catch (PDOException $e)
          {
            throw new Exception('Could not connect');
            echo 'Connection failed: ';// . $e->getMessage(); // SHOWING DATABASE NAME
          }

          break; ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - break
        case 'chkPrice':

        $user_id = DB_ACCESS::meta_user();
        $user_pass = DB_ACCESS::meta_pass();
        //
        try
        {


          $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", DB_ACCESS::meta_user(), DB_ACCESS::meta_pass());
          $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $st_meta = $conn_meta->prepare("SELECT * FROM product_spec WHERE prod_id=:prodid AND cap=:prodcap");
          $st_meta->bindParam(':prodid', $str_arr[0]);
          $st_meta->bindParam(':prodcap', $str_arr[1]);
          $st_meta->execute();
          $rows = $st_meta->fetch(PDO::FETCH_ASSOC);
          if ($str_arr[3]=="undefined") {
            echo "<div class='container' style='' class='recycler-price'><h6 style='font-size:120%;font-weight: 900;color:red'>Kindly select your Network, Capacity and condition.</h6></div>";
          }
          else {
          	$productid = $str_arr[0];
            // echo $productid;

            // GETTING DEVICE NAME
            $sqlb = $conn_meta->prepare("SELECT * FROM products where id='".$productid."'");
            $sqlb->execute();
            $sqlrow = $sqlb->fetch(PDO::FETCH_ASSOC);
            // echo $sqlrow['product_re'];
            $productname = $sqlrow['product_re'];

            // --------------Replace spaces with "-" Symbol
            $productname = str_replace(" ", "-", $productname);

         // CHECKING IF Apple iPhone DEVICE
  			if (substr($productname, 0, 3) === 'iph'){
          // echo "apple-".$productname;
  				$productname = "apple-".$productname;
  			}
        // CHECKING IF SAMSUNG DEVICE ----------------------------
        if (substr($productname, 0, 4) === 'gala'){
          // echo "apple-".$productname;
          $productname = "samsung-".$productname;
        }
        // HERE IS CAPACITY ----------------------------
          	$capacity = $str_arr[1];
          	$capacity = "-".$capacity."gb/";
            // echo $capacity;

        // HERE IS NETWORK ----------------------------
            $network = $str_arr[3];
            // CHECKING IF SAMSUNG DEVICE ----------------------------
        if ($network === '3'){
          // echo "apple-".$productname;
          $network = "three";
        }
        if ($network === 'unknown'){
          // echo "apple-".$productname;
          $network = "other";
        }
            $network = "network:".$network;
            // echo $network;
        // HERE IS CONDITION ----------------------------
            $condition = $str_arr[2];
          	$condition = "/condition:".$condition;
            // echo $condition;

            // MAKING MERGE DEVICE FULL DETAILS
            $devicefull = $productname."".$capacity."".$network."".$condition;
            // echo $devicefull;
?>
<link rel="stylesheet" type="text/css" href="../stylee.css">

  <style type="text/css">
    .comparison-cell{
      border-bottom: 0px solid black;
    }
    .comparison-header,.period,.method,.freepost,.sell,.no-price,.rating-tooltip,h3,.more-info,.merchant-info-row .label{
      display: none;
    }
    .comparison-cell.merchant .star_rating{
      display: block;
    }
    .comparison-cell .merchant-logo {
     max-width:90px;
     width:100%
    }
    .comparison-row {
     /*display:table;*/
     table-layout:fixed;
     width:20%;
     border:3px solid #ccc;
     /*border-bottom:3px solid #eaeaea;*/
     border-radius: 0.9em;
     box-sizing:border-box;
     text-transform:uppercase;
     /*margin: 5px auto;*/
    float: left;
    margin: 0px 27px 27px 27px;
    }
  </style>
<?php
// -----------------------

            $getval = $rows[$str_arr[2].'_cond_'.$str_arr[3]];

            ?>
          <div class="container"><div class="recycler-price kobixa" style="background:white">
            <div class="" style="font-size:100%;text-align:left;margin-bottom:0px;color:#1C1F24"><strong><u>Term & Conditions</u></strong><br></div>

            <?php
            echo "<h6 style='text-align:left;margin-top:0px;color:#141311;border-color:black'>";echo $rows[$str_arr[2].'_cond_desc']."</div></div>";
            echo "<div class='clearfix'></div><hr><h2 class='' style='background:#1655a6;color:white;text-transform:uppercase;font-size:1.4em;font-weight: 1000;margin:0;padding:10px;'><strong>Recommended</strong><span id='blink' style='color:black' class='glyphicon glyphicon-menu-down'></span>";
            echo "<div class='' style='background:#1655a6;'>
          <img style='background: white;padding: 10px;border-radius: 10px;margin: 10px;' class='header_img_logo img-responsive' alt='LONDON Techntrade' src='https://www.londontt.com/wp-content/uploads/2017/09/logo-1.png'>
            </div>";
            echo "<div style='background:;margin:0px;padding:00px;border:0;font-size:25px;' class='recycler-price'><h1 style='font-size:1.4em;font-weight: 900;color:#089de3'>£";echo $rows[$str_arr[2].'_cond_'.$str_arr[3]]."</h1></div>";
            echo "<input type='hidden' name='formated' value='".$rows[$str_arr[2].'_cond_'.$str_arr[3]]."'>";
            echo "<h2 class='container' style='color:black;text-transform:uppercase;font-size:1.4em;font-weight: 1000;margin-top:10px;'><strong>Compare Price with others networks </strong><br><span id='blink' style='color:black' class='glyphicon glyphicon-menu-down'></span></h2>";
            //$newurl = "https://www.compareandrecycle.co.uk/mobile-phones/".$devicefull;
            //$htmllive = file_get_html($newurl);
error_reporting(0);
            //foreach($htmllive->find('div#comparison-table') as $elite)
              //echo "<div class='container'>";
                //echo $elite->innertext . '<br>';
              //echo "</div>
              //<div class='clearfix'></div>";
              // echo $sqlrow['product_re'];
             // echo "<br><h2>Continue with Recommended Network Price</h2><h2>£". $rows[$str_arr[2].'_cond_'.$str_arr[3]]."</h2>";

            ?>
<div class="clearfix"></div>
<!-- <hr style="margin-top:5px;margin-bottom:5px"> -->
          <!-- <h4 class="text-center" style="font-weight: 800;color:#1655a6!important;">Sell Us</h4> -->
            <?php

            // echo $rows[$str_arr[2].'_cond_'.$str_arr[3].'_mc'];
            // echo $rows[$str_arr[2].'_cond_'.$str_arr[3].'_mc'];
          }
          ?>
          <?php

        }
        catch(PDOException $e)
        {
          echo $e->getMessage();echo "yahan";
        }
        //
        break; ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - break
        case 'LRRecycle':
        // echo "MODEL_RESPOND_RECYCLE".$get_arg_1;
        $get_arg_1_arr = explode(' ',$get_arg_1);
        // print_r($get_arg_1_arr);echo "<br>";
        // echo count($get_arg_1_arr);
        // exit;
        $user_id = DB_ACCESS::meta_user();
        $user_pass = DB_ACCESS::meta_pass();
        //
        try
        {
          $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", $user_id, $user_pass);
          $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $st_meta = $conn_meta->prepare("select P.product_id,P.brandname,P.modelname,P.image_one from products P where isActive_recycle=1 AND modelname LIKE CONCAT('%', :prodid, '%')");
          $st_meta->bindParam(':prodid', $get_arg_1, PDO::PARAM_STR);
          echo $get_arg_1;
          $st_meta->execute();
          //
          ?>
          <div class="row" style="">
            <?php
          while ($row = $st_meta->fetch(PDO::FETCH_ASSOC))
          { ?>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12" style="padding:0">
                  <a href="/phones/<?php echo $row['modelname'] ?>">
                    <div style="text-align:center;padding:13px;margin:4px 4px 0px 4px;background:#1655a6;color:white;text-transform:uppercase;font-weight: 800;">
                      <!-- <img src="<?php echo $row["image_one"] ?>" class="img-responsive" style="max-width:200px"> -->
                      <font style="text-transform:uppercase;color:white;"><?php echo $row['modelname'] ?></font>
                    </div>
                  </a>
                </div>
    <?php }

        }
        catch(PDOException $e)
        {
          // echo $e->getMessage();echo "yahan";
          echo '<font style="color:red">NO RESULT FOUND</font>';
        }
        echo "</div>";
        //
        break; ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - break
        case 'LRRepair':
        // echo "MODEL_RESPOND_RECYCLE".$get_arg_1;
        $get_arg_1_arr = explode(' ',$get_arg_1);
        // print_r($get_arg_1_arr);echo "<br>";
        // echo count($get_arg_1_arr);
        // exit;
        $user_id = DB_ACCESS::meta_user();
        $user_pass = DB_ACCESS::meta_pass();
        //
        try
        {
          $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8",$user_id,$user_pass);
          $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $st_meta = $conn_meta->prepare("SELECT * FROM products WHERE product_re LIKE CONCAT('%', :prodid, '%')");
          $st_meta->bindParam(':prodid', $get_arg_1, PDO::PARAM_STR);
          $st_meta->execute();
          //
          ?>
          <div class="row" style="">
            <?php
          while ($row = $st_meta->fetch(PDO::FETCH_ASSOC))
          {
            ?>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12" style="padding:0">
                  <a href="/repair/phones/<?php echo $row['product_re'] ?>">
                  <div class="" style="text-align:center;padding:10px;margin:4px 4px 0px 4px;background:#1655a6;color:white;text-transform:uppercase;">
                    <font style="text-transform:uppercase;color:black;"><?php echo $row['product_re'] ?></font>
                  </div>
  </a>
                </div>
            <?php
          }

        }
        catch(PDOException $e)
        {
          // echo $e->getMessage();echo "yahan";
          echo '<font style="color:red">NO RESULT FOUND</font>';
        }
        echo "</div>";
        //
        break; ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - break
        case 'FAVRecycle':
        //echo "MODEL_RESPOND_RECYCLE".$get_arg_1;
        $get_arg_1_arr = explode(' ',$get_arg_1);
        // print_r($get_arg_1_arr);echo "<br>";
        // echo count($get_arg_1_arr);
        // exit;
        $user_id = DB_ACCESS::meta_user();
        $user_pass = DB_ACCESS::meta_pass();
        //
        try
        {
          $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", DB_ACCESS::meta_user(), DB_ACCESS::meta_pass());
          $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $st_meta = $conn_meta->prepare("SELECT * FROM products WHERE favorite_re=1 LIMIT 8");
          $st_meta->bindParam(':prodid', $get_arg_1, PDO::PARAM_STR);
          $st_meta->execute();
          //
          while ($row = $st_meta->fetch(PDO::FETCH_ASSOC))
          { ?>
                <div class="col-md-3 col-sm-4 col-xs-1" style="padding:10px 0;background:white;border:0px solid #1655a6">
                  <div class="thumbnail text-center">
                    <img class="img-responsive" src="<?php echo $row['img_re'] ?>" alt="<?php echo $row['product_re'] ?>" style="width:200px"><a href="/phones/<?php echo $row['product_re'] ?>">
                      <div class="centered" style="font-family:'Lato';width:150px;height:70%;background:rgba(255, 255, 254, 0.5);position: absolute;top: 60%;left: 50%;transform: translate(-50%, -50%);text-transform:uppercase;color:black;font-weight:bold;text-shadow:0.1px 0.1px 0.5px white;font-weight: bolder;">
                        <?php echo $row['product_re'] ?><br>
                        <font style="color:black;font-family:Arial;font-weight:bold;font-size:10px;text-shadow:0.1px 0.1px 0.3px white">We are offering you</font><br>
                        <div style="background:#1655a6;color:white;border-radius: 3px;"><font style="font-family:'Lato';font-weight:bolder;font-size:20px;">£<?php echo $row['max_price_re'] ?></font></div>
                        SELL NOW
                      </div>
                    </div>
                  </a>
                </div>
                <br>
    <?php }

        }
        catch(PDOException $e)
        {
          // echo $e->getMessage();echo "yahan";
          echo '<font style="color:red">NO RESULT FOUND</font>';
        }
        //
        break; ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - break
        case 'allprob':
        //echo "MODEL_RESPOND_RECYCLE".$get_arg_1;
        $get_arg_1_arr = explode(' ',$get_arg_1);
        // print_r($get_arg_1_arr);echo "<br>";
        // echo count($get_arg_1_arr);
        // exit;
        $user_id = DB_ACCESS::meta_user();
        $user_pass = DB_ACCESS::meta_pass();
        //
        try
        {
          $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", DB_ACCESS::meta_user(), DB_ACCESS::meta_pass());
          $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $st_meta = $conn_meta->prepare("SELECT * FROM p_prob WHERE product_re=:productre");
          $st_meta->bindParam(':productre', $get_arg_1, PDO::PARAM_STR);
          $st_meta->execute();
          //
          while ($row = $st_meta->fetch(PDO::FETCH_ASSOC))
          { ?>

              <label class='col-md-3 col-sm-11' style="font-size:15.3px;margin-left:3px;padding: 13px 20px 0px 20px;color:#f84e0f;border: 3px solid #f84e0f;border-radius:0.5em;background:#fff" for="<?php echo $row['prob_name']; ?>" class="btn btn-danger"><div style="font-weight:800;color:##ff4500;"><?php echo $row['prob_name']; ?></div><div style='display:;margin-top:10px;font-size: 15px;font-family: century gothic;'><b>Was <strike>&pound; <?php echo $row['o_price']; ?></strike> - Now &pound;<?php echo $row['n_price']; ?> </b></div><input type="checkbox" id="<?php echo $row['prob_name']; ?>" name="checkedprob['<?php echo $row['prob_name']; ?>']" class="badgebox"><span style="color:black;" class="badge">&check;</span></label>
    <?php }

        }
        catch(PDOException $e)
        {
          // echo $e->getMessage();echo "yahan";
          echo '<font style="color:red">NO RESULT FOUND</font>';
        }
        //
        break; ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - break
        case 'get_one':
        //echo "MODEL_RESPOND_RECYCLE".$get_arg_1;
        //echo "MODEL_RESPOND_RECYCLE".$alx;
        $get_arg_1_arr = explode(' ',$get_arg_1);
        // print_r($get_arg_1_arr);echo "<br>";
        // echo count($get_arg_1_arr);
        // exit;
        $user_id = DB_ACCESS::meta_user();
        $user_pass = DB_ACCESS::meta_pass();
        //
        try
        {
          $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", DB_ACCESS::meta_user(), DB_ACCESS::meta_pass());
          $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $st_meta = $conn_meta->prepare("SELECT * FROM products where product_re=:productre");
          $st_meta->bindParam(':productre', $get_arg_1, PDO::PARAM_STR);
          $st_meta->execute();
          //
          while ($row = $st_meta->fetch(PDO::FETCH_ASSOC))
          {
            $a = str_replace('http://ifixedit.co.uk','',$row['img_re']); ?>
            <img class="img-responsive text-align"  src="<?php echo $a  ?>" alt="" style="width:250px;">
    <?php }

        }
        catch(PDOException $e)
        {
          // echo $e->getMessage();echo "yahan";
          echo '<font style="color:red">NO RESULT FOUND</font>';
        }
        //
        break; ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - break
        case 'admin':
        //echo "MODEL_RESPOND_RECYCLE".$get_arg_1;
        $get_arg_1_arr = explode(' ',$get_arg_1);
        // print_r($get_arg_1_arr);echo "<br>";
        // echo count($get_arg_1_arr);
        // exit;
        $user_id = DB_ACCESS::meta_user();
        $user_pass = DB_ACCESS::meta_pass();
        //
        try
        {
          $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", DB_ACCESS::meta_user(), DB_ACCESS::meta_pass());
          $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $st_meta = $conn_meta->prepare("SELECT * FROM geninfo");
          $st_meta->execute();


          while ($row = $st_meta->fetch(PDO::FETCH_ASSOC))
          {
            echo $row[$arg_1];
          }

        }
        catch(PDOException $e)
        {
          // echo $e->getMessage();echo "yahan";
          echo '<font style="color:red">NO RESULT FOUND</font>';
        }
        //
        break; ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - break
        case 'fetch':
        //echo "MODEL_RESPOND_RECYCLE".$get_arg_1;
        $get_arg_1_arr = explode(' ',$get_arg_1);
        // print_r($get_arg_1_arr);echo "<br>";
        // echo count($get_arg_1_arr);
        // exit;
        $user_id = DB_ACCESS::meta_user();
        $user_pass = DB_ACCESS::meta_pass();
        //
        try
        {
          $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", $user_id, $user_pass);
          $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $st_meta = $conn_meta->prepare("SELECT * FROM products where brand_re='".$arg_1."'" );
          $st_meta->execute();

          while ($row = $st_meta->fetch(PDO::FETCH_ASSOC))
          {

           echo "

           <div class='col-md-3 col-sm-6 col-xs-12 text-center ' style='border:1px solid black;background:white;padding:10px'><b style='font-weight:800;text-transform:uppercase'>
           ".$row['product_re'];
           echo "</b><br><img class='img-thumbnail' style='height:120px;' src='".$row['img_re']."'>
           <form class='form-group' action='/occoto-admin/submitprod/".str_replace(' ','_',$row['product_re'])."' method='POST'>
           <input type='hidden' name='id_".str_replace(' ','_',$row['product_re'])."' value='".$row['id']."'>
           Color <input type='text' class='form-control' name='color_".str_replace(' ','_',$row['product_re'])."' value='".$row['color_re']."'><br>
           Max Price <input type='text' class='form-control' name='mxp_".str_replace(' ','_',$row['product_re'])."' value='".$row['max_price_re']."'><br>
           Image Url <input type='text' class='form-control' name='img_".str_replace(' ','_',$row['product_re'])."' value='".$row['img_re']."'><br>
           Favorite <input type='text' class='form-control' name='fav_".str_replace(' ','_',$row['product_re'])."' value='".$row['favorite_re']."'><br>
           View Priority <input type='text' class='form-control' name='prior_".str_replace(' ','_',$row['product_re'])."' value='".$row['prior_re']."'><br>
           Active/InActive <input type='text' class='form-control' name='active_".str_replace(' ','_',$row['product_re'])."' value='".$row['active_re']."'><br>
           <input class='btn' type='submit' name='submit_".str_replace(' ','_',$row['product_re'])."' value='Update & Next' style='background:black;color:white'>
           </form>
           </div>
           ";
           echo "<hr><br>";

          }

        }
        catch(PDOException $e)
        {
          // echo $e->getMessage();echo "yahan";
          echo '<font style="color:red">NO RESULT FOUND</font>';
        }
        //
        break; ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - break
        case 'update_prod': /////////////////////////// update f*k
          $user_id = DB_ACCESS::meta_user();
          $user_pass = DB_ACCESS::meta_pass();
          try
          {
            $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", "$user_id", "$user_pass");
            $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $st_insexec = $conn_meta->prepare("update products set color_re=:color , max_price_re=:mxp , img_re=:img , favorite_re=:fav , prior_re=:prior , active_re=:active where product_re=:prod");
            $st_insexec->bindParam(':color', $str_arr[0], PDO::PARAM_STR);
            $st_insexec->bindParam(':mxp', $str_arr[1], PDO::PARAM_STR);
            $st_insexec->bindParam(':img', $str_arr[2], PDO::PARAM_STR);
            $st_insexec->bindParam(':fav', $str_arr[3], PDO::PARAM_STR);
            $st_insexec->bindParam(':prior', $str_arr[4], PDO::PARAM_STR);
            $st_insexec->bindParam(':active', $str_arr[5], PDO::PARAM_STR);
            $st_insexec->bindParam(':prod', $arg_2, PDO::PARAM_STR);
            $st_insexec->execute();
            //var_dump($st_insexec);
          }
          catch (PDOException $e)
          {
            throw new Exception('Could not connect to database');
            echo 'Connection failed: ' . $e->getMessage(); // SHOWING DATABASE NAME
          }

          break; ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - break
          case 'update_prod_spec': /////////////////////////// update f*k
            $user_id = DB_ACCESS::meta_user();
            $user_pass = DB_ACCESS::meta_pass();
            $arr_merge_data = explode('-_-',$_SESSION['prod_spec_merge']);
            try
            {
              $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", "$user_id", "$user_pass");
              $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              $st_insexec = $conn_meta->prepare("update product_spec set
              new_cond_desc=:new_conddesc ,
              good_cond_desc=:good_conddesc ,
              working_cond_desc=:working_conddesc ,
              broken_cond_desc=:broken_conddesc ,
              new_cond_unlocked=:new_condunlocked ,
              good_cond_unlocked=:good_condunlocked ,
              working_cond_unlocked=:working_condunlocked ,
              broken_cond_unlocked=:broken_condunlocked ,
              new_cond_o2=:new_condo2 ,
              good_cond_o2=:good_condo2 ,
              working_cond_o2=:working_condo2 ,
              broken_cond_o2=:broken_condo2 ,
              new_cond_o2=:new_condo2 ,
              good_cond_o2=:good_condo2 ,
              working_cond_o2=:working_condo2 ,
              broken_cond_o2=:broken_condo2 ,
              new_cond_ee=:new_condee ,
              good_cond_ee=:good_condee ,
              working_cond_ee=:working_condee ,
              broken_cond_ee=:broken_condee ,
              new_cond_vodafone=:new_condvodafone ,
              good_cond_vodafone=:good_condvodafone ,
              working_cond_vodafone=:working_condvodafone ,
              broken_cond_vodafone=:broken_condvodafone ,
              new_cond_orange=:new_condorange ,
              good_cond_orange=:good_condorange ,
              working_cond_orange=:working_condorange ,
              broken_cond_orange=:broken_condorange ,
              new_cond_tmobile=:new_condtmobile ,
              good_cond_tmobile=:good_condtmobile ,
              working_cond_tmobile=:working_condtmobile ,
              broken_cond_tmobile=:broken_condtmobile ,
              new_cond_3=:new_cond3 ,
              good_cond_3=:good_cond3 ,
              working_cond_3=:working_cond3 ,
              broken_cond_3=:broken_cond3 ,
              new_cond_virgin=:new_condvirgin ,
              good_cond_virgin=:good_condvirgin ,
              working_cond_virgin=:working_condvirgin ,
              broken_cond_virgin=:broken_condvirgin ,
              new_cond_tesco=:new_condtesco ,
              good_cond_tesco=:good_condtesco ,
              working_cond_tesco=:working_condtesco ,
              broken_cond_tesco=:broken_condtesco ,
              new_cond_unknown=:new_condunknown ,
              good_cond_unknown=:good_condunknown ,
              working_cond_unknown=:working_condunknown ,
              broken_cond_unknown=:broken_condunknown where id=:prodid
              ");
              $st_insexec->bindParam(':new_conddesc',$arr_merge_data[0], PDO::PARAM_STR);
              $st_insexec->bindParam(':good_conddesc',$arr_merge_data[1], PDO::PARAM_STR);
              $st_insexec->bindParam(':working_conddesc',$arr_merge_data[2], PDO::PARAM_STR);
              $st_insexec->bindParam(':broken_conddesc',$arr_merge_data[3], PDO::PARAM_STR);
              $st_insexec->bindParam(':new_condunlocked',$arr_merge_data[4], PDO::PARAM_STR);
              $st_insexec->bindParam(':good_condunlocked',$arr_merge_data[5], PDO::PARAM_STR);
              $st_insexec->bindParam(':working_condunlocked',$arr_merge_data[6], PDO::PARAM_STR);
              $st_insexec->bindParam(':broken_condunlocked',$arr_merge_data[7], PDO::PARAM_STR);
              $st_insexec->bindParam(':new_condo2',$arr_merge_data[8], PDO::PARAM_STR);
              $st_insexec->bindParam(':good_condo2',$arr_merge_data[9], PDO::PARAM_STR);
              $st_insexec->bindParam(':working_condo2',$arr_merge_data[10], PDO::PARAM_STR);
              $st_insexec->bindParam(':broken_condo2',$arr_merge_data[11], PDO::PARAM_STR);
              $st_insexec->bindParam(':new_condo2',$arr_merge_data[12], PDO::PARAM_STR);
              $st_insexec->bindParam(':good_condo2',$arr_merge_data[13], PDO::PARAM_STR);
              $st_insexec->bindParam(':working_condo2',$arr_merge_data[14], PDO::PARAM_STR);
              $st_insexec->bindParam(':broken_condo2',$arr_merge_data[15], PDO::PARAM_STR);
              $st_insexec->bindParam(':new_condee',$arr_merge_data[16], PDO::PARAM_STR);
              $st_insexec->bindParam(':good_condee',$arr_merge_data[17], PDO::PARAM_STR);
              $st_insexec->bindParam(':working_condee',$arr_merge_data[18], PDO::PARAM_STR);
              $st_insexec->bindParam(':broken_condee',$arr_merge_data[19], PDO::PARAM_STR);
              $st_insexec->bindParam(':new_condvodafone',$arr_merge_data[20], PDO::PARAM_STR);
              $st_insexec->bindParam(':good_condvodafone',$arr_merge_data[21], PDO::PARAM_STR);
              $st_insexec->bindParam(':working_condvodafone',$arr_merge_data[22], PDO::PARAM_STR);
              $st_insexec->bindParam(':broken_condvodafone',$arr_merge_data[23], PDO::PARAM_STR);
              $st_insexec->bindParam(':new_condorange',$arr_merge_data[24], PDO::PARAM_STR);
              $st_insexec->bindParam(':good_condorange',$arr_merge_data[25], PDO::PARAM_STR);
              $st_insexec->bindParam(':working_condorange',$arr_merge_data[26], PDO::PARAM_STR);
              $st_insexec->bindParam(':broken_condorange',$arr_merge_data[27], PDO::PARAM_STR);
              $st_insexec->bindParam(':new_condtmobile',$arr_merge_data[28], PDO::PARAM_STR);
              $st_insexec->bindParam(':good_condtmobile',$arr_merge_data[29], PDO::PARAM_STR);
              $st_insexec->bindParam(':working_condtmobile',$arr_merge_data[30], PDO::PARAM_STR);
              $st_insexec->bindParam(':broken_condtmobile',$arr_merge_data[31], PDO::PARAM_STR);
              $st_insexec->bindParam(':new_cond3',$arr_merge_data[32], PDO::PARAM_STR);
              $st_insexec->bindParam(':good_cond3',$arr_merge_data[33], PDO::PARAM_STR);
              $st_insexec->bindParam(':working_cond3',$arr_merge_data[34], PDO::PARAM_STR);
              $st_insexec->bindParam(':broken_cond3',$arr_merge_data[35], PDO::PARAM_STR);
              $st_insexec->bindParam(':new_condvirgin',$arr_merge_data[36], PDO::PARAM_STR);
              $st_insexec->bindParam(':good_condvirgin',$arr_merge_data[37], PDO::PARAM_STR);
              $st_insexec->bindParam(':working_condvirgin',$arr_merge_data[38], PDO::PARAM_STR);
              $st_insexec->bindParam(':broken_condvirgin',$arr_merge_data[39], PDO::PARAM_STR);
              $st_insexec->bindParam(':new_condtesco',$arr_merge_data[40], PDO::PARAM_STR);
              $st_insexec->bindParam(':good_condtesco',$arr_merge_data[41], PDO::PARAM_STR);
              $st_insexec->bindParam(':working_condtesco',$arr_merge_data[42], PDO::PARAM_STR);
              $st_insexec->bindParam(':broken_condtesco',$arr_merge_data[43], PDO::PARAM_STR);
              $st_insexec->bindParam(':new_condunknown',$arr_merge_data[44], PDO::PARAM_STR);
              $st_insexec->bindParam(':good_condunknown',$arr_merge_data[45], PDO::PARAM_STR);
              $st_insexec->bindParam(':working_condunknown',$arr_merge_data[46], PDO::PARAM_STR);
              $st_insexec->bindParam(':broken_condunknown',$arr_merge_data[47], PDO::PARAM_STR);
              $st_insexec->bindParam(':prodid',$arg_1, PDO::PARAM_STR);
              $st_insexec->execute();
              //var_dump($st_insexec);
            }
            catch (PDOException $e)
            {
              throw new Exception('Could not connect to database');
              echo 'Connection failed: ' . $e->getMessage(); // SHOWING DATABASE NAME
            }

            break; ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - break
          case 'fetch-ind':
          //echo "MODEL_RESPOND_RECYCLE".$get_arg_1;
          $get_arg_1_arr = explode(' ',$get_arg_1);
          // print_r($get_arg_1_arr);echo "<br>";
          // echo count($get_arg_1_arr);
          // exit;
          $user_id = DB_ACCESS::meta_user();
          $user_pass = DB_ACCESS::meta_pass();
          //
          try
          {
            $conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", $user_id, $user_pass);
            $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $st_meta = $conn_meta->prepare("SELECT * FROM product_spec where prod_id='".$arg_1."'" );
            $st_meta->execute();
            $coun = $st_meta->rowCount();
              echo "<br><div style='margin-top:-10px;' class='row'><div class='col-md-12'>
<form action='/occoto-admin/capsubmit/".$arg_1."' method='post'>
              <div class='col-md-10 col-sm-12 col-xs-12'><input type='text' style='margin-top:10px;' class='form-control' name='cap' placeholder='Enter Your Capacity'></div>
              <input type='submit' name='capsubmit' style='margin-top:10px;' class='btn btn-default form-control col-md-2 col-sm-12 col-xs-12' value='Add Capacity'></form>
              </div></div>
              <hr>";
              echo '<div class="clearfix"></div>';
            while ($row = $st_meta->fetch(PDO::FETCH_ASSOC))
            {
              echo "<div class='col-md-4 col-sm-6 col-xs-12 text-center' style='text-align:left;background:white'><b>Storage : ".$row['cap']." GB</b>
              <form action='/occoto-admin/submitprod/ind/".$row['id']."' method='post'>
              <input type='hidden' name='".$row['id']."' value='".$row['id']."'>
              Description New:<br><input type='text' class='form-control' name='new_cond_desc_".$row['id']."' value='".$row['new_cond_desc']."'><br>
              Description Good:<br> <input type='text' class='form-control' name='good_cond_desc_".$row['id']."' value='".$row['good_cond_desc']."'><br>
              Description Working:<br> <input type='text' class='form-control' name='working_cond_desc_".$row['id']."' value='".$row['working_cond_desc']."'><br>
              Description Broken:<br> <input type='text' class='form-control' name='broken_cond_desc_".$row['id']."' value='".$row['broken_cond_desc']."'><hr>
              New Unlocked Price:<br> <input type='text' class='form-control' name='new_cond_unlocked_".$row['id']."' value='".$row['new_cond_unlocked']."'><br>
              Good Unlocked Price:<br> <input type='text' class='form-control' name='good_cond_unlocked_".$row['id']."' value='".$row['good_cond_unlocked']."'><br>
              Working Unlocked Price:<br> <input type='text' class='form-control' name='working_cond_unlocked_".$row['id']."' value='".$row['working_cond_unlocked']."'><br>
              Broken Unlocked Price:<br> <input type='text' class='form-control' name='broken_cond_unlocked_".$row['id']."' value='".$row['broken_cond_unlocked']."'><hr>
              New O2 Price:<br> <input type='text' class='form-control' name='new_cond_o2_".$row['id']."' value='".$row['new_cond_o2']."'><br>
              Good O2 Price:<br> <input type='text' class='form-control' name='good_cond_o2_".$row['id']."' value='".$row['good_cond_o2']."'><br>
              Working O2 Price:<br> <input type='text' class='form-control' name='working_cond_o2_".$row['id']."' value='".$row['working_cond_o2']."'><br>
              Broken O2 Price:<br> <input type='text' class='form-control' name='broken_cond_o2_".$row['id']."' value='".$row['broken_cond_o2']."'><hr>
              New O2 Price:<br> <input type='text' class='form-control' name='new_cond_o2_".$row['id']."' value='".$row['new_cond_o2']."'><br>
              Good O2 Price:<br> <input type='text' class='form-control' name='good_cond_o2_".$row['id']."' value='".$row['good_cond_o2']."'><br>
              Working O2 Price:<br> <input type='text' class='form-control' name='working_cond_o2_".$row['id']."' value='".$row['working_cond_o2']."'><br>
              Broken O2 Price:<br> <input type='text' class='form-control' name='broken_cond_o2_".$row['id']."' value='".$row['broken_cond_o2']."'><hr>
              New EE Price:<br> <input type='text' class='form-control' name='new_cond_ee_".$row['id']."' value='".$row['new_cond_ee']."'><br>
              Good EE Price:<br> <input type='text' class='form-control' name='good_cond_ee_".$row['id']."' value='".$row['good_cond_ee']."'><br>
              Working EE Price:<br> <input type='text' class='form-control' name='working_cond_ee_".$row['id']."' value='".$row['working_cond_ee']."'><br>
              Broken EE Price:<br> <input type='text' class='form-control' name='broken_cond_ee_".$row['id']."' value='".$row['broken_cond_ee']."'><hr>
              New VODAFONE Price:<br> <input type='text' class='form-control' name='new_cond_vodafone_".$row['id']."' value='".$row['new_cond_vodafone']."'><br>
              Good VODAFONE Price:<br> <input type='text' class='form-control' name='good_cond_vodafone_".$row['id']."' value='".$row['good_cond_vodafone']."'><br>
              Working VODAFONE Price:<br> <input type='text' class='form-control' name='working_cond_vodafone_".$row['id']."' value='".$row['working_cond_vodafone']."'><br>
              Broken VODAFONE Price:<br> <input type='text' class='form-control' name='broken_cond_vodafone_".$row['id']."' value='".$row['broken_cond_vodafone']."'><hr>
              New Orange Price:<br> <input type='text' class='form-control' name='new_cond_orange_".$row['id']."' value='".$row['new_cond_orange']."'><br>
              Good Orange Price:<br> <input type='text' class='form-control' name='good_cond_orange_".$row['id']."' value='".$row['good_cond_orange']."'><br>
              Working Orange Price:<br> <input type='text' class='form-control' name='working_cond_orange_".$row['id']."' value='".$row['working_cond_orange']."'><br>
              Broken Orange Price:<br> <input type='text' class='form-control' name='broken_cond_orange_".$row['id']."' value='".$row['broken_cond_orange']."'><hr>
              New T-Mobile Price:<br> <input type='text' class='form-control' name='new_cond_tmobile_".$row['id']."' value='".$row['new_cond_tmobile']."'><br>
              Good T-Mobile Price:<br> <input type='text' class='form-control' name='good_cond_tmobile_".$row['id']."' value='".$row['good_cond_tmobile']."'><br>
              Working T-Mobile Price:<br> <input type='text' class='form-control' name='working_cond_tmobile_".$row['id']."' value='".$row['working_cond_tmobile']."'><br>
              Broken T-Mobile Price:<br> <input type='text' class='form-control' name='broken_cond_tmobile_".$row['id']."' value='".$row['broken_cond_tmobile']."'><hr>
              New 3 Price:<br> <input type='text' class='form-control' name='new_cond_3_".$row['id']."' value='".$row['new_cond_3']."'><br>
              Good 3 Price:<br> <input type='text' class='form-control' name='good_cond_3_".$row['id']."' value='".$row['good_cond_3']."'><br>
              Working 3 Price:<br> <input type='text' class='form-control' name='working_cond_3_".$row['id']."' value='".$row['working_cond_3']."'><br>
              Broken 3 Price:<br> <input type='text' class='form-control' name='broken_cond_3_".$row['id']."' value='".$row['broken_cond_3']."'><hr>
              New VIRGIN Price:<br> <input type='text' class='form-control' name='new_cond_virgin_".$row['id']."' value='".$row['new_cond_virgin']."'><br>
              Good VIRGIN Price:<br> <input type='text' class='form-control' name='good_cond_virgin_".$row['id']."' value='".$row['good_cond_virgin']."'><br>
              Working VIRGIN Price:<br> <input type='text' class='form-control' name='working_cond_virgin_".$row['id']."' value='".$row['working_cond_virgin']."'><br>
              Broken VIRGIN Price:<br> <input type='text' class='form-control' name='broken_cond_virgin_".$row['id']."' value='".$row['broken_cond_virgin']."'><hr>
              New Tesco Price:<br> <input type='text' class='form-control' name='new_cond_tesco_".$row['id']."' value='".$row['new_cond_tesco']."'><br>
              Good Tesco Price:<br> <input type='text' class='form-control' name='good_cond_tesco_".$row['id']."' value='".$row['good_cond_tesco']."'><br>
              Working Tesco Price:<br> <input type='text' class='form-control' name='working_cond_tesco_".$row['id']."' value='".$row['working_cond_tesco']."'><br>
              Broken Tesco Price:<br> <input type='text' class='form-control' name='broken_cond_tesco_".$row['id']."' value='".$row['broken_cond_tesco']."'><hr>
              New Unknow Price:<br> <input type='text' class='form-control' name='new_cond_unknown_".$row['id']."' value='".$row['new_cond_unknown']."'><br>
              Good Unknow Price:<br> <input type='text' class='form-control' name='good_cond_unknown_".$row['id']."' value='".$row['good_cond_unknown']."'><br>
              Working Unknow Price:<br> <input type='text' class='form-control' name='working_cond_unknown_".$row['id']."' value='".$row['working_cond_unknown']."'><br>
              Broken Unknow Price:<br> <input type='text' class='form-control' name='broken_cond_unknown_".$row['id']."' value='".$row['broken_cond_unknown']."'><hr>
              <input class='btn' type='submit' name='submit_".$row['id']."' value='Update' style='background:black;color:white'>
              </form>
              </div>";
            }
          }
          catch(PDOException $e)
          {
            // echo $e->getMessage();echo "yahan";
            echo '<font style="color:red">NO RESULT FOUND</font>';
          }
          //
          break; ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - break
        default:
        echo "NO_MODEL_RESULT_AGAIST_TH";
        break;
    }
  }
}
 ?>
