<?php
require_once '_controllers/login.php';
$login = new login();
$bool = false;
if($login->is_loggedin()!="")
{
  $user_id = $_SESSION['user_session'];
	$stmt = $login->runQuery("SELECT * FROM tbl_users WHERE user_id=:user_id");
	$stmt->execute(array(":user_id"=>$user_id));
	$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
  if ($userRow['isAdmin']!='1') {
    header('location: /recycle/');
  }
  $bool = true;
}
else {
  header('location: /login/');
}
 ?>
 <!DOCTYPE html>
 <html>
   <head>
     <meta charset="utf-8">
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
     <meta name="description" content="sell smartphone buy phone shop recycle phone unlocking repairing at Londontt.">
     <meta NAME="geo.region" content="london">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title><?php echo strtoupper($subrequest_render); ?> Edit Page</title>
     <link rel="stylesheet" href="/bootstrap-4.0.0/css/bootstrap.css">
     <script src="/js/jquery/jquery-3.2.1.js"></script>
     <script src="/js/jquery/popper.js"></script>
     <script src="/bootstrap-4.0.0/js/bootstrap.min.js"></script>
     <script src="/js/script.js"></script>
     <link rel="stylesheet" href="/css/style.css" type="text/css"/>
     <!-- fonts -->
     <link href='https://fonts.googleapis.com/css?family=Anton' rel='stylesheet'>
     <link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
     <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet'>
     <link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
     <link href='https://fonts.googleapis.com/css?family=Bangers' rel='stylesheet'>
     <link href='https://fonts.googleapis.com/css?family=Bubblegum Sans' rel='stylesheet'>
     <link href='https://fonts.googleapis.com/css?family=Bad Script' rel='stylesheet'>
     <link href='https://fonts.googleapis.com/css?family=Allan' rel='stylesheet'>
     <link href='https://fonts.googleapis.com/css?family=Baloo Tamma' rel='stylesheet'>
     <link href='https://fonts.googleapis.com/css?family=Cabin' rel='stylesheet'>
   </head>
<body style="">
<?php
require_once 'header.php';
?>

 <div class='container text-center'>
  <br>
<a href="/occoto-admin/">Go Dashboard</a>
<h3 class="text-center" style="text-transform: uppercase;margin:0  ">
<?php echo $subrequest_render; ?> Product
</h3>
 <br>
   <div class='row'>
    
     <?php OccotoAdmin::select_products($subrequest_render); ?>

   </div>
 </div>
 
</body>
</html>
