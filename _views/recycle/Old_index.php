<?php
require_once '_controllers/login.php';
$login = new login();
$bool = false;
if($login->is_loggedin()!="")
{
  //echo $login->is_loggedin();exit; // will show user id
  $user_id = $_SESSION['user_session'];
	$stmt = $login->runQuery("SELECT * FROM tbl_users WHERE user_id=:user_id");
	$stmt->execute(array(":user_id"=>$user_id));
	$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
  $bool = true;
}
else $bool = false;

 ?>

    <title>Recycle</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href='//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' rel='stylesheet'/>
    <script data-require="jquery" data-semver="2.0.3" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Bangers' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Archivo' rel='stylesheet'>

    <link href='https://fonts.googleapis.com/css?family=Alfa Slab One' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Anton' rel='stylesheet'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style media="screen">
    ul.dropdown-cart{
    min-width:250px;
}
ul.dropdown-cart li .item{
    display:block;
    padding:3px 10px;
    margin: 3px 0;
}
ul.dropdown-cart li .item:hover{
    background-color:#f3f3f3;
}
ul.dropdown-cart li .item:after{
    visibility: hidden;
    display: block;
    font-size: 0;
    content: " ";
    clear: both;
    height: 0;
}

ul.dropdown-cart li .item-left{
    float:left;
}
ul.dropdown-cart li .item-left img,
ul.dropdown-cart li .item-left span.item-info{
    float:left;
}
ul.dropdown-cart li .item-left span.item-info{
    margin-left:10px;
}
ul.dropdown-cart li .item-left span.item-info span{
    display:block;
}
ul.dropdown-cart li .item-right{
    float:right;
}
ul.dropdown-cart li .item-right button{
    margin-top:14px;
}
    .top-bar-slogan{
      text-transform:uppercase;
      padding:10px;
      border-top:solid #8BC34A;
      border-bottom:solid #8BC34A;
      margin-bottom:0px;
      text-align:center;
      background: darkgreen;
      color:white;
    }

    .thumbnails li> .fff .caption {
        background:#fff !important;
        padding:10px
    }

    /* Page Header */
    .page-header {
        background: #f9f9f9;
        margin: -30px -40px 40px;
        padding: 20px 40px;
        border-top: 4px solid #ccc;
        color: #999;
        text-transform: uppercase;
    }

    .page-header h3 {
        line-height: 0.88rem;
        color: #000;
    }

    ul.thumbnails {
        margin-bottom: 0px;
    }

    /* Thumbnail Box */
    .caption h4 {
        color: #444;
    }

    .caption p {
        color: #999;
    }

    /* Carousel Control */
    .control-box {
        text-align: right;
        width: 100%;
    }
    .carousel-control{
        background: #666;
        border: 0px;
        border-radius: 0px;
        display: inline-block;
        font-size: 34px;
        font-weight: 200;
        line-height: 18px;
        opacity: 0.5;
        padding: 4px 10px 0px;
        position: static;
        height: 30px;
        width: 15px;
    }

    .nav > li > a{
      display: inline-block;
    }

    /* Mobile Only */
    @media (max-width: 767px) {
      .top-bar-slogan{
        text-transform:uppercase;
        padding:10px;
        border-top:solid #8BC34A;
        border-bottom:solid #8BC34A;
        margin-bottom:10px;
        text-align:center;
        background: darkgreen;
        color:white;
      }
      .nav > li{
        display: inline-block;
      }

        .page-header, .control-box {
            text-align: center;
        }
    }
    @media (max-width: 479px) {
      .top-bar-slogan{
        text-transform:uppercase;
        padding:10px;
        border-top:solid #8BC34A;
        border-bottom:solid #8BC34A;
        margin-bottom:10px;
        text-align:center;
        background: darkgreen;
        color:white;
      }
        .caption {
            word-break: break-all;
        }
        .nav > li{
          display: inline-block;
        }
    }

    footer.nb-footer {
    background: #222;
    border-top: 4px solid darkgreen; }
    footer.nb-footer .about {
    margin: 0 auto;
    margin-top: 40px;
    max-width: 1170px;
    text-align: center; }
    footer.nb-footer .about p {
    font-size: 13px;
    color: #999;
    margin-top: 30px; }
    footer.nb-footer .about .social-media {
    margin-top: 15px; }

    footer.nb-footer .about .social-media ul li a {
        display: inline-block;
        width: 60px;
        height: 60px;
        line-height: 0px;
        border-radius: 50%;
        font-size: 35px;
        color: white;
        border: 1px solid rgba(255, 255, 255, 0.3);
    }

    footer.nb-footer .about .social-media ul li a:hover {
    background: green;
    color: #fff;
    border-color: green }

    footer.nb-footer .footer-info-single {
    margin-top: 30px; }

    footer.nb-footer .footer-info-single .title {
    color: #aaa;
    text-transform: uppercase;
    font-size: 16px;
    border-left: 4px solid #b78c33;
    padding-left: 5px; }

    footer.nb-footer .footer-info-single ul li a {
    display: block;
    color: #aaa;
    padding: 2px 0; }
    footer.nb-footer .footer-info-single ul li a:hover {
    color: #b78c33; }
    footer.nb-footer .footer-info-single p {
    font-size: 13px;
    line-height: 20px;
    color: #aaa; }
    footer.nb-footer .copyright {
    margin-top: 15px;
    background: #111;
    padding: 7px 0;
    color: #999; }
    footer.nb-footer .copyright p {
    margin: 0;
    padding: 0; }

    /*---*/

    .btn {
      font-size:25px;
    }
    .toparea {
      background:;
      padding:15px;
      margin-top:-15px;
      text-transform:uppercase;
      text-align:center;
    }
    .nav>li>a {
      position: relative;
      display: block;
      padding: 20px 15px;
      border-radius: 0px !important;
    }

    .nav > li.active > a.topmenu-a:hover, .nav > li.active > a.topmenu-a:focus{
      background: green !important;
      color: black !important;
    }

    .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
            color: black !important;
            background-color: white !important;
            border-bottom: 1px solid darkgreen;
            border-right: 1px solid darkgreen;
        }

        .nav > li > a:hover, .nav > li > a:focus {
            text-decoration: none;
            background-color: white !important;
        }

    .price {
        color: #190061;
        font-weight: bold;
        margin-bottom: 0;
        line-height: 24px;
        font-family: "VAGRoundedStd Bold",Arial,Helvetica,sans-serif;
        font-size: 32px;
    }
    .h4 {
        color: #484848;
        margin-top: 10px;
        margin-bottom: 5px;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 20px;
        font-weight: bold;
        text-decoration: none;
        display: block;
    }

    a.btnsell {
        background: lightgreen;
        color:black;
        border-radius: 2px;
        padding: 5px 10px;
        width: 80%;
        max-width: 150px;
    }

    a.btnsell:hover {
        border: 1px solid black;
    }
    @media (max-width: 786px){
      .btn {
        font-size:15px;
      }
      .toparea {
        background:;
        padding:5px;
        margin-top:-5px;
        margin-bottom:-5px;
        text-transform:uppercase;
        text-align:center;
      }
      .toparea>hr{
        margin-top:-5px;
        margin-bottom:5px;
      }
      .nav-pills>li {
          float: left;
      }
      .nav-pills {

      }
      .nav>li>a {
        padding: 13px 18px;
        margin-bottom: 0px;
        font-size: 13px;
        position: relative;
        display: inline-block;
      }
    }
    </style>
  </head>
  <body style="background:#f1f9f9;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',sans-serif;">


  <nav class="navbar navbar-default navbar-fixed-top" style="padding:0px">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header" style="margin-left:3px">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <h4 style="vertical-align:middle;margin-top:20px;margin-left:-7px;margin-right:7px;"><strong><a href="/index">LONDON TECHNTRADE</a></strong></h4>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="background:white;">
      <ul class="nav navbar-nav" style="margin-left:0px">
        <li class="active"><a class="topmenu-a" style="background:green;color:white" href="#"><strong>RECYCLE | RE-SALE</strong><span class="sr-only">(current)</span></a></li>
        <li><a href="/unlock" class="topmenu-a" href="#">UNLOCKING</a></li>
        <li><a href="/repair" class="topmenu-a" href="#">REPAIRING</a></li>
        <li><a href="/shop" class="topmenu-a" href="#">SHOP</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right" style="margin-left:0px">
          <?php if ($bool == true) { ?>
            <li>
              <a><font color=green>LOGIN: <strong><?php echo $userRow['user_name']; ?></strong></font>
              </a>
            </li>
            <li>
              <a href='/logout'><font color=red>LOGOUT </font><span class="glyphicon glyphicon-log-out"></span></a>
            </li>
          <?php }else if ($bool == false){?>
            <li>
              <a href='/login'><font color=black>LOGIN</font></a>
              </li>
              <li><a href='/sign-up' style="color:black">SIGN UP</a></li>
          <?php } ?>
        <ul class="nav navbar-nav navbar-right" style="margin-right:2px">
          <li class="dropdown" style="margin-left:0px">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <span class="glyphicon glyphicon-shopping-cart"></span> <?php echo count($_SESSION['sell_cart']); ?> - Items<span class="caret"></span></a>
            <ul class="dropdown-menu dropdown-cart" role="menu" style="width:500px;height:auto;">
              <li>
                <?php $totalprice = 0;
                $totalqty = 0;
                foreach ($_SESSION['sell_cart'] as $sell_cart_item => $value) {
                  if ($bool==true) {
                    if ($_SESSION['sell_cart'][$sell_cart_item]['user_id']=="0" || $_SESSION['sell_cart'][$sell_cart_item]['user_id']=="") {
                      $_SESSION['sell_cart'][$sell_cart_item]['user_id'] = $_SESSION['user_session'];
                    }
                  }
                  echo "User ID: ".$_SESSION['sell_cart'][$sell_cart_item]['user_id']." Product ID: ".$_SESSION['sell_cart'][$sell_cart_item]['product_id']." Model: ".$_SESSION['sell_cart'][$sell_cart_item]['model']." Capacity: ".$_SESSION['sell_cart'][$sell_cart_item]['cap']." Condition: ".$_SESSION['sell_cart'][$sell_cart_item]['con']." Network: ".$_SESSION['sell_cart'][$sell_cart_item]['net']." Color: ".$_SESSION['sell_cart'][$sell_cart_item]['col'];
                  echo " Price: ".$_SESSION['sell_cart'][$sell_cart_item]['price']." Qty: ".$_SESSION['sell_cart'][$sell_cart_item]['qty']."<br>";
                  echo "<br>";
                  $totalprice += $_SESSION['sell_cart'][$sell_cart_item]['qty'] * $_SESSION['sell_cart'][$sell_cart_item]['price'];
                  $totalqty +=$_SESSION['sell_cart'][$sell_cart_item]['qty'];echo "";
                  echo "<li class='divider'></li>";
                }
                echo "<br>Total Price: ".$totalprice."<br>Total Qty: ".$totalqty; ?>
                <!-- <span class="item">
                  <span class="item-left">
                    <img src="http://lorempixel.com/50/50/" alt="" />
                    <span class="item-info">
                      <span>Item name</span>
                      <span>23$</span>
                    </span>
                  </span>
                  <span class="item-right">
                    <button class="btn btn-xs btn-danger pull-right">x</button>
                  </span>
                </span> -->
              </li>
              <li class="divider"></li>
              <?php if (isset($_SESSION['shipmode'])){
              echo "<li><a class='text-center' href='/finished'>View Cart</a></li>";
            }else{ echo "<li><a class='text-center' href='/ship-mode'>View Cart</a></li>"; } ?>

            </ul>
          </li>
        </ul>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

  <div class="toparea" style="margin-top:60px">
    <div class="row">
      <div class="col-sm-4" style="padding:5px;text-align:center">
        <img alt="LONDON Techntrade" src="http://londontt.com/wp-content/themes/fixit/assets/phone-repair/images/logo.png">
        <h4><strong>LONDON TECHNTRADE</strong></h4>
      </div>
      <div class="col-sm-4" style="margin-top:4px;items-align:middle;text-align:center;color:darkgreen">
        <h3><strong>RECYCLE | RE-SALE</strong></h3>
      </div>
      <div class="col-sm-4" style="padding:5px;text-align:center">
        <img width="100" src="/assets/img/guarantee.png" alt="">
      </div>
    </div>
    <div class="row" style="">
      <h5 class="top-bar-slogan" align="center">
      At london TECHNTRADE WE PAY MOST CASH FOR YOUR PHONE / TABLET / MAC / LAPTOP
      </h5>
    </div>
  </div>
  <!-- </nav> -->

<div class="box" style="background:black;padding:30px;margin-top:-15px;text-transform:uppercase;text-align:center">
<div class="container">
  <h2 style="font-family: 'Bangers';padding:0px;margin-top:0px;color:white">Want to sell your PHONE / TABLET / MAC / LAPTOP ?<br></h2>
  <h5 style="padding:0px;margin-top:0px;color:white">At London Techntrade we will offer you most cash, even we buying broken / damaged device.<br></h5>
</div>
</div>

<h4 style="border-top:1px solid lightgray;border-bottom:1px solid lightgray;background:#8BC34A;padding:10px;margin-top:0px;text-transform:uppercase;text-align:center">
  <span class="glyphicon glyphicon-arrow-down"></span>
  Chose Your Brand here
</h4>


<script type="text/javascript">
function showUser(str) {
    if (str == "") {
        document.getElementById(str).innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('apple').innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","/tabload/"+str+"",true);
        xmlhttp.send();
    }
}
//
// Carousel Auto-Cycle
  $(document).ready(function() {
    $('.carousel').carousel({
      interval: 2000
    })
  });

</script>

<ul class="nav nav-tabs nav-justified" style="padding:0px;font-family: 'Bangers';border-right: 1px solid lightgray;border-left: 1px solid lightgray;color:green;margin-top:-11px;font-size:20px">
  <li><a style="color:green;" onclick="showUser(this.text)" data-toggle="tab" href="#apple">APPLE</a></li>
  <li><a style="color:green;" onclick="showUser(this.text)" data-toggle="tab" href="#apple">SAMSUNG</a></li>
  <li><a style="color:green;" onclick="showUser(this.text)" data-toggle="tab" href="#apple">SONY</a></li>
  <li><a style="color:green;" onclick="showUser(this.text)" data-toggle="tab" href="#apple">MICROSOFT</a></li>
  <li><a style="color:green;" onclick="showUser(this.text)" data-toggle="tab" href="#apple">HTC</a></li>
  <li><a style="color:green;" onclick="showUser(this.text)" data-toggle="tab" href="#apple">Google</a></li>
  <li><a style="color:green;" onclick="showUser(this.text)" data-toggle="tab" href="#apple">BlackBerry</a></li>
  <li><a style="color:green;" onclick="showUser(this.text)" data-toggle="tab" href="#apple">Amazon</a></li>
</ul>
<div id="apple" class="tab-pane fade" style="border-left: 1px solid lightgray;border-bottom: 1px solid lightgray;border-right: 1px solid lightgray;width:100%;background:white;margin-top:0px;margin-bottom:0px;padding:0px"></div>
<h2 style="margin-top:0px;font-family: 'Bangers';margin-bottom:0px;border-left: 1px solid darkgreen;border-top: 1px solid darkgreen;border-bottom: 1px solid darkgreen;border-right: 1px solid darkgreen;width:100%;background:green;font-size: 50;font-weight: 90;;text-align:center;color:white;padding:20px">What do we buy?</h2>
  <!-- <h3 style="margin-top:0px;margin-bottom:0px;border-left: 1px solid darkgreen;border-top: 0px solid darkgreen;border-bottom: 1px solid darkgreen;border-right: 1px solid darkgreen;width:100%;background:white;font-size: 20;font-weight: 100;text-align:left;color:#202020;padding:40px;display:flex;justify-content: left;vertical-align:middle">
    <div class="container text-center" style="font-size: 20px;font-weight: 800;text-align:;color:#202020;">
      <style>
      *{
        margin:0;
        padding:0;
      }
  .img-responsive{
    display:inline;
  }
      </style>
  <div class="col-md-4">
    <img src="https://images.frys.com/art/product/big/9339370.01.big.jpg" class="img-round thumbnail img-responsive" style="max-width:250px;" alt="">
    <h3>IPHONES, IPADS & MAC</h3>
  </div>
  <div class="col-md-4">
    <img src="https://phoneplacekenya.com/wp-content/uploads/2017/09/samsung-galaxy-s7-edge.jpg" class="img-round thumbnail img-responsive" style="max-width:250px;" alt="">
    <h3>ALL SAMSUNG SMARTPHONE (2016 or later released)</h3>
  </div>
  <div class="col-md-4">
    <img src="https://s3-ap-southeast-2.amazonaws.com/wc-prod-pim/Category_400x400/convertible-cat.jpg" class="img-round thumbnail img-responsive" style="max-width:250px;" alt="">
    <h3>LAPTOP & DESKTOP</h3>
  </div>
  </div>
  </h3> -->
<br>
  <div class="carousel slide" id="myCarousel">
    <div class="carousel-inner">
      <div class="item active">
        <ul class="thumbnails">
          <li class="col-sm-3" style="display: block;">
            <div class="fff">
              <div class="caption">
                <h4>IPHONE</h4>
                <p>IPHONE 8,IPHONE X, IPHONE 7, IPHONE 6</p>
                <!-- <a class="btn btn-mini" href="#">» Read More</a> -->
              </div>
              <div class="thumbnail">
                <img src="https://images.frys.com/art/product/big/9339370.01.big.jpg" alt="">
              </div>

            </div>
          </li>
          <li class="col-sm-3" style="display: block;">
            <div class="fff">
              <div class="caption">
                <h4>SAMSUNG</h4>
                <p>SAMSUNG SMARTPHONE (2016 or later released)</p>
                <!-- <a class="btn btn-mini" href="#">» Read More</a> -->
              </div>
              <div class="thumbnail">
                <img src="https://phoneplacekenya.com/wp-content/uploads/2017/09/samsung-galaxy-s7-edge.jpg" alt="">
              </div>

            </div>
          </li>
          <li class="col-sm-3" style="display: block;">
            <div class="fff">
              <div class="caption">
                <h4>IPAD & GALAXY TAB</h4>
                <p>Apple and samsunge tab</p>
                <!-- <a class="btn btn-mini" href="#">» Read More</a> -->
              </div>
              <div class="thumbnail">
                <img src="http://cdn.shopify.com/s/files/1/1043/3082/products/iPad10.5_grande.png" alt="">
              </div>

            </div>
          </li>
          <li class="col-sm-3" style="display: block;">
            <div class="fff">
              <div class="caption">
                <h4>MAC & Laptop</h4>
                <p>MAC Pro, MAC AIR, Samsung, Dell, HP laptop etc</p>
                <!-- <a class="btn btn-mini" href="#">» Read More</a> -->
              </div>
              <div class="thumbnail">
                <img src="https://i.pinimg.com/736x/5d/e7/57/5de757616ac9dd7e0768f19d6c9b158e--macbook-case-laptop-cases.jpg" alt="">
              </div>

            </div>
          </li>
      </ul>
    </div><!-- /Slide1 -->
  </div>


 <!-- <nav>
  <ul class="control-box pager">
    <li><a data-slide="prev" href="#myCarousel" class=""><i class="glyphicon glyphicon-chevron-left"></i></a></li>
    <li><a data-slide="next" href="#myCarousel" class=""><i class="glyphicon glyphicon-chevron-right"></i></a></li>
  </ul>
</nav> -->
 <!-- /.control-box -->

</div><!-- /#myCarousel -->
<br>
  <div class="section" style="background:white">
    <h2 style="margin-top:0px;font-family: 'Bangers';margin-bottom:0px;border-left: 1px solid darkgreen;border-top: 1px solid darkgreen;border-bottom: 1px solid darkgreen;border-right: 1px solid darkgreen;width:100%;background:green;font-size: 50;font-weight: 90;;text-align:center;color:white;padding:20px">How it's work?</h2>
    <div class="container" style="background:#FFFFFF;color:darkgray;margin-top:0px;margin-bottom:10px;">

        <article>
            <font color=green><h3>Sell your any electronics device - How to Sell My APPLE/SAMSUNG/MICROSOFT/SONY or other brands electronics devices</h3></font>
            <h4><p><strong>Sell your APPLE/SAMSUNG/MICROSOFT/SONY and others brand electronics device with Londontt.com/recycle - it’s simple and very simple!</strong></p></h4>
            <p>There are many websites are buying your APPLE/SAMSUNG/MICROSOFT/SONY devices but they all pay different prices but we pay higest possiblle amount as much as we can. It takes time searching all the APPLE/SAMSUNG/MICROSOFT/SONY or other devices buyback companies for the best deal. This is where LONDON Techntrade comes in. Our unique price comparison engine compares prices from all the APPLE/SAMSUNG/MICROSOFT/SONY buyback companies in seconds to get you the best deal. As we compare more Apple APPLE/SAMSUNG/MICROSOFT/SONY trade in companies than anyone else you are always guaranteed of the most cash when selling an APPLE/SAMSUNG/MICROSOFT/SONY!</p>
            <p>Best Price Guarantee - at Londontt.com/recycle you are guaranteed of the most cash for your used iPhone. We offer a Best Price Guarantee for all APPLE/SAMSUNG/MICROSOFT/SONY trade ins. If you find a better deal for your used iPhone anywhere else we’ll pay you double the difference! See our iPhone Trade In <a href="/why-use-londontt">Best Price Guarantee</a> for more details</p>
            <hr>
            <font color=green><h3>APPLE/SAMSUNG/MICROSOFT/SONY and others brand electronics - How it Works:</h3></font>
            <p><strong>Step 1:</strong></p>
            <p>Select your iPhone model above to find out how much your APPLE/SAMSUNG/MICROSOFT/SONY devices is worth</p>
            <p><strong>Step 2:</strong></p>
            <p>Our unique price comparison engine compares prices from all the leading APPLE/SAMSUNG/MICROSOFT/SONY and others brand electronics buyers in the US to get you the best deal for your old APPLE/SAMSUNG/MICROSOFT/SONY and others brand electronics. Choose who you want to ‘Sell Now’</p>
            <p><strong>Step 3:</strong></p>
            <p>Fill in your details and follow the instructions about how to ship your phone to your chosen APPLE/SAMSUNG/MICROSOFT/SONY and others brand electronics buyer.</p>
            <p><strong>Step 4:</strong></p>
            <p>Once the iPhone buyer receives your device they will test it and then send you payment by your chosen payment method. So, just sit back and wait for your cash!</p>
            <hr>
            <font color=green><h3>Want to Sell a Broken APPLE/SAMSUNG/MICROSOFT/SONY and others brand electronics?</h3></font>
            <p>Don’t worry, you can still sell broken APPLE/SAMSUNG/MICROSOFT/SONY and others brand electronics! All our APPLE/SAMSUNG/MICROSOFT/SONY and others brand electronics buyback companies buy broken APPLE/SAMSUNG/MICROSOFT/SONY and others brand electronics. Obviously if you have a damaged APPLE/SAMSUNG/MICROSOFT/SONY and others brand electronics then you are not going to get full value but you can still get up to 80% of full value depending on the damage.</p>
            <p>To sell broken iPhone, click your APPLE/SAMSUNG/MICROSOFT/SONY and others brand electronics above to go to the comparison page with all the trade in prices. On this page under ‘Condition’ select ‘Broken’ to see the broken APPLE/SAMSUNG/MICROSOFT/SONY and others brand electronics prices</p>
            <hr>
          </article>
    </div>
  </div>
  <?php require_once 'foot.php'; ?>
</body>

</html>
