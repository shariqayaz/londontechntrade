<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Thank You! - Your Submission has been recieved.">
    <meta name="robots" content="index, follow">
    <meta property="og:description" content="Repair stuff, sell your stuff">
    <meta property="og:title" content="Repair Your Stuff, Sell Your Stuff" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.londontt.com/thank-you" />
    <meta NAME="geo.region" content="london">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script> -->

    <!-- fonts -->
    <link href='https://fonts.googleapis.com/css?family=Anton' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet'>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <link rel="stylesheet" href="/bootstrap-4.0.0/css/bootstrap.css">
    <script src="/js/jquery/jquery-3.2.1.js"></script>
    <script src="/js/jquery/popper.js"></script>
    <script src="/bootstrap-4.0.0/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>
    <link rel="stylesheet" href="/css/style.css" type="text/css"/>

<title>Thank You! - Your Submission has been recieved - London Techntrade</title>
</head>
<body style="background:#f6f6f6">
  <?php require_once "_viewControllers/commonview.php"; $commonview = new CommonView();$commonview->vRet('headvone'); ?>
<br>

<h1>Thank You!</h1>
<h3>Your Submission has been recieved.</h3>

<?php require_once "_viewControllers/commonview.php"; $commonview = new CommonView();$commonview->vRet('footvone'); ?>

</body>
</html>
