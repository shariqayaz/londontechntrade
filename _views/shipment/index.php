<?php
require_once '_controllers/login.php';
$login = new login();
$bool = false;
$user_id = "0";
if(login::is_loggedin()!="")
{
  $user_id = $_SESSION['user_session'];
	$stmt = $login->runQuery("SELECT * FROM tbl_users WHERE user_id=:user_id");
	$stmt->execute(array(":user_id"=>$user_id));
	$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
  $bool = true;
}
else
{
  $bool = false;
  $user_id = "0";
}
 ?>
<?php
 if(isset($_POST['shipsubmit'])){
   $get_ship = $_POST['shipradio'];

   //require_once "_sysconfig/Safety_heaven.php";
   $shcall = new SafeHeaven();
   $get_ship = $shcall->xss_clean($get_ship);
   $_SESSION['shipmode'] = $get_ship;
   echo $_SESSION['shipmode'];
   // prepare data to insert into session
    unset($_POST['shipsubmit']);
    header('location: /finished');

   // carry forward to shipment method

 }
 ?>

 <!DOCTYPE html>
 <html>
   <head>
     <meta charset="utf-8">
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
     <meta name="description" content="sell smartphone buy used phone recycle cell phone unlocking repair. At Londontt.">
     <meta NAME="geo.region" content="london">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.css">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script> -->

     <!-- fonts -->
     <link href='https://fonts.googleapis.com/css?family=Anton' rel='stylesheet'>
     <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet'>
     <link rel="stylesheet" href="/bootstrap-4.0.0/css/bootstrap.css">
     <script src="/js/jquery/jquery-3.2.1.js"></script>
     <script src="/js/jquery/popper.js"></script>
     <script src="/bootstrap-4.0.0/js/bootstrap.min.js"></script>
     <script src="/js/script.js"></script>
     <link rel="stylesheet" href="/css/style.css" type="text/css"/>
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

 <title>LONDON TECHNTRADE</title>
 <style type="text/css">
   .funkyradio input[type="radio"]:hover:not(:checked) ~ label:before, .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label:before {
    content:'\2714';
    text-indent: 0em;
    /*line-height: 2.5em;*/
    color: #00a8c5;
    padding-top:12px;
    /*border: 1px solid black;*/
}
.funkyradio-primary input[type="radio"]:checked ~ label:before, .funkyradio-primary input[type="checkbox"]:checked ~ label:before {
    color: black;
    padding-top:15px;
    background-color: #00a8c5;
}
 </style>
 </head>
 <body style="background:#f6f6f6">
   <?php require_once "_viewControllers/commonviewhome.php"; $commonview = new CommonView();$commonview->vRet('headvone'); ?>


<div class="" style="background:#fff;padding:10px;margin-top:0px;text-transform:uppercase;text-align:center">
<div class="container">
  <br>
  <h3 style="padding:0px;margin-top:0px;color:">How To Send Device?</h3>
  <font class="display-5" style="margin-bottom:0px;text-transform:uppercase;font-size: 14px;"><strong> Please choose your FREE send option
</strong></font>
<br>
<br>
</div>
</div>
<div style="width:100%;text-align:center;margin:0px;background:#fff;" class="container-fluid">
  <div class="row" style="margin:0px">
    <form class="" action="" method="post">
    <div class="col-md-12">
    <div class='funkyradio'>
    <div style="margin:5px" class='funkyradio-primary'>
        <input onchange='getCheckedValue()' style='' type='radio' name='shipradio' id='courierdrop' value="courierdrop" checked/>
        <label style='width:100%;margin:0px;padding: 12px;font-weight: bold;' for='courierdrop'>ROYAL MAIL <hr style="width:100%;border-color:black;margin-bottom:0px">
            <div style="padding-left: 40px;padding-right: 10px;text-align:justify;">
              <h3 style="margin-top: 10px;">Send your items for FREE from your local Post Office.</h3>
              <ul style="margin: 0;padding: 0;list-style-type: none;">
                <li style="margin: 0;padding: 0;">1: Sending your stuff for FREE is so easy. It’s quick, convenient and there are thousands of branches to choose from!</li>
                <li style="margin: 0;padding: 0;">2: Send from over 11,500 Post Office branches. Find your nearest here.</li>
                <li style="margin: 0;padding: 0;">3: Your items should arrive within 48 hours.</li>
                <li style="margin: 0;padding: 0;">4: Your items are insured with proof of postage.</li>
              </ul>
              <br>
            <div class="breadcrumb" style="text-align: center;">Your items are fully trackable so you can see how your order is progressing.
              We’ll email your Pack & Send Guide after you’ve completed your order. Simply print out and attach the label included to your box and send it for FREE! If you’d prefer us to post your label, just select the ‘post’ option on the order review screen. Find your nearest Post Office <a href="">here</a>.
            </div>
            </div>
        </label>
      </div>
      </div>  
    </div>
  <div class="col-md-12">
  <div class='funkyradio'>
    <div style="margin:5px" class='funkyradio-primary'>
      <input onchange='getCheckedValue()'  type='radio' name='shipradio' id='dpddrop' value="dpddrop" />
      <label style='width:100%;margin:0px;padding: 12px;font-weight: bold;' for='dpddrop'>DPD<hr style="width:100%;border-color:black;margin-bottom:0">
      <div style="padding-left: 40px;padding-right: 10px;text-align:justify;">
        <h3 style="margin-top: 10px;">Send your items for FREE with DPD Drop Off</h3>
        <p style="margin:0px;padding: 0">Sending your items using DPD Drop Off is fast, easy and completely FREE. Simply take your box down to your nearest DPD Drop Off point and they’ll handle the rest. </p>
        <ul style="margin: 0;padding: 0;list-style-type: none;">
          <li style="margin: 0;padding: 0;">1: Over 2,500 shops across the UK, all with convenient opening hours.</li>
          <li style="margin: 0;padding: 0;">2: 96% of the population live within 5 miles of a shop.</li>
          <li style="margin: 0;padding: 0;">3: Your items are insured with proof of postage.</li>
          <li style="margin: 0;padding: 0;">4: Your items are fully trackable.</li>
        </ul>
        <br>
      <div class="breadcrumb" style="text-align: center;">We'll email your Pack & Send Guide after you’ve completed your order. Simply print out and attach the label included to your box and take it down to your nearest DPD Drop Off shop. The assistant will scan your parcel and provide a tracking receipt, and your items will arrive with us shortly after. <br>Find nearest shop <a href="">Click here</a>.
      </div>
      </div>
      </label>
    </div>
    </div>
  </div>
  <div class="col-md-12" style="top: -30px;">
   <div class='funkyradio' style="">
    <div style="margin:5px" class='funkyradio-primary'>
      <input onchange='getCheckedValue()' style='margin-bottom:0px' type='radio' name='shipradio' id='selfdrop' value="selfdrop">
      <label style='padding: 15px;font-weight: bold;' for='selfdrop'>I'll DROP IT MYSELF</label>
    </div>
  </div>
  </div>
  <div class="col-md-12" style="margin-top: -68px;">
  <div class='funkyradio'>
    <div style="margin:5px" class='funkyradio-primary'>
      <input onchange='getCheckedValue()' style='margin-bottom:0px' type='radio' name='shipradio' id='nodrop' value="nodrop" />
      <label style='padding: 15px;font-weight: bold;' for='nodrop'>HAVEN'T DECIDED YET</label>
    </div>
  </div>
  </div>
  </div>
</div>
<!-- <hr style="width:90%"> -->
<div class="clearfix"></div>
<div class="text-center" style="background:#fff;padding-bottom: 10px;">
<input type="submit" id="submit" name="shipsubmit" class="btn btn-primary pull-center" value="Checkout Now" />
<!-- <input type="submit" id="submit" name="shipsubmit" class="btn btn-primary pull-center" value="NEXT ->" style="background:black;font-size:18px;width:200px;height:50px" /> -->
<div class="clearfix"></div>

</div>

</form>

</div>
<?php require_once "_viewControllers/commonview.php"; $commonview = new CommonView();$commonview->vRet('footvone'); ?>

</body>
</html>
