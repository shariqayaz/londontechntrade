<?php
require_once '_controllers/login.php';
$login = new login();
$bool = false;
$user_id = "0";
if(login::is_loggedin()!="")
{
  $user_id = $_SESSION['user_session'];
	$stmt = $login->runQuery("SELECT * FROM tbl_users WHERE user_id=:user_id");
	$stmt->execute(array(":user_id"=>$user_id));
	$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
  $bool = true;
}
else
{
  $bool = false;
  $user_id = "0";
}
 ?>
<?php
 if(isset($_POST['shipsubmit'])){
   $get_ship = $_POST['shipradio'];

   //require_once "_sysconfig/Safety_heaven.php";
   $shcall = new SafeHeaven();
   $get_ship = $shcall->xss_clean($get_ship);
   $_SESSION['shipmode'] = $get_ship;
   echo $_SESSION['shipmode'];
   // prepare data to insert into session
    unset($_POST['shipsubmit']);
    header('location: /finished');

   // carry forward to shipment method

 }
 ?>

  <!DOCTYPE html>
  <html lang="en">
  <head>
    <title>Shipment</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href='//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' rel='stylesheet'/>
    <script data-require="jquery" data-semver="2.0.3" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Bangers' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Anton' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Cairo' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Archivo' rel='stylesheet'>

    <link href='https://fonts.googleapis.com/css?family=Alfa Slab One' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Anton' rel='stylesheet'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style media="screen">
.top-bar-slogan{
  text-transform:uppercase;
  padding:10px;
  border-top:solid #8BC34A;
  border-bottom:solid #8BC34A;
  margin-bottom:15px;
  text-align:center;
  background: darkgreen;
  color:white;
}

footer.nb-footer {
background: #222;
border-top: 4px solid darkgreen; }
footer.nb-footer .about {
margin: 0 auto;
margin-top: 40px;
max-width: 1170px;
text-align: center; }
footer.nb-footer .about p {
font-size: 13px;
color: #999;
margin-top: 30px; }
footer.nb-footer .about .social-media {
margin-top: 15px; }

footer.nb-footer .about .social-media ul li a {
    display: inline-block;
    width: 60px;
    height: 60px;
    line-height: 0px;
    border-radius: 50%;
    font-size: 35px;
    color: white;
    border: 1px solid rgba(255, 255, 255, 0.3);
}

footer.nb-footer .about .social-media ul li a:hover {
background: green;
color: #fff;
border-color: green }

footer.nb-footer .footer-info-single {
margin-top: 30px; }

footer.nb-footer .footer-info-single .title {
color: #aaa;
text-transform: uppercase;
font-size: 16px;
border-left: 4px solid #b78c33;
padding-left: 5px; }

footer.nb-footer .footer-info-single ul li a {
display: block;
color: #aaa;
padding: 2px 0; }
footer.nb-footer .footer-info-single ul li a:hover {
color: #b78c33; }
footer.nb-footer .footer-info-single p {
font-size: 13px;
line-height: 20px;
color: #aaa; }
footer.nb-footer .copyright {
margin-top: 15px;
background: #111;
padding: 7px 0;
color: #999; }
footer.nb-footer .copyright p {
margin: 0;
padding: 0; }

/*---*/




input {
  display: none;
}

.clrbutton {
  display: inline-block;
  position: relative;
  width: 60px;
  height: 60px;
  margin: 20px;
  cursor: pointer;
}

.clrbutton span {
  display: block;
  position: absolute;
  width: 80px;
  height: 40px;
  padding: 0;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  -o-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  border-radius: 5%;
  background: #eeeeee;
  box-shadow: 2px 2px 5px 0 rgba(0, 0, 0, 0.25);
  transition: ease .3s;
}

.clrlayer {
  display: block;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  background: transparent;
  /*transition: ease .3s;*/
  z-index: -1;
}

.clrbutton span:hover {
  padding: 25px;
}

    .recycler-price {
    border: 1px solid green;
    border-radius: 5px;
    margin-bottom: 10px;
    width: 100%;
    padding: 5px;
    opacity: 1;
    transition: opacity .25s ease-in-out;
    -moz-transition: opacity .25s ease-in-out;
    -webkit-transition: opacity .25s ease-in-out;
}
    label > input{ /* HIDE RADIO */
    visibility: hidden; /* Makes input not-clickable */
    position: absolute; /* Remove input from document flow */
    }
    label > input + img{ /* IMAGE STYLES */
    cursor:pointer;
    border:2px solid transparent;
    }
    label > input:checked + img{ /* (RADIO CHECKED) IMAGE STYLES */
      border:2px solid green;
      border-radius: 5px;
    }

    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css') .funkyradio div {
        clear: both;
        /*margin: 0 50px;*/
        overflow: hidden;
    }
    .funkyradio label {
        /*min-width: 400px;*/
        width: 100%;
        border-radius: 5px;
        border: 2px solid darkgreen;
        font-weight: normal;
        font-family: 'cairo';
    }

    .funkyradio label:hover {
        /*min-width: 400px;*/
        width: 100%;
        border-radius: 5px;
        border: 3px solid darkgreen;
        font-weight: normal;
        font-family: 'cairo';
    }
    .funkyradio input[type="radio"]:empty, .funkyradio input[type="checkbox"]:empty {
        display: none;
    }
    .funkyradio input[type="radio"]:empty ~ label, .funkyradio input[type="checkbox"]:empty ~ label {
        position: relative;
        line-height: 2.5em;
        text-indent: 0.5em;
        margin: 2em;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .funkyradio input[type="radio"]:empty ~ label:before, .funkyradio input[type="checkbox"]:empty ~ label:before {
        position: absolute;
        display: block;
        top: 2;
        bottom: 120;
        left: 2;
        content:'';
        width: 2.5em;
        height: 2.5em;
        background: #F8F8FF;
        border-radius: 3px 3px 3px 3px;
    }
    .funkyradio input[type="radio"]:hover:not(:checked) ~ label:before, .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label:before {
        content:'\2714';
        text-indent: .9em;
        color: green;
    }
    .funkyradio input[type="radio"]:hover:not(:checked) ~ label, .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label {
        color: black;
        font-size: 13px;
        font-family: 'cairo';
        font-weight: bold;
    }
    .funkyradio input[type="radio"]:checked ~ label:before, .funkyradio input[type="checkbox"]:checked ~ label:before {
        content:'\2713';
        text-indent: 0.1em;
        color: green;
        background-color: #ccc;
    }
    .funkyradio input[type="radio"]:checked ~ label, .funkyradio input[type="checkbox"]:checked ~ label {
        color: black;
        font-size: 18px;
        font-family: 'cairo';
        font-weight: bold;
        padding: 0px;
        margin: 0px;
    }
    .funkyradio input[type="radio"]:focus ~ label:before, .funkyradio input[type="checkbox"]:focus ~ label:before {
        box-shadow: 5px 0 0 4px #999;
    }
    .funkyradio-default input[type="radio"]:checked ~ label:before, .funkyradio-default input[type="checkbox"]:checked ~ label:before {
        color: green;
        background-color: #1655a6;
    }
    .funkyradio-primary input[type="radio"]:checked ~ label:before, .funkyradio-primary input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #1655a6;
    }
    .funkyradio-success input[type="radio"]:checked ~ label:before, .funkyradio-success input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #5cb85c;
        background: ;
    }
    .funkyradio-danger input[type="radio"]:checked ~ label:before, .funkyradio-danger input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #d9534f;
    }
    .funkyradio-warning input[type="radio"]:checked ~ label:before, .funkyradio-warning input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #f0ad4e;
    }
    .funkyradio-info input[type="radio"]:checked ~ label:before, .funkyradio-info input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #5bc0de;
    }

    .nav>li>a {
      position: relative;
      display: block;
      padding: 20px 15px;
      border-radius: 0px !important;
    }

    .h4 {
        color: #484848;
        margin-top: 10px;
        margin-bottom: 5px;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 20px;
        font-weight: bold;
        text-decoration: none;
        display: block;
    }

    a.btnsell {
        background: #ffb600;
        box-shadow: inset -1px 1px 1px #EEb600,0 0 1px #505;
        border-radius: 2px;
        padding: 5px 10px;
        text-shadow: -1px 1px 1px #e38201;
        width: 80%;
        border: 1px solid #c48c00;
        max-width: 150px;
    }

    a.btnsell:hover {
        box-shadow: inset -1px 1px 1px #888,0 0 1px #555;
        border: 1px solid #444;
        text-shadow: -1px 1px 1px #444;
    }
    @media (max-width: 786px){
      .funkyradio label {
          /*min-width: 400px;*/
          width: 100%;
          border-radius: 5px;
          border: 2px solid darkgreen;
          font-weight: normal;
          font-family: 'cairo';
      }

      .funkyradio label:hover {
          /*min-width: 400px;*/
          width: 100%;
          border-radius: 5px;
          border: 1px solid darkgreen;
          font-weight: normal;
          font-family: 'cairo';
      }
      .funkyradio input[type="radio"]:empty, .funkyradio input[type="checkbox"]:empty {
          display: none;
      }
      .funkyradio input[type="radio"]:empty ~ label, .funkyradio input[type="checkbox"]:empty ~ label {
          position: relative;
          line-height: 1.4em;
          text-indent: 2.5em;
          margin: 4em;
          cursor: pointer;
          -webkit-user-select: none;
          -moz-user-select: none;
          -ms-user-select: none;
          user-select: none;
      }
      .funkyradio input[type="radio"]:empty ~ label:before, .funkyradio input[type="checkbox"]:empty ~ label:before {
          position: absolute;
          display: block;
          top: 1;
          bottom: 120;
          left: 1;
          content:'';
          width: 1.4em;
          height: 1.4em;
          background: #F8F8FF;
          border-radius: 2px 2px 3px 3px;
      }
      .funkyradio input[type="radio"]:hover:not(:checked) ~ label:before, .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label:before {
          content:'\2714';
          text-indent: .7em;
          color: green;
      }
      .funkyradio input[type="radio"]:hover:not(:checked) ~ label, .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label {
          color: black;
          font-size: 10px;
          font-family: 'cairo';
          font-weight: bold;
      }
      .funkyradio input[type="radio"]:checked ~ label:before, .funkyradio input[type="checkbox"]:checked ~ label:before {
          content:'\2713';
          text-indent: 0em;
          color: green;
          background-color: #ccc;
      }
      .funkyradio input[type="radio"]:checked ~ label, .funkyradio input[type="checkbox"]:checked ~ label {
          color: black;
          font-size: 16px;
          font-family: 'cairo';
          margin-left: 2px;
          margin-right: -2px;
          font-weight: bold;
          padding: 0px;
          margin: 0px;
      }
      .funkyradio input[type="radio"]:focus ~ label:before, .funkyradio input[type="checkbox"]:focus ~ label:before {
          box-shadow: 5px 0 0 4px #999;
      }
      .funkyradio-default input[type="radio"]:checked ~ label:before, .funkyradio-default input[type="checkbox"]:checked ~ label:before {
          color: green;
          background-color: #1655a6;
      }
      .funkyradio-primary input[type="radio"]:checked ~ label:before, .funkyradio-primary input[type="checkbox"]:checked ~ label:before {
          color: #fff;
          background-color: #1655a6;
      }
      .funkyradio-success input[type="radio"]:checked ~ label:before, .funkyradio-success input[type="checkbox"]:checked ~ label:before {
          color: #fff;
          background-color: #5cb85c;
          background: ;
      }
      .funkyradio-danger input[type="radio"]:checked ~ label:before, .funkyradio-danger input[type="checkbox"]:checked ~ label:before {
          color: #fff;
          background-color: #d9534f;
      }
      .funkyradio-warning input[type="radio"]:checked ~ label:before, .funkyradio-warning input[type="checkbox"]:checked ~ label:before {
          color: #fff;
          background-color: #f0ad4e;
      }
      .funkyradio-info input[type="radio"]:checked ~ label:before, .funkyradio-info input[type="checkbox"]:checked ~ label:before {
          color: #fff;
          background-color: #5bc0de;
      }
      .top-bar-slogan{
        text-transform:uppercase;
        padding:10px;
        border-top:solid #8BC34A;
        border-bottom:solid #8BC34A;
        margin-bottom:15px;
        text-align:center;
        background: darkgreen;
        color:white;
      }


      .nav-pills>li {
          float: left;
      }
      .nav-pills {

      }
      .nav>li>a {
        padding: 13px 18px;
        margin-bottom: 0px;
        font-size: 13px;
        position: relative;
        display: inline-block;
      }
    }
    </style>
  </head>
  <body style="background:;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',sans-serif;">


  <nav class="navbar navbar-default navbar-fixed-top" style="padding:0px">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header" style="margin-left:3px">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <h4 style="vertical-align:middle;margin-top:20px;margin-left:-7px;margin-right:7px;" ><strong>LONDON TECHNTRADE</strong></h4>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="background:white;">
      <ul class="nav navbar-nav" style="margin-left:0px">
        <li class="active"><a class="topmenu-a" style="background:green;color:white" href="#"><strong>RECYCLE | RE-SALE</strong><span class="sr-only">(current)</span></a></li>
        <li><a class="topmenu-a" href="#">UNLOCKING</a></li>
        <li><a class="topmenu-a" href="#">REPAIRING</a></li>
        <li><a class="topmenu-a" href="#">SHOP</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right" style="margin-left:0px">
          <?php if ($bool == true) { ?>
            <li>
              <a><font color=green>LOGIN: <strong><?php echo $userRow['user_name']; ?></strong></font>
              </a>
            </li>
            <li>
              <a href='/logout'><font color=red>LOGOUT </font><span class="glyphicon glyphicon-log-out"></span></a>
            </li>
          <?php }else if ($bool == false){?>
            <li>
              <a href='/login'><font color=black>LOGIN</font></a>
              </li>
              <li><a href='/sign-up' style="color:black">SIGN UP</a></li>
          <?php } ?>
        <ul class="nav navbar-nav navbar-right" style="margin-right:2px">
          <li class="dropdown" style="margin-left:0px">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <span class="glyphicon glyphicon-shopping-cart"></span> <?php echo count($_SESSION['sell_cart']); ?> - Items<span class="caret"></span></a>
            <ul class="dropdown-menu dropdown-cart" role="menu" style="width:500px;height:auto;">
              <li>
                <?php $totalprice = 0;
                $totalqty = 0;
                foreach ($_SESSION['sell_cart'] as $sell_cart_item => $value) {
                  if ($bool==true) {
                    if ($_SESSION['sell_cart'][$sell_cart_item]['user_id']=="0" || $_SESSION['sell_cart'][$sell_cart_item]['user_id']=="") {
                      $_SESSION['sell_cart'][$sell_cart_item]['user_id'] = $_SESSION['user_session'];
                    }
                  }
                  echo "User ID: ".$_SESSION['sell_cart'][$sell_cart_item]['user_id']." Product ID: ".$_SESSION['sell_cart'][$sell_cart_item]['product_id']." Model: ".$_SESSION['sell_cart'][$sell_cart_item]['model']." Capacity: ".$_SESSION['sell_cart'][$sell_cart_item]['cap']." Condition: ".$_SESSION['sell_cart'][$sell_cart_item]['con']." Network: ".$_SESSION['sell_cart'][$sell_cart_item]['net']." Color: ".$_SESSION['sell_cart'][$sell_cart_item]['col'];
                  echo " Price: ".$_SESSION['sell_cart'][$sell_cart_item]['price']." Qty: ".$_SESSION['sell_cart'][$sell_cart_item]['qty']."<br>";
                  echo "<br>";
                  $totalprice += $_SESSION['sell_cart'][$sell_cart_item]['qty'] * $_SESSION['sell_cart'][$sell_cart_item]['price'];
                  $totalqty +=$_SESSION['sell_cart'][$sell_cart_item]['qty'];echo "";
                  echo "<li class='divider'></li>";
                }
                echo "<br>Total Price: ".$totalprice."<br>Total Qty: ".$totalqty; ?>
                <!-- <span class="item">
                  <span class="item-left">
                    <img src="http://lorempixel.com/50/50/" alt="" />
                    <span class="item-info">
                      <span>Item name</span>
                      <span>23$</span>
                    </span>
                  </span>
                  <span class="item-right">
                    <button class="btn btn-xs btn-danger pull-right">x</button>
                  </span>
                </span> -->
              </li>
              <li class="divider"></li>
              <?php if (isset($_SESSION['shipmode'])){
              echo "<li><a class='text-center' href='/finished'>View Cart</a></li>";
            }else{ echo "<li><a class='text-center' href='/ship-mode'>View Cart</a></li>"; } ?>

            </ul>
          </li>
        </ul>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

  <div class="toparea" style="margin-top:60px">

     <div class="row">
       <div class="col-sm-4" style="padding:5px;text-align:center">
         <img alt="LONDON Techntrade" src="http://londontt.com/wp-content/themes/fixit/assets/phone-repair/images/logo.png">
                   <h4><strong>LONDON TECHNTRADE</strong> </h4>
       </div>
       <div class="col-sm-4" style="text-transform:uppercase;vertical-align:middle;text-align:center;color:darkgreen">
         <h4 style="margin-bottom:2px">store time</h4>
         <h3 style="color:darkgreen;margin-top:-4px;margin-bottom:5px;margin-left:8px"><strong>9AM - 10PM</strong></h3>
         <h4 style="margin-bottom:2px">Need help?</h4>
         <h3 style="color:darkgreen;margin-top:-4px;margin-left:8px"><strong> 020 8691 7978</strong></h3>


       </div>
       <div class="col-sm-4" style="padding:5px;text-align:center">
           <img width="100" src="/assets/img/guarantee.png" alt="">
       </div>
     </div>
       <div class="row">
         <h5 class="top-bar-slogan" align="center">
           At london TECHNTRADE WE PAY MOST CASH FOR YOUR PHONE / TABLET / MAC / LAPTOP
         </h5>
       </div>
   </div>
 <!-- </nav> -->

<div class="box" style="background:black;padding:10px;margin-top:-15px;text-transform:uppercase;text-align:center">
<div class="container">
  <h4 style="padding:0px;margin-top:0px;color:white">How do we get your device ?</h4>
  <h1 class="display-4" style="margin-bottom:0px;text-transform:uppercase;color:white"><strong>SELECT YOUR SHIPMENT method</strong></h1></div>
</div>
<br>
<div style="width:95%;text-align:center;margin-top:10px;" class="container">
  <form class="" action="" method="post">
  <div style="margin:10px" class="row">
    <div class='funkyradio'>
    <div class="col-md-4">
      <div style="margin:5px" class='funkyradio-primary'>
        <input onchange='getCheckedValue()' style='margin-bottom:0px' type='radio' name='shipradio' id='selfdrop' value="selfdrop" checked/>
        <label style='width:99%;height:155px;margin:0px' for='selfdrop'>I'll DROP IT MYSELF<hr style="width:100%;border-color:lightgrey;margin-bottom:-15px"></label>
      </div>
    </div>
    <div class="col-md-4">
      <div style="margin:5px" class='funkyradio-primary'>
        <input onchange='getCheckedValue()' style='margin-bottom:0px' type='radio' name='shipradio' id='courierdrop' value="courierdrop" />
        <label style='width:99%;height:155px;margin:0px' for='courierdrop'>ROYAL MAIL<hr style="width:100%;border-color:lightgrey;margin-bottom:-15px"></label>
      </div>
    </div>
    <div class="col-md-4">
      <div style="margin:5px" class='funkyradio-primary'>
        <input onchange='getCheckedValue()' style='margin-bottom:0px' type='radio' name='shipradio' id='nodrop' value="nodrop" />
        <label style='width:99%;height:155px;margin:0px' for='nodrop'>HAVEN'T DECIDED YET<hr style="width:100%;border-color:lightgrey;margin-bottom:-15px"></label>
      </div>
    </div>
  </div>
</div>
<hr style="width:90%">
<input type="submit" id="submit" name="shipsubmit" class="btn btn-primary pull-center" value="NEXT ->" style="background:black;font-size:18px;width:200px;height:50px" />

</form>
</div>
<br>
<?php require_once 'foot.php'; ?>
</body>
</html>
