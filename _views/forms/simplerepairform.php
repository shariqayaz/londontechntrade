<?php
if ($requeststr!='emptysellform' && $requeststr=='repairsimple') { ?>

  <div class="container">
      <div class="col">
        <br style="clear:both">
        <font style="text-transform:uppercase;font-weight:bold;color:#1655a6">Provide your contact details</font>

        <br style="clear:both">
        <br style="clear:both">
        <div class="form-group">
          <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Name" style="border-color:#1655a6;font-size:14px" required>
        </div>
        <div class="form-group">
          <input type="email" class="form-control" id="email" name="email" placeholder="Email" style="border-color:#1655a6;font-size:14px" required>
        </div>
        <div class="form-group">
          <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" style="border-color:#1655a6;font-size:14px" required>
        </div>
        <div class="form-group">
          <input type="text" class="form-control" id="message" name="message" placeholder="describe the problem you are facing" maxlength="200" style="border-color:#1655a6;">
        </div>

      </div>
    </div>

<?php } ?>
