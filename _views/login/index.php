<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
//
// $login = new login();

if(login::is_loggedin()!="")
{
	$login->redirect('/login');
}
else {
	//echo "12 12";
	//$login->redirect('/login');
}

if(isset($_POST['btn-login']))
{
	$uname = strip_tags($_POST['txt_uname_email']);
	$umail = strip_tags($_POST['txt_uname_email']);
	$upass = strip_tags($_POST['txt_password']);

	$shcall = new SafeHeaven();
	$uname = $shcall->xss_clean($uname);
	$umail = $shcall->xss_clean($umail);
	$upass = $shcall->xss_clean($upass);

	if(login::doLogin($uname,$umail,$upass))
	{
		login::redirect('/recycle');
	}
	else
	{
		$error = "Wrong Details !";
	}
}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="description" content="sell smartphone buy used phone recycle cell phone unlocking repair at Londontt.">
    <meta name="robots" content="index, follow">
    <meta property="og:description" content="Recycle stuff, sell your stuff">
    <meta property="og:title" content="Sell Your Stuff" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.londontt.com/" />
    <meta NAME="geo.region" content="london">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script> -->

    <!-- fonts -->
    <link href='https://fonts.googleapis.com/css?family=Anton' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet'>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="/bootstrap-4.0.0/css/bootstrap.css">
    <script src="/js/jquery/jquery-3.2.1.js"></script>
    <script src="/js/jquery/popper.js"></script>
    <script src="/bootstrap-4.0.0/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>
    <link rel="stylesheet" href="/css/style.css" type="text/css"/>

<style type="text/css">
    *{
 margin:0;
 padding: 0;   
}

.active{
    color:#0d6792;
}
</style>
</head>
<body>
<?php require_once "_viewControllers/commonview.php"; $commonview = new CommonView();$commonview->vRet('headvone'); ?>
<div class="signin-form">

	<div class="container text-center">


       <form class="form-signin" method="post" id="login-form">
        <br>
        <br>
        <img src="/assets/images/logo.png">
        <!-- <h2 class="form-signin-heading">London Techntrade</h2> -->
        <br>
        <br>
        <br>
        <h2 class="form-signin-heading">Login</h2><hr />

        <div id="error">
        <?php
			if(isset($error))
			{
				?>
                <div class="alert alert-danger">
                   <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error; ?> !
                </div>
                <?php
			}
		?>
        </div>

        <div class="form-group">
        <input type="text" class="form-control" name="txt_uname_email" placeholder="Username or E mail ID" required />
        <span id="check-e"></span>
        </div>

        <div class="form-group">
        <input type="password" class="form-control" name="txt_password" placeholder="Your Password" />
        </div>

     	<hr />

        <div class="form-group">
            <button type="submit" name="btn-login" class="btn btn-default">
                	<i class="glyphicon glyphicon-log-in"></i> &nbsp; SIGN IN
            </button>
        </div>
      	<br />
            <label>Don't have account yet? <a href="/sign-up">Sign Up!</a></label>
      </form>

    </div>

</div>

</body>
</html>
