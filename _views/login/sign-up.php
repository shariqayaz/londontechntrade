	<?php
if(!isset($_SESSION))
{ SessionMGR::init(); echo " session not started why "; exit;}

if(login::is_loggedin()!="")
{
	login::redirect('/recycle');
}

if(isset($_POST['btn-signup']))
{
	$uname = strip_tags($_POST['txt_uname']);
	$fname = strip_tags($_POST['txt_fullname']);
	$umail = strip_tags($_POST['txt_umail']);
	$upass = strip_tags($_POST['txt_upass']);
	$address = strip_tags($_POST['txt_address']);
	$phone_no = strip_tags($_POST['txt_phone_no']);
	// check for x
	$shcall = new SafeHeaven();
	$uname = $shcall->xss_clean($uname);
	$fname = $shcall->xss_clean($fname);
	$umail = $shcall->xss_clean($umail);
	$upass = $shcall->xss_clean($upass);
	$address = $shcall->xss_clean($address);
	$phone_no = $shcall->xss_clean($phone_no);

	if($uname=="")	{
		$error[] = "provide username !";
	}
	if($fname=="")	{
		$error[] = "provide Full Name !";
	}
	else if($umail=="")	{
		$error[] = "provide email id !";
	}
	else if(!filter_var($umail, FILTER_VALIDATE_EMAIL))	{
	    $error[] = 'Please enter a valid email address !';
	}
	else if($upass=="")	{
		$error[] = "provide password !";
	}
	else if(strlen($upass) < 6){
		$error[] = "Password must be atleast 6 characters";
	}
	else
	{
		try
		{
			$conn_meta = new PDO("mysql:host=localhost;dbname=irecycle2020;charset=UTF8", DB_ACCESS::meta_user(), DB_ACCESS::meta_pass());
      $conn_meta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $st_meta = $conn_meta->prepare("SELECT user_name, user_email FROM tbl_users WHERE user_name=:uname OR user_email=:umail");
			$st_meta->bindParam(':uname', $uname);
      $st_meta->bindParam(':umail', $umail);
      $st_meta->execute();
      $rows = $st_meta->fetch(PDO::FETCH_ASSOC);
			if($rows['user_name']==$uname) {
				$error[] = "sorry username already taken !";
			}else if($rows['user_email']==$umail) {
				$error[] = "sorry email id already taken !";
			}
			else
			{
				if(login::register($uname,$fname,$umail,$upass,$address,$phone_no)){
					login::redirect('sign-up.php?joined');
				}
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();echo "yahan";
		}
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>London TT - Sign Up</title>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet'>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <link rel="stylesheet" href="/bootstrap-4.0.0/css/bootstrap.css">
    <script src="/js/jquery/jquery-3.2.1.js"></script>
    <script src="/js/jquery/popper.js"></script>
    <script src="/bootstrap-4.0.0/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>
    <link rel="stylesheet" href="/css/style.css" type="text/css"/>
<style type="text/css">
    *{
 margin:0;
 padding: 0;   
}
</style>
</head>
<body>
<?php require_once "_viewControllers/commonview.php"; $commonview = new CommonView();$commonview->vRet('headvone'); ?>
<div class="signin-form text-center">

<div class="container">
<br>
        <br>
        <img src="/assets/images/logo.png">
        <!-- <h2 class="form-signin-heading">London Techntrade</h2> -->
        <br>
        <br>
        <br>
        <form method="post" class="form-signin">
            <h2 class="form-signin-heading">Sign up.</h2><hr />
            <?php
			if(isset($error))
			{
			 	foreach($error as $error)
			 	{
					 ?>
                     <div class="alert alert-danger">
                        <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error; ?>
                     </div>
                     <?php
				}
			}
			else if(isset($_GET['joined']))
			{
				 ?>
                 <div class="alert alert-info">
                      <i class="glyphicon glyphicon-log-in"></i> &nbsp; Successfully registered <a href='index.php'>login</a> here
                 </div>
                 <?php
			}
			?>
            <div class="form-group">
            <input type="text" class="form-control" name="txt_uname" placeholder="Enter Username" value="<?php if(isset($error)){echo $uname;}?>" />
            </div>
						<div class="form-group">
            <input type="text" class="form-control" name="txt_fullname" placeholder="Enter Full Name" value="<?php if(isset($error)){echo $fname;}?>" />
            </div>
            <div class="form-group">
            <input type="text" class="form-control" name="txt_umail" placeholder="Enter E-Mail ID" value="<?php if(isset($error)){echo $umail;}?>" />
            </div>
            <div class="form-group">
            	<input type="password" class="form-control" name="txt_upass" placeholder="Enter Password" />
            </div>
            <div class="form-group">
            	<input type="text" class="form-control" name="txt_address" placeholder="Enter Address | Valid Postcode" />
            </div>
            <div class="form-group">
            	<input type="number" class="form-control" name="txt_phone_no" placeholder="Enter Phone Number" />
            </div>
            <div class="clearfix"></div><hr />
            <div class="form-group">
            	<button type="submit" class="btn btn-primary" name="btn-signup">
                	<i class="glyphicon glyphicon-open-file"></i>&nbsp;SIGN UP
                </button>
            </div>
            <label>Already have account! <a href="/login" class="">Sign In</a></label>
        </form>
       </div>
</div>

</div>

</body>
</html>

