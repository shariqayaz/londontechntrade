<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'p3plcpnl0143.prod.phx3.secureserver.net';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'noreply@londontt.com';                 // SMTP username
    $mail->Password = 'Extream_ds_365';                           // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('kashan@londontt.com', 'Kashan London Techntrade');
    $mail->addAddress('goodsbitz@gmail.com', 'Goods Bitz');     // Add a recipient
    //$mail->addAddress('ellen@example.com');               // Name is optional
    $mail->addReplyTo('kashan@londontt.com', 'Kashan');
    //$mail->addCC('gr8shariq@gmail.com');
    $mail->addBCC('kashan@londontt.com');

    // //Attachments
    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Here is the subject';
    $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';
    header('location: /index');
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}

 ?>
 <!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="sell smartphone buy used phone recycle cell phone unlocking repair at London techntrade.">
    <meta name="robots" content="index, follow">
    <meta property="og:description" content="Repair stuff, sell your stuff">
    <meta property="og:title" content="Repair Your Stuff, Sell Your Stuff" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.londontt.com/" />
    <meta NAME="geo.region" content="london">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script> -->

    <!-- fonts -->
    <link href='https://fonts.googleapis.com/css?family=Anton' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet'>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <link rel="stylesheet" href="/bootstrap-4.0.0/css/bootstrap.css">
    <script src="/js/jquery/jquery-3.2.1.js"></script>
    <script src="/js/jquery/popper.js"></script>
    <script src="/bootstrap-4.0.0/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>
    <link rel="stylesheet" href="/css/style.css" type="text/css"/>

<title>LONDON TECHNTRADE CONTACT</title>
</head>
<body style="background:#f6f6f6">
  <?php require_once "_viewControllers/commonview.php"; $commonview = new CommonView();$commonview->vRet('headvone'); ?>
<br>

<br>
<?php require_once "_viewControllers/commonview.php"; $commonview = new CommonView();$commonview->vRet('footvone'); ?>

</body>
</html>
