<link rel="stylesheet" href="/bootstrap-4.0.0/css/bootstrap.css">
<script src="/js/jquery/jquery-3.2.1.js"></script>
<script src="/js/jquery/popper.js"></script>
<script src="/bootstrap-4.0.0/js/bootstrap.min.js"></script>
<script src="/js/script.js"></script>
<link rel="stylesheet" href="/css/style.css" type="text/css"/>

<title>LONDON TECHNTRADE</title>
</head>
<body style="background:#f6f6f6">
  <?php require_once "_viewControllers/commonview.php"; $commonview = new CommonView();$commonview->vRet('index'); ?>
<br>
<div style="text-align:center">
  <font style="color:red;text-align:center;font-family:'Baloo Tamma';font-size:25px">Hey, how may i help you?</font>
</div>
<div class="container">
  <ul class="nav nav-tabs" role="tablist" style="border:0px;">
    <li class="nav-item">
      <a class="nav-link active tabtitle" data-toggle="tab" href="#sell" role="tab">
        <font style="text-align:left;font-weight:bold;color:black">
          WANT TO SELL YOUR STUFF?
        </font>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link tabtitle" data-toggle="tab" href="#repair" role="tab">
        <font style="text-align:left;font-weight:bold;border:0px;color:black">
          WANT TO REPAIR YOUR STUFF?
        </font>
      </a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div class="tab-pane active" id="sell" role="tabpanel" style="border:0px">
      <div class="col" style="background:white;border-top:3px solid #1655a6;border-bottom:1px solid red;border-left:1px solid #1655a6;border-right:1px solid #1655a6;">
        <div style="text-align:center"><br>
          <font class="searchboxheading">
            <img src="/assets/img/gbpicon.png" alt="" style="width:30px;margin-top:-5px;"> SELL YOUR STUFF
          </font><br>
          <font class="searchboxstep"><font style="font-family: 'Baloo Tamma';font-size:16px;font-weight:bold;color:red">1.</font>SEARCH</font>
          <font class="hidden-sm-down" style="color:black;font-family:'Lato';font-size:12px;font-weight:lighter">Provide your product details. </font>
          <font class="searchboxstep"><font style="font-family: 'Baloo Tamma';font-size:16px;font-weight:bold;color:red">2.</font>SELL</font>
          <font class="hidden-sm-down" style="color:black;font-family:'Lato';font-size:12px;font-weight:lighter">Provide your details. </font>
          <font class="searchboxstep"><font style="font-family: 'Baloo Tamma';font-size:16px;font-weight:bold;color:red">3.</font>SEND</font>
          <font class="hidden-sm-down" style="color:black;font-family:'Lato';font-size:12px;font-weight:lighter;">Just courier us your product. </font><br>
          <div class="input-group" style="text-align:center;margin:0px">
            <input onkeyup="recyclesearchResult(this.value)" class="form-control" aria-describedby="basic-addon2" type="text" name="" value="" placeholder="IPHONE OR GALAXY" style="font-family:'Lato';font-weight:bold;font-size:15px;text-transform:uppercase;color:#2a2a2a;text-align:center;items-align:center;border-radius:0px;border-color:#1655a6;border: 2px solid #1655a6;background:white;color:rgb(255,69,0);">
              <span class="input-group-addon" style="width:100px;border-radius:1px;color:black;background:#1655a6">SEARCH</span>
            </div>
        </div>
        <div class="col" id="livesearchrecycle" style="PADDING-bottom:15px;">
        </div>
      </div>
    </div>
    <div class="tab-pane" id="repair" role="tabpanel">
      <div class="col" style="background:white;border-top:3px solid #1655a6;border-bottom:1px solid #1655a6;border-left:1px solid #1655a6;border-right:1px solid #1655a6;">
        <div style="text-align:center"><br>
          <font class="searchboxheading">
            REPAIR YOUR STUFF
          </font><br>
          <font class="searchboxstep"><font style="font-size:16px;font-weight:bold;color:red">1.</font>SEARCH</font>
          <font class="hidden-sm-down" style="color:black;font-family:'Lato';font-size:12px;font-weight:lighter">Provide your product details. </font>
          <font class="searchboxstep"><font style="font-size:16px;font-weight:bold;color:red">2.</font>DESCRIBE</font>
          <font class="hidden-sm-down" style="color:black;font-family:'Lato';font-size:12px;font-weight:lighter">Provide your details. </font>
          <font class="searchboxstep"><font style="font-size:16px;font-weight:bold;color:red">3.</font>SEND</font>
          <font class="hidden-sm-down" style="color:black;font-family:'Lato';font-size:12px;font-weight:lighter;">Just courier us your product. </font><br>
          <div class="input-group" style="text-align:center;margin:0px">
            <input onkeyup="repairsearchResult(this.value)" class="form-control" aria-describedby="basic-addon2" type="text" name="" value="" placeholder="IPHONE OR GALAXY" style="font-family:'Lato';font-weight:bold;font-size:15px;text-transform:uppercase;color:#2a2a2a;text-align:center;items-align:center;border-radius:0px;border-color:#1655a6;border: 2px solid #1655a6;background:white;color:rgb(255,69,0);">
              <span class="input-group-addon" style="width:100px;border-radius:1px;color:black;background:#1655a6">SEARCH</span>
            </div>
        </div>
        <div class="col" id="livesearchrepair" style="PADDING-bottom:15px;">
        </div>
      </div>
    </div>
  </div>
  </div>

  <div class="container" style="padding-top:12px;border:1px solid red;text-align:center;margin-top:20px;height:auto;width:99%;background:#1655a6">
    <font class="searchboxheading" style="margin:0px">
      Our most popular phones
    </font>
  </div>
  <div class="container-fluid" style="height:auto;width:100%;">
    <div class="row" style="text-align:center;margin-top:-1px">
      <div class="col" style="width:100%;background:white;"><br>
        SAMSUNG
      </div>
      <div class="col" style="width:100%;background:white;"><br>
        IPHONE
      </div>
      <div class="col" style="width:100%;background:white;"><br>
        Google
      </div>
      <div class="col" style="width:100%;background:white;"><br>
        Sony
      </div>
      <div class="col" style="width:100%;background:white;"><br>
        Microsoft
      </div>
      <div class="col" style="width:100%;background:white;"><br>
        HTC
      </div>
    </div>
    <div class="row" style="text-align:center;margin-top:-1px">
      <div class="col" style="width:100%;background:white;"><br>
        Nokia
      </div>
      <div class="col" style="width:100%;background:white;"><br>
        BlackBerry
      </div>
      <div class="col" style="width:100%;background:white;"><br>
        DELL
      </div>
      <div class="col" style="width:100%;background:white;"><br>
        HP
      </div>
      <div class="col" style="width:100%;background:white;"><br>
        IBM
      </div>
      <div class="col" style="width:100%;background:white;"><br>
        APPLE
      </div>
    </div>
    <br><br>
  </div>

  <div class="container" style="padding-top:12px;border:1px solid red;text-align:center;margin-top:20px;height:auto;width:90%;background:#1655a6">
    <font class="searchboxheading" style="margin:0px">
      WHY US?
    </font>
  </div>
  <div class="container" style="height:auto;width:90%;">
    <div class="row" style="margin-top:-1px;border:1px solid #1655a6;">
      <div class="col" style="width:90%;border:1px solid #1655a6; background:white;"><br>
        <div class="containerbtn" style="border-left:3px solid #b20000;opacity:1;color:black;margin: 0 auto;">
          <u><a href="/repair" style="color:black"><p style="font-weight:bold;margin-left:6px;text-align:left;text-transform:uppercase;">REPAIRING | UNLOCKING</p></a></u>
        </div>
        <div class="container4" style="border-left:2px solid #b20000;opacity:1;color:black;margin: 0 auto;">
          <p style="margin-left:5px;text-align:left;text-transform:uppercase;">We are repairing and unlocking almost all devices of most brands.</p>
        </div>
        <br>
        <div class="container4" style="border-left:2px solid #b20000;opacity:1;color:black;margin: 0 auto;">
          <p style="margin-left:5px;text-align:left;text-transform:uppercase;">Once we determine the issue, we’ll review your repair options with you but <strong>quickly.</strong></p>
        </div>
        <br>
        <div class="container4" style="border-left:2px solid #b20000;opacity:1;color:black;margin: 0 auto;">
          <p style="margin-left:5px;text-align:left;text-transform:uppercase;">We only use the best quality and genion parts so your phone will work exactly as you want.</p>
        </div>
        <br>
        <div class="container4" style="border-left:2px solid #b20000;opacity:1;color:black;margin: 0 auto;">
          <p style="margin-left:5px;text-align:left;text-transform:uppercase;">Free diagnostic and get free advice.<br>NO FIX NO FEE.</p>
        </div>
        <br>
        <div class="containerbtn" style="background:#b20000;opacity:1;color:black;margin: 0 auto;">
          <a href="/repair" style="font-weight:bold;color:black;;margin-left:5px;text-align:center;text-transform:uppercase">-> VISIT REPAIRING / UNLOCKING Application</a>
        </div><br>
      </div>
      <div class="col" style="width:90%;border:1px solid #1655a6; background:white;"><br>
        <div class="containerbtn" style="border-left:3px solid #4D9C58;opacity:1;color:black;margin: 0 auto;">
          <u><a href="/recycle" style="color:black"><p style="font-weight:bold;margin-left:5px;text-align:left;text-transform:uppercase;">RECYCLE | RE-SALE</p></a></u>
        </div>
        <div class="container4" style="border-left:2px solid #4D9C58;opacity:1;color:black;margin: 0 auto;">
          <p style="margin-left:5px;text-align:left;text-transform:uppercase;">WE ACCEPT DAMAGED PHONES & DEVICES.</p>
        </div>
        <br>
        <div class="container4" style="border-left:2px solid #4D9C58;opacity:1;color:black;margin: 0 auto;">
          <p style="margin-left:5px;text-align:left;text-transform:uppercase;">WE PAY MOST CASH of your. Fast Payment.</p>
        </div>
        <br>
        <div class="container4" style="border-left:2px solid #4D9C58;opacity:1;color:black;margin: 0 auto;">
          <p style="margin-left:5px;text-align:left;text-transform:uppercase;">SAME DAY PAYMENT.</p>
        </div>
        <br>
        <div class="container4" style="border-left:2px solid #4D9C58;opacity:1;color:black;margin: 0 auto;">
          <p style="margin-left:5px;text-align:left;text-transform:uppercase;">FREE POSTAGE.</p>
        </div>
        <br>
        <div class="containerbtn" style="background: #4D9C58;opacity:1;color:black;margin: 0 auto;">
          <a href="/recycle" style="font-weight:bold;color:darkblue;margin-left:5px;text-align:center;text-transform:uppercase;">-> Sell my phone</a>
        </div><br>
      </div>
      <div class="col" style="width:90%;border:1px solid #1655a6; background:white;"><br>
        <div class="containerbtn" style="border-left:3px solid skyblue;opacity:1;color:black;margin: 0 auto;">
          <u><a href="/shop" style="color:black"><p style="font-weight:bold;margin-left:5px;text-align:left;text-transform:uppercase;">SHOP</p></a></u>
        </div>
        <div class="container4" style="border-left:2px solid skyblue;opacity:1;color:black;margin: 0 auto;">
          <p style="margin-left:5px;text-align:left;text-transform:uppercase;">GOOD AS NEW AND NO CONTRACT</p>
        </div>
        <br>
        <div class="container4" style="border-left:2px solid skyblue;opacity:1;color:black;margin: 0 auto;">
          <p style="margin-left:5px;text-align:left;text-transform:uppercase;">FREE DELIVERY.</p>
        </div>
        <br>
        <div class="container4" style="border-left:2px solid skyblue;opacity:1;color:black;margin: 0 auto;">
          <p style="margin-left:5px;text-align:left;text-transform:uppercase;">One Year Warranty</p>
        </div>
        <br>
        <div class="container4" style="border-left:2px solid skyblue;opacity:1;color:black;margin: 0 auto;">
          <p style="margin-left:5px;text-align:left;text-transform:uppercase;">We include screen protector for free.</p>
        </div>
        <br>
        <div class="containerbtn" style="background:skyblue;opacity:1;color:black;margin: 0 auto;">
          <a href="/shop" style="font-weight:bold;color:black;margin-left:5px;text-align:center;text-transform:uppercase;">-> Visit Shop</a>
        </div><br>
      </div>
    </div>
  </div>

  <div class="container" style="padding-top:12px;border:1px solid red;text-align:center;margin-top:20px;height:auto;width:99%;background:#1655a6">
    <font class="searchboxheading" style="margin:0px">
      What we are Repairing and Unlocking?
    </font>
  </div>

<div id="repair" class="container-fluid" style="background:white;border-bottom:1px solid silver;width:99%;height:auto;color:black;text-align:center">
  <br>
  <div class="row" style="text-align:left">
    <div class="col details" style="font-family:'Cabin';font-weight:bold;color:black;margin:10px;border-left:2px solid #b20000">
      Almost All models of all brands we are repairing and unlocking.
    </div>
  </div>
  <div class="row" style="background:transparent;text-align:left">
    <div class="col details" style="font-family:'Cabin';font-weight:bold;color:black;margin:10px;border-left:2px solid #b20000">
       Including APPLE IPhones, APPLE MAC, APPLE IPOD and APPLE IPAD and SAMSUNG GALAXY Smartphones and tablets or GALAXY TABS, Amazon, Google, Nokia, Microsoft, HTC, Huawei, SONY, LG, BlackBerry etc...
    </div>
  </div>
  <div class="row" style="background:transparent;text-align:left">
    <div class="col details" style="font-family:'Cabin';font-weight:bold;color:black;margin:10px;border-left:2px solid #b20000">
       You can contact us for almost any issue like
       <u>Broken or Damaged Hardware replacement</u>,
       <u>LCD / Screen replacement</u>
       of laptop or cellphones.
       You can also contact us for software issue's.
       We are fixing your slow PC problem and virus issue's,
       Operating System installations, Flashing, Firmware upgrading, Unlocking, software installation issues etc...
    </div>
  </div>
  <div class="row" style="background:transparent;text-align:left">
    <div class="col details" style="font-family:'Cabin';font-weight:bold;color:black;margin:10px;border-left:2px solid #b20000">
       Infact you can call or text us for any issue with you electronic device.<br><br>
       <font class="topslogan">
         <img src="/assets/img/emailicon.png" alt="" style="width:13px;height:13px;margin-top:-2px;"><a style="color:black;" href="mailto:kashan@londontt.com?subject=feedback"> kashan@londontt.com</a>
       </font><br>
       <font class="topslogan">
         <img src="/assets/img/callicon.png" alt="" style="width:13px;height:13px;margin-top:-2px;"> 07401824142
       </font><br>
       <font class="topslogan">
         <img src="/assets/img/wapp.png" alt="" style="width:15px;height:15px;margin-top:-2px;">"<?php require_once '_controllers/occotoadmin.php';OccotoAdmin::info('whatsapp'); ?>"
       </font>
       </a>
    </div>
  </div>

</div>

<div class="container" style="padding-top:12px;border:1px solid red;text-align:center;margin-top:20px;height:auto;width:99%;background:#1655a6">
  <font class="searchboxheading" style="margin:0px">
    Quck Appointment?
  </font>
</div>
<div class="container" style="padding-top:12px;border:1px solid red;text-align:center;margin-top:0px;height:auto;width:99%;background:white">
  <div class="row">
    <div class="col">
      <form action="/form/appointment/submit/" method="post">
        <div class="container">
            <div class="col">
              <br style="clear:both">
              <div class="form-group">
                <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Name" required>
                <input type="hidden" class="form-control" id="url" name="url" value="<?php echo $requeststr; ?>" required>
              </div>
              <div class="form-group">
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" required>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="brand" name="brand" placeholder="Brand: Apple or Samsung" required>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="model" name="model" placeholder="Model: IPhone6 OR Galaxy S7" required>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="message" name="message" placeholder="Describe your details" maxlength="140">
              </div>
              <div class="container" style="text-align:center;align:center">
              <input style="border:1px solid red;border-radius:1px;color:red;background:white;font-size:18px;width:270px;height:50px" type="submit" id="quickappsubmit" name="quickappsubmit" class="btn btn-primary pull-center" value="Submit Appointment" />
            </div><br>
            </div>
          </div>
      </form>
    </div>
  </div>
</div>
<br>
<div id="repair" class="container" style="border-bottom:1px solid gray;height:auto;color:black;text-align:center;background:#1655a6;">
  <br>
  <font class="searchboxheading" style="margin:0px">
    what we are buying
  </font>
  <div class="row" style="background:transparent;text-align:left">
    <div class="col details" style="font-family:'Lato';font-weight:bold;color:white;margin:10px;border-left:2px solid white">
      We are actively buying Smartphones Laptop MAC IPod IPad and PC's and you may also contact us for other than these. Just Send Message to our WhatsApp or cellphones <br>
      <font class="topslogan">
        <img src="/assets/img/emailicon.png" alt="" style="width:13px;height:13px;margin-top:-2px;"><a style="color:black;" href="mailto:kashan@londontt.com?subject=feedback"> kashan@londontt.com</a>
      </font><br>
      <font class="topslogan">
        <img src="/assets/img/callicon.png" alt="" style="width:13px;height:13px;margin-top:-2px;"><font style="color:black;">07401824142</font>
      </font><br>
      <font class="topslogan">
        <img src="/assets/img/wapp.png" alt="" style="width:15px;height:15px;margin-top:-2px;">"<?php require_once '_controllers/occotoadmin.php';OccotoAdmin::info('whatsapp'); ?>"
      </font>
    </div>
  </div>
  <div class="row" style="background:transparent;text-align:left">
    <div class="col details" style="font-family:'Lato';font-weight:bold;color:white;margin:10px;border-left:2px solid white">
      In Smartphones we are actively buying APPLE products like IPHONE's and SAMSUNG Galaxy S Series devices (2015 or Later) and other gadgets.
    </div>
  </div>
  <div class="row" style="background:transparent;text-align:left">
    <div class="col details" style="font-family:'Lato';font-weight:bold;color:white;margin:10px;border-left:2px solid white">
      We are buying Laptops of almost all brands including DELL, HP, SONY, LENEVO, Thinkpad, IDEAPAD, etc...
    </div>
  </div><br>
  <div class="col" style="height:auto;">
    <div class="containerbtn" style="background: #4D9C58;opacity:1;color:white;margin: 0 auto;">
      <a href="/" style="font-weight:bold;color:white;margin-left:5px;text-align:center;text-transform:uppercase;">Visit Recycle | Re-Sale</a>
    </div><br>
</div>
</div>

<div class="container-fluid" style="border-bottom:1px solid gray;height:auto;color:black;text-align:center;background:url(/lt.png);background-attachment: fixed;">
  <br>
  <font class="headingtitle" style="text-align:center;font-weight:bold;margin-bottom:0px;margin-top:10px;padding-top:10px;padding-bottom:10px;padding-left:15px;padding-right:15px;color:darkblue;">
    ALL SOLUTIONS AT ONE PLACE MAKES SENCE.
  </font>
  <br>
  <br>
  <div class="row" style="background:transparent;text-align:left">
    <div class="col details" style="font-family:'Lato';font-weight:bold;color:dimgray;margin:10px;border-left:20px solid #b20000">
      Don’t worry – if you’re experiencing any problem with your any device? We are ready to repair or fix it or we are happy to buy it as it is. Yes its true just call us. Our Technician or Buying deparment representative is ready to assist you.
    </div>
  </div>
  <div class="row" style="background:transparent;text-align:left">
    <div class="col details" style="font-family:'Lato';font-weight:bold;color:dimgray;margin:10px;border-left:20px solid #4D9C58">
      At LONDON TECHNTRADE selling your phones and laptop devices made easy and effortlessly.
      You can sell your phones to us from anywhere in UK. You just need to fill the form and send your device by Royel Mail at below given address. If your device is network locked or out of order and you don't want to fix it, we are happy accept it and pay you as much as it worth.
      Once we received your device and our assestment is done, we will ask your payment options would you like to receive your cash and we'll pay you same day. That's it.<br><a href="/recycle">Visit Re-Sale | Recycle Application</a>
    </div>
  </div>
  <div class="row" style="background:transparent;text-align:left">
    <div class="col details" style="font-family:'Lato';font-weight:bold;color:dimgray;margin:10px;border-left:20px solid skyblue">We are selling Brand New and Used Smartphones, Tablets, MAC, Laptop, other electronics gadget and accessories. Its means you also have option to sell your old phone and buy new or other one.</div>
  </div>
  </div>
  <div class="container-fluid" style="border-bottom:1px solid gray;height:auto;color:black;text-align:center;background:url(/sveVis.png)">
    <br>
    <font class="headingtitle" style="text-align:center;font-weight:bold;margin-bottom:0px;margin-top:10px;padding-top:10px;padding-bottom:10px;padding-left:15px;padding-right:15px;color:darkblue;">
      HEARED ENOUGH ?
    </font>
    <br>
    <br>
    <div class="row">
      <div class="col" style="height:auto;">
        <div class="containerbtn" style="background:#b20000;opacity:1;color:black;margin: 0 auto;">
          <a href="/" style="font-weight:bold;color:black;;margin-left:5px;text-align:center;text-transform:uppercase">-> VISIT REPAIRING / UNLOCKING Application</a>
        </div><br>
      </div>
      <div class="col" style="height:auto;">
        <div class="containerbtn" style="background: #4D9C58;opacity:1;color:black;margin: 0 auto;">
          <a href="/" style="font-weight:bold;color:darkblue;margin-left:5px;text-align:center;text-transform:uppercase;">-> Sell my phone</a>
        </div><br>
      </div>
      <div class="col" style="height:auto;">
        <div class="containerbtn" style="background:skyblue;opacity:1;color:black;margin: 0 auto;">
          <a href="/" style="font-weight:bold;color:black;margin-left:5px;text-align:center;text-transform:uppercase;">-> Visit Shop</a>
        </div><br>
      </div>
    </div>
    <br>
    <div class="container-fluid">
      <div class="row">
        <div class="col" style="text-align:right">
          <font style="margin-bottom:0px;font-size: 18px;font-weight:bold;color:darkblue">
            CALL US:
          </font>
        </div>
        <div class="col" style="text-align:left">
          <font style="font-family:'Lato';margin-top:-10px;font-size: 18px;font-weight:bold;color:black;">
          020 8691 7978
          </font>
        </div>
      </div><br>
      <div class="row">
        <div class="col" style="text-align:right">
          <font style="margin-bottom:-10px;font-size: 18px;font-weight:bold;color:darkblue">
            WhatsApp Support:
          </font>
        </div>
        <div class="col" style="text-align:left">
          <a href="https://api.whatsapp.com/send?phone=447384131222">
            <font style="font-family:'Lato';margin-top:-10px;font-size: 18px;font-weight:bold;color:black;">
              +447384131222
            </font>
            <img src="/assets/img/wapp.png" width="40" height="40" alt=""></a>
        </div>
      </div><br>
      <div class="row">
        <div class="col" style="text-align:center">
          <font style="margin-bottom:-10px;font-size: 18px;font-weight:bold;color:darkblue">
            EMAIL:
          </font>
        </div>
        </div>
        <div class="row">
        <div class="col" style="text-align:center">
            <font style="font-family:'Lato';margin-top:-10px;font-size: 18px;font-weight:bold;color:black;">
              <a style="color:black;" href="mailto:kashan@londontt.com?subject=feedback">kashan@londontt.com</a>
            </font>
        </div>
      </div><br>
      <div class="row">
        <div class="col" style="text-align:right">
          <font style="margin-bottom:-10px;font-size: 18px;font-weight:bold;color:darkblue">
            ADDRESS:
          </font>
        </div>
        <div class="col" style="text-align:left">
          <font style="font-family:'Lato';margin-top:-10px;font-size: 18px;font-weight:bold;color:black;">
            London Techntrade, 113 Loampit Vale, London SE13 7TG, UK
          </font>
        </div>
      </div><br>
      <div class="row">
        <div class="col" style="text-align:right">
          <font style="margin-bottom:-10px;font-size: 18px;font-weight:bold;color:darkblue">
            SHOP TIMING:
          </font>
        </div>
        <div class="col" style="text-align:left">
          <font style="font-family:'Lato';margin-top:-10px;font-size: 18px;font-weight:bold;color:black;">
            9AM - 10PM 7 days in week
          </font>
        </div>
      </div>


    </div>
<br><hr>
<font style="text-align:center;font-weight:bold;margin-bottom:0px;margin-top:10px;padding-top:10px;padding-bottom:10px;padding-left:15px;padding-right:15px;color:black;font-size:18px">
OUR LOCATION
</font>
<div class="row">
<div class="col" style="width:50%;">
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2485.6048981942176!2d-0.02087084882033078!3d51.465410221493734!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487602668bfefffd%3A0xfdd8e59ecdc439c1!2sLondon+TechnTrade!5e0!3m2!1sen!2s!4v1516400795430" width=100% height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
</div>
</div>
<footer class="container-fluid fixed-bottom-footer">
  <div class="row">
    <div class="col">
      <font class="brandlogo" style="font-weight:bold;margin-right:0px;padding-left:5px;padding-right:5px;color:darkblue">
        LONDON
      </font>
      <font class="brandlogo" style="font-weight:bold;margin-right:0px;color:black;background:darkblue">
        TECHNTRADE
      </font><br><br>
      <font style="font-family:'Lato';font-size:15px;font-weight:bold;color:black;margin:0px;padding-bottom:0px">
        Copyright © 2018 London Techntrade | All Rights Reserved.
      </font>
      <font class="brandlogo" style="font-size: 15px;font-weight:bold;color:black;background:darkblue">
        POWERED BY OCCOTO
      </font><br>
    </div>
  </div>
</footer>
</body>
</html>
