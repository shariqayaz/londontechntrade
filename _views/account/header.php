<?php
require_once '_controllers/login.php';
$login = new login();
$bool = false;
if($login->is_loggedin()!="")
{
  $user_id = $_SESSION['user_session'];
	$stmt = $login->runQuery("SELECT * FROM tbl_users WHERE user_id=:user_id");
	$stmt->execute(array(":user_id"=>$user_id));
	$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
  if ($userRow['isAdmin']!='1') {
    header('location: /recycle/');
  }
  $bool = true;
}
else {
  header('location: /login/');
}
$sitename = "London TechnTrade";
 ?>

  <!DOCTYPE html>
  <html lang="en">
  <head>
    <title><?php echo $sitename; ?> - Main Admin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/style.css">
        <script data-require="jquery" data-semver="2.0.3" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <style media="screen">
    .nav-tabs{
background-color:#161616;
}
.tab-content{
  background-color:#88BDBC;
  height: 100%
}
.nav-tabs > li > a{
border: none;
margin-bottom:1px;
background: #254E58;
color: white;
}
.nav-tabs > li > a:hover{
  background-color: lightgray !important;
  margin-bottom: 0px;
  border: none;
  border-radius: 0;
  color: white;
}
    </style>
    <script type="text/javascript">
    function showUser(str)
    {
      if (str=="General Information")
      {
        if (str == "") {
            document.getElementById(str).innerHTML = "";
            return;
        } else {
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById('apple').innerHTML = this.responseText;
                }
            };
            xmlhttp.open("GET","/occoto-admin/"+str+"",true);
            xmlhttp.send();
        }
      }
      else if (str=="Product Management")
      {
        if (str == "") {
            document.getElementById(str).innerHTML = "";
            return;
        } else {
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById('apple').innerHTML = this.responseText;
                }
            };
            xmlhttp.open("GET","/occoto-admin/"+str+"",true);
            xmlhttp.send();
        }
      }

    }
    //
    function showprod(str) {
        if (str == "") {
            document.getElementById(str).innerHTML = "";
            return;
        } else {
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById('prod').innerHTML = this.responseText;
                }
            };
            xmlhttp.open("GET","/occoto-admin/tabload/"+str+"",true);
            xmlhttp.send();
        }
    }
    </script>
  </head>
  <body style="">
  <div class="top-admin" style="background: blue;">
<div class="container">

    <!-- Loged in -->
    <?php if ($bool == true && $userRow['isAdmin']==true) { ?>
      <div class="toparea" style="direction: ltr;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',sans-serif;text-transform:uppercase;color:white;">
        <div class="col">
          <h6 style="text-align:left;float: left;"><a href="/occoto-admin/" style="font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',sans-serif;color:white;"><?php echo $sitename; ?> |  Admin Panel</a>
          </h6>
          <h6 style="float: right">
            <font color="white" >Login: <strong><?php echo $userRow['user_name']; ?></font> | <a href='/logout'><font color=white>logout <span class="glyphicon glyphicon-log-out"></span></font></a></strong>
          </h6>
        </div>
     </div>
  <?php }else if ($bool == true && $userRow['isAdmin']==false) { ?>
      <div class="toparea" style="direction: ltr;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',sans-serif;text-transform:uppercase;color:black;">
        <div class="col">
          <h5 style="text-align:right"><font color=green>Login: <strong><?php echo $userRow['user_name']; ?></font> | <a href='/logout'><font color=red>logout </font><span class="glyphicon glyphicon-log-out"></span></a></strong></h5>
        </div>
     </div>
  <?php }else if ($bool == false){?>
  <div class="toparea" style="direction: ltr;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',sans-serif;padding:2px;margin-top:0px;color:black;margin-bottom:0px;text-transform:uppercase;">
     <div class="container">
       <h6 style="text-align:right"><a href='/login'><font color=black>Login</a> | <a href='/sign-up' style="color:black">Sign Up</font></a></strong></h6>
     </div>
     
     <?php } ?>

</div>
</div>
