<?php

require_once '_controllers/login.php';
$login = new login();
$bool = false;
$user_id = "0";
if(login::is_loggedin()!="")
{
  $user_id = $_SESSION['user_session'];
	$stmt = $login->runQuery("SELECT * FROM tbl_users WHERE user_id=:user_id");
	$stmt->execute(array(":user_id"=>$user_id));
	$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
  $bool = true;
}
else
{
  $_SESSION['user_session'] = "";
  $bool = false;
  $user_id = "0";
}
//unset($_SESSION['sell_cart']);

$con=mysqli_connect("localhost",DB_ACCESS::meta_user(),DB_ACCESS::meta_pass(),"irecycle2020");
// Check connection
//echo $get_requeststr;
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

   $sql="SELECT * FROM products where modelname='$get_requeststr' AND isActive_recycle='1'";
   $result=mysqli_query($con,$sql);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="description" content="sell <?php echo $get_requeststr; ?> recycle <?php echo $get_requeststr; ?> unlocking repair. At Londontt.">
    <meta NAME="geo.region" content="london">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script> -->
    <!-- fonts -->
    <link href='https://fonts.googleapis.com/css?family=Anton' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet'>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="/bootstrap-4.0.0/css/bootstrap.css">
    <script src="/js/jquery/jquery-3.2.1.js"></script>
    <script src="/js/jquery/popper.js"></script>
    <script src="/bootstrap-4.0.0/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>
    <link rel="stylesheet" href="/css/style.css" type="text/css"/>
    <link rel="stylesheet" href="/css/recyclephone.css" type="text/css"/>
<title>LONDON TECHNTRADE</title>
</head>
<body style="background:">
  <?php require_once "_viewControllers/commonview.php"; $commonview = new CommonView();$commonview->vRet('headvone'); ?>
  <form action="<?php echo URL_ROOT; ?>phones/cartadd/add" method="post">
  <input type="hidden" name="model" value="<?php echo $get_requeststr; ?>">
  <div class="model-now">
    <div class="container" style="">
      <br>
      <div class="row">

      <div class="col-xl-12 text-center">
      <?php
      // Associative array
      if ($row = $result->fetch_assoc()) {
      ?>
      <input id="prodid" type="hidden" name="prodid" value="<?php echo $row['product_id']; ?>">
      <img class='img-rounded text-center' src="<?php echo $row["image_one"]; ?>" alt="Sell My <?php echo $row["modelname"]; ?>" style='width:200px;height:auto'>

      <div style="text-align:center;margin:8px 0 0 0">
      <font class="" style="font-size:25px;text-align:center;color:#1655a6;margin:0px;text-transform:uppercase"><strong><?php echo $get_requeststr; ?></strong></font><br>
      </div>
      <h5 style="padding:10px;margin-top:0px;text-transform:;text-align:center;font-size: 13px;">
      <span>Select Your <span style="color: #1655a6;text-transform:uppercase"><b><?php echo $get_requeststr; ?></b></span> Condition, Capacity, Color & Carrier <b style="font-weight: 800;"> to assess the price</b>.</span>
      </h5>
      </div>
      </div>
    </div>
  </div>
  <div class="details-now">
      <div class="container">
        <br>
      <div class="row">

<div class="col-md-12 text-center" style="background: white;color: #1655a6;margin: 0 auto;border-radius: 5px;">

<?php
//#FETCHING THE NETWORKS
require_once '_controllers/phones.php';
$phnet = new phones();

$obj = new StdClass;
$obj = $phnet->get_networks($row['product_id']);

foreach ($obj as $intIndex => $objRecord) {
  echo "<label>
  <input onchange='getCheckedValue()' type='radio' name='networkradio' class='networkedbutton' value=".$objRecord->network_id." required>
  <img id=".$objRecord->network_id." class='networkedButSelected' src=".$objRecord->network_img.">
  </label>";
}
 ?>
<!-- <div class="clearfix"></div> -->
</div>
<div class="col-md-3">
      <font class="display-8 " style="color:white;font-weight:800;text-transform:uppercase">
        <h4 style="margin-top:10px;">Capacity</h4>
      </font>
<?php

require_once '_controllers/phones.php';
$phcap = new phones();
$obj = new StdClass;
$obj = $phcap->get_capacity($row['product_id']);
foreach ($obj as $intIndex => $objRecord) {
  echo "<div class='funkyradio'>
             <div class='funkyradio-primary'>
               <input onchange='getCheckedValue()' style='margin-bottom:0px' type='radio' name='capradio' id='".$objRecord->capacity."' value='".$objRecord->specific_product_id."' required>
               <label style='margin-top:0px'  for='".$objRecord->capacity."'>".$objRecord->capacity." GB</label>
             </div>
          </div>";
        }
?>

</div>

<div class="col-md-6">
<!-- ------------------------------------------------------ -->
<font class="display-8" style="color:white;font-weight:800;text-transform:uppercase">
  <h4 style="margin-top:10px;">Condition</h4>
</font>
<div class='funkyradio'>

  <?php

  require_once '_controllers/phones.php';
  $phcap = new phones();
  $obj = new StdClass;
  $obj = $phcap->get_condition($row['product_id']);
  foreach ($obj as $intIndex => $objRecord) {
    echo "<div class='funkyradio-primary'>
      <input onchange='getCheckedValue(); getProbs();' style='margin-bottom:0px' type='radio' name='condradio' id='".$objRecord->condition_name."' value='".$objRecord->condition_id."' required>
      <label style='margin-top:0px' for='".$objRecord->condition_name."'>".$objRecord->conditions."</label>
    </div>";
  }

?>

<div id="condition_desc" style="margin: 10px;font-size: 13px;color: white;padding: 1px;border:1px solid blue;text-align:center;">

</div>

</div>
<!-- ------------------------------------------------------ -->
</div>

<div class="col-xl-2">
  <?php

  require_once '_controllers/phones.php';
  $phcol = new phones();
  //var_dump($phcol->get_colors($row['product_id']));
  $obj = new StdClass;
  $obj = $phcol->get_colors($row['product_id']);
  ?>
  <font class="" style="color:white;text-transform:uppercase"><h4 style="margin-top:10px;">Color</h4></font><br>
    <?php
  foreach ($obj as $intIndex => $objRecord) {
    echo "<style media='screen'>
    .".$objRecord->color_name." .clrbutton span {
      background: ".$objRecord->color_code.";
    }
    .".$objRecord->color_name." input:checked ~ .clrbutton > span {
      background: ".$objRecord->color_code.";
      padding: 25px;
      box-shadow: 0px 0px 5px 3px rgba(0.1, 0.1, 0.1, 0.5);
    }
    </style>";
    echo "<label class=".$objRecord->color_name."><input type='radio' name='color' value=".$objRecord->color_id." required><div class='clrlayer'></div><div class='clrbutton'><span style='font-size:12px;text-align:center;color:black;font-weight:800;text-shadow:0px 0px 2px white;'>".$objRecord->color_name."</span></div></label>";
}
    ?>
  <?php
  }
  else
  {
  echo "<script>window.history.back()</script>";
  }
  ?>
</div>

    </div>
    </div>

</div>
  </div>
<br>
<div id="price"  style="text-align:center;">
  Price:
</div>

<div class="container" style="text-align:center;align:center">
  <input type="submit" id="submit" name="addProd" class="btn btn-primary pull-center" value="Add To Cart" style="background:;font-size:18px;width:270px;height:50px;font-weight: 500 !important;font-family: lato;" />
  <br><br>
  <input type="submit" id="submit" name="submit" class="btn btn-primary pull-center" value="Proceed & Checkout" style="background:;font-size:18px;width:270px;height:50px;font-weight: 500 !important;font-family: lato;" />
</div>
</form>
<br>
  <?php require_once "_viewControllers/commonview.php"; $commonview = new CommonView();$commonview->vRet('footvone'); ?>
</body>
</html>
