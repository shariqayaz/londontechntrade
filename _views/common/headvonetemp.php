<?php
require_once '_controllers/login.php';
$login = new login();
$bool = false;
if($login->is_loggedin()!="")
{
  //echo $login->is_loggedin();exit; // will show user id
  $user_id = $_SESSION['user_session'];
	$stmt = $login->runQuery("SELECT * FROM tbl_users WHERE user_id=:user_id");
	$stmt->execute(array(":user_id"=>$user_id));
	$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
  $bool = true;
}
else $bool = false;

 ?>
 <div class="navbar" style="margin:0px;background:#1655a6;border-bottom:2px solid white;">
     <div class="col-md-12" style="font-family:'Lato';text-align:left;font-size:12px;color:#1655a6">
      
        
       
        
    </div>
    <div class="col-md-12" style="font-family:'Lato';text-align:right;font-size:12px;color:black">
        <a href="/finished" style="float:left;color:#191919"> <strong>(<?php echo count($_SESSION['sell_cart']); ?>)</strong> Cart & Checkout</a>
      <?php if ($bool == true) { ?>
        Logged In: <strong><?php echo $userRow['user_name']; ?> | <a href="/logout" style="color:#191919">Logout</a></strong>
          <?php }else if ($bool == false){?>
        <strong><a href="/login" style="color:#191919">Login</a></strong>
            <?php } ?>
    </div>
</div>
<nav class="nav navbar" style="margin:0px;background:#1655a6;">
  <div class="container" style="">
    <div class="row" style="margin-bottom:10px;margin-top:10px">
      <div class="col-md-9 col-sm-12">
        <font class="brandlogo" style="font-weight:bold;margin-right:-5px;padding-left:0px;padding-right:5px;color:black">
          LONDON
        </font>
        <font class="brandlogo" style="font-weight:bold;margin-right:0px;color:#1655a6;background:black;">
          TECHNTRADE
        </font><br>
        <font class="sticky-top topslogan" style="font-family:'Baloo Tamma';color:black;font-weight:lighter">
          BUYING & SELLING | REPAIRING & UNLOCKING
        </font>
      </div>
      <div class="col-md-3 col-sm-12" style="margin:0px">
          <font class="topslogan">
            <img src="/assets/img/emailicon.png" alt="" style="width:13px;height:13px;margin-top:-2px;"> <?php require_once '_controllers/occotoadmin.php';OccotoAdmin::info('email'); ?>
          </font><br>
          <font class="topslogan">
            <img src="/assets/img/callicon.png" alt="" style="width:13px;height:13px;margin-top:-2px;"> <?php require_once '_controllers/occotoadmin.php';OccotoAdmin::info('phone'); ?>
          </font><br>
          <font class="topslogan">
            <img src="/assets/img/wapp.png" alt="" style="width:15px;height:15px;margin-top:-2px;"><?php require_once '_controllers/occotoadmin.php';OccotoAdmin::info('whatsapp'); ?></a>
          </font>
      </div>
    </div>
</nav>
<nav class="sticky-top" style="margin:0px">
<div class="row" style="margin:0px;">
  <div class="col sticky-top navbtn" style="padding:10px;background:#fff;border:1px solid #1655a6;border-bottom:2px solid red;border-top:0px solid red;border-left:2;color:rgb(266,69,0);font-weight: bold"><a href="/repair" style="color:#1655a6;"><span class="glyphicon glyphicon-wrench"></span></a></div>
  <div class="col sticky-top navbtn" style="padding:10px;background:#fff;border:1px solid #1655a6;border-bottom:2px solid red;border-top:0px solid red;border-left:2;color:rgb(266,69,0);font-weight: bold"><a href="/recycle" style="color:#1655a6;"><span class="glyphicon glyphicon-trash"></span></a></div>
  <div class="col sticky-top navbtn" style="padding:10px;background:#fff;border:1px solid #1655a6;border-bottom:2px solid red;border-top:0px solid red;border-left:2;color:rgb(266,69,0);font-weight: bold"><a href="/shop" style="color:#1655a6;"><span class="glyphicon glyphicon-briefcase"></span></a></div>
  <div class="col sticky-top navbtn" style="padding:10px;background:#fff;border:1px solid #1655a6;border-bottom:2px solid red;border-top:0px solid red;border-left:2;color:rgb(266,69,0);font-weight: bold"><a href="/index" style="color:#1655a6;"><span class="glyphicon glyphicon-user"></span></a></div>
</div>
</div>
</nav>
