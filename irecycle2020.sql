-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 09, 2020 at 06:02 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `irecycle2020`
--

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE `color` (
  `color_id` int(11) NOT NULL,
  `color_name` varchar(25) NOT NULL DEFAULT '-',
  `color_code` varchar(50) NOT NULL DEFAULT '-',
  `demand_factor_rate` decimal(4,2) DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`color_id`, `color_name`, `color_code`, `demand_factor_rate`) VALUES
(1, 'Black', 'black', '0.00'),
(2, 'White', 'white', '0.00'),
(3, 'Silver', 'lightgray', '0.00'),
(4, 'Spacegray', 'gray', '0.00'),
(5, 'Gold', '#DAA520', '0.00'),
(6, 'Rosegold', '#b76e79', '0.00'),
(7, 'Jetblack', '#36373a', '0.00'),
(8, 'Skyblue', 'skyblue', '0.00'),
(9, 'Yellow', 'yellow', '0.00'),
(10, 'Pink', 'pink', '0.00'),
(11, 'Purple', '#D1CDDA', '0.00'),
(12, 'Green', '#AEE1CD', '0.00'),
(13, 'Red', '#BA0C2E', '0.00'),
(15, 'MIDNIGHT GREEN', '#4E5851', '0.00'),
(16, 'Jet Black', '#1F2020', '0.00'),
(17, 'Blue', '#5EB0E5', '0.00'),
(18, 'Coral', '#EE7762', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `color_productbind`
--

CREATE TABLE `color_productbind` (
  `color_bindid` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `color_productbind`
--

INSERT INTO `color_productbind` (`color_bindid`, `product_id`, `color_id`) VALUES
(1, 346, 11),
(2, 346, 9),
(3, 346, 12),
(4, 346, 1),
(5, 346, 2),
(6, 346, 13),
(7, 347, 15),
(8, 347, 4),
(9, 347, 3),
(10, 347, 5),
(11, 348, 15),
(12, 348, 4),
(13, 348, 3),
(14, 348, 5),
(15, 352, 5),
(16, 352, 3),
(17, 352, 4),
(18, 353, 5),
(19, 353, 3),
(20, 353, 4),
(21, 335, 5),
(22, 335, 6),
(23, 335, 3),
(24, 335, 4),
(25, 336, 5),
(26, 336, 6),
(27, 336, 3),
(28, 336, 4),
(29, 338, 6),
(30, 338, 5),
(31, 338, 3),
(32, 338, 1),
(33, 338, 16),
(34, 338, 13),
(35, 339, 6),
(36, 339, 5),
(37, 339, 3),
(38, 339, 1),
(39, 339, 16),
(40, 339, 13),
(41, 340, 5),
(42, 340, 13),
(43, 340, 3),
(44, 340, 4),
(45, 341, 5),
(46, 341, 13),
(47, 341, 3),
(48, 341, 4),
(49, 344, 5),
(50, 344, 4),
(51, 344, 3),
(52, 345, 5),
(53, 345, 4),
(54, 345, 3),
(55, 337, 1),
(56, 337, 2),
(57, 337, 13),
(58, 342, 3),
(59, 342, 4),
(60, 343, 13),
(61, 343, 9),
(62, 343, 2),
(63, 343, 18),
(64, 343, 1),
(65, 343, 17);

-- --------------------------------------------------------

--
-- Table structure for table `conditions`
--

CREATE TABLE `conditions` (
  `condition_id` int(11) NOT NULL,
  `conditions` varchar(20) NOT NULL,
  `condition_name` varchar(20) NOT NULL DEFAULT '-'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `conditions`
--

INSERT INTO `conditions` (`condition_id`, `conditions`, `condition_name`) VALUES
(1, 'NEW', 'condNew'),
(2, 'Used/Mind Good', 'condGood'),
(3, 'Used Scratches Dents', 'condWorking'),
(4, 'BROKEN', 'condBroken'),
(5, 'Scraps', 'scrap');

-- --------------------------------------------------------

--
-- Table structure for table `ltt_users`
--

CREATE TABLE `ltt_users` (
  `u_id` int(11) NOT NULL,
  `fname` varchar(50) NOT NULL DEFAULT 'NO NAME',
  `lname` varchar(50) NOT NULL DEFAULT 'NO NAME',
  `phone` varchar(25) NOT NULL DEFAULT 'NO PHONE',
  `email` varchar(30) NOT NULL DEFAULT 'NO EMAIL',
  `address` varchar(300) NOT NULL DEFAULT '-',
  `postal_code` varchar(40) NOT NULL DEFAULT '-',
  `u_pass` varchar(200) NOT NULL DEFAULT '-',
  `isActive` bit(1) NOT NULL DEFAULT b'1',
  `isSystemAdmin` bit(1) NOT NULL DEFAULT b'0',
  `TStamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ltt_users`
--

INSERT INTO `ltt_users` (`u_id`, `fname`, `lname`, `phone`, `email`, `address`, `postal_code`, `u_pass`, `isActive`, `isSystemAdmin`, `TStamp`) VALUES
(1, 'GUEST', 'Visitor', 'NO PHONE', 'NO EMAIL', '-', '-', '-', b'1', b'0', '2020-08-04 13:02:49');

-- --------------------------------------------------------

--
-- Table structure for table `meta_data_description`
--

CREATE TABLE `meta_data_description` (
  `meta_desc_id` int(11) NOT NULL,
  `dynamic_page` varchar(20) NOT NULL DEFAULT 'index',
  `page_title` varchar(200) NOT NULL DEFAULT 'LONDONTT',
  `page_description` varchar(200) NOT NULL DEFAULT '-'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `meta_data_description`
--

INSERT INTO `meta_data_description` (`meta_desc_id`, `dynamic_page`, `page_title`, `page_description`) VALUES
(1, 'index', 'LONDONTT', 'abc'),
(2, 'home', 'LONDONTT', 'abc');

-- --------------------------------------------------------

--
-- Table structure for table `networks`
--

CREATE TABLE `networks` (
  `network_id` int(11) NOT NULL,
  `network_name` varchar(20) NOT NULL,
  `network_img` varchar(250) NOT NULL DEFAULT '-'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `networks`
--

INSERT INTO `networks` (`network_id`, `network_name`, `network_img`) VALUES
(1, 'O2', '/assets/images/carrier/O2.png'),
(2, 'EE', '/assets/images/carrier/EE.png'),
(3, 'Voda Phone', '/assets/images/carrier/Vodafone.png'),
(4, 'Orange', '/assets/images/carrier/Orange.png'),
(5, 'T Mobile', '/assets/images/carrier/T-Mobile.png'),
(6, 'Three', '/assets/images/carrier/3.png'),
(7, 'Virgin Mobile', '/assets/images/carrier/Virgin.png'),
(8, 'Tesco', '/assets/images/carrier/Tesco.png'),
(9, 'Unknown', '/assets/images/carrier/Unknown.png'),
(10, 'Unlocked', '/assets/images/carrier/Unlocked.png');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `recycle_desc_template_id` int(11) NOT NULL,
  `brandname` varchar(20) NOT NULL DEFAULT '-',
  `product_type` varchar(250) NOT NULL DEFAULT '-',
  `modelname` varchar(250) NOT NULL DEFAULT '-',
  `image_one` varchar(250) DEFAULT '-',
  `image_two` varchar(250) DEFAULT '-',
  `isActive_recycle` bit(1) NOT NULL DEFAULT b'1',
  `isActive_repair` bit(1) NOT NULL DEFAULT b'1',
  `isFavorite_recycle` bit(1) NOT NULL DEFAULT b'1',
  `isFavorite_repair` bit(1) NOT NULL DEFAULT b'1',
  `displayPriority` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `recycle_desc_template_id`, `brandname`, `product_type`, `modelname`, `image_one`, `image_two`, `isActive_recycle`, `isActive_repair`, `isFavorite_recycle`, `isFavorite_repair`, `displayPriority`) VALUES
(335, 1, 'Apple', 'Smartphone/Phone', 'iPhone 6s', '-', '-', b'1', b'1', b'1', b'1', 1),
(336, 1, 'Apple', 'Smartphone/Phone', 'iPhone 6s Plus', '-', '-', b'1', b'1', b'1', b'1', 1),
(337, 1, 'Apple', 'Smartphone/Phone', 'iPhone SE 2020', '-', '-', b'1', b'1', b'1', b'1', 1),
(338, 1, 'Apple', 'Smartphone/Phone', 'iPhone 7', '-', '-', b'1', b'1', b'1', b'1', 1),
(339, 1, 'Apple', 'Smartphone/Phone', 'iPhone 7 Plus', '-', '-', b'1', b'1', b'1', b'1', 1),
(340, 1, 'Apple', 'Smartphone/Phone', 'iPhone 8', '-', '-', b'1', b'1', b'1', b'1', 1),
(341, 1, 'Apple', 'Smartphone/Phone', 'iPhone 8 Plus', '-', '-', b'1', b'1', b'1', b'1', 1),
(342, 1, 'Apple', 'Smartphone/Phone', 'iPhone X', '-', '-', b'1', b'1', b'1', b'1', 1),
(343, 1, 'Apple', 'Smartphone/Phone', 'iPhone XR', '-', '-', b'1', b'1', b'1', b'1', 1),
(344, 1, 'Apple', 'Smartphone/Phone', 'iPhone XS', '-', '-', b'1', b'1', b'1', b'1', 1),
(345, 1, 'Apple', 'Smartphone/Phone', 'iPhone XS Max', '-', '-', b'1', b'1', b'1', b'1', 1),
(346, 1, 'Apple', 'Smartphone/Phone', 'iPhone 11', '-', '-', b'1', b'1', b'1', b'1', 1),
(347, 1, 'Apple', 'Smartphone/Phone', 'iPhone 11 Pro', '-', '-', b'1', b'1', b'1', b'1', 1),
(348, 1, 'Apple', 'Smartphone/Phone', 'iPhone 11 Pro Max', '-', '-', b'1', b'1', b'1', b'1', 1),
(352, 1, 'Apple', 'Smartphone/Phone', 'iPhone 6', '-', '-', b'1', b'1', b'1', b'1', 1),
(353, 1, 'Apple', 'Smartphone/Phone', 'iPhone 6 Plus', '-', '-', b'1', b'1', b'1', b'1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_capacity`
--

CREATE TABLE `product_capacity` (
  `specific_product_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT 0,
  `capacity` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_capacity`
--

INSERT INTO `product_capacity` (`specific_product_id`, `product_id`, `capacity`) VALUES
(8, 335, 16),
(9, 335, 64),
(10, 335, 128),
(11, 336, 16),
(12, 336, 64),
(13, 336, 128),
(14, 338, 32),
(15, 338, 128),
(16, 338, 256),
(17, 339, 32),
(18, 339, 128),
(19, 339, 256),
(20, 340, 64),
(21, 340, 256),
(22, 341, 64),
(23, 341, 256),
(24, 342, 64),
(25, 342, 256),
(26, 343, 64),
(27, 343, 128),
(28, 343, 256),
(29, 344, 64),
(30, 344, 256),
(31, 344, 512),
(32, 345, 64),
(33, 345, 256),
(34, 345, 512),
(35, 346, 64),
(36, 346, 128),
(37, 346, 256),
(38, 347, 64),
(39, 347, 256),
(40, 347, 512),
(41, 348, 64),
(42, 348, 256),
(43, 348, 512),
(2, 352, 16),
(3, 352, 64),
(4, 352, 128),
(5, 353, 16),
(6, 353, 64),
(7, 353, 128);

-- --------------------------------------------------------

--
-- Table structure for table `product_description_template`
--

CREATE TABLE `product_description_template` (
  `template_id` int(11) NOT NULL,
  `template_name` varchar(100) NOT NULL DEFAULT '-',
  `applied_for` varchar(300) NOT NULL DEFAULT '-'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_description_template`
--

INSERT INTO `product_description_template` (`template_id`, `template_name`, `applied_for`) VALUES
(1, 'IPHONE V1', 'almost all iphones later describe'),
(2, 'Samsung V1', 'almost all samsung. later descrbe in details');

-- --------------------------------------------------------

--
-- Table structure for table `recyleapp_price`
--

CREATE TABLE `recyleapp_price` (
  `id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `capacity_id` int(11) NOT NULL,
  `condition_id` int(11) NOT NULL,
  `network_id` int(11) NOT NULL,
  `Offer_Price` decimal(12,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `recyleapp_price`
--

INSERT INTO `recyleapp_price` (`id`, `prod_id`, `capacity_id`, `condition_id`, `network_id`, `Offer_Price`) VALUES
(9, 342, 24, 1, 3, '300.00'),
(10, 342, 24, 2, 3, '240.00'),
(12, 342, 24, 2, 9, '200.00'),
(13, 342, 24, 2, 10, '320.00'),
(15, 342, 24, 2, 4, '200.00'),
(16, 342, 24, 3, 3, '240.00'),
(17, 342, 24, 4, 3, '240.00');

-- --------------------------------------------------------

--
-- Table structure for table `template_description`
--

CREATE TABLE `template_description` (
  `template_id` int(11) NOT NULL,
  `condition_id` int(11) NOT NULL,
  `description1` varchar(500) NOT NULL DEFAULT '-',
  `description2` varchar(500) NOT NULL DEFAULT '-'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `template_description`
--

INSERT INTO `template_description` (`template_id`, `condition_id`, `description1`, `description2`) VALUES
(1, 1, 'NEW CONDITION<br><ul><li>Product with Box and charger</li><li>No Minor or Major dents</li></ul>', 'new desc'),
(1, 2, 'Used/Mind Good', 'Used/Mind Good'),
(1, 3, 'Used Scratches Dents', 'Used Scratches Dents'),
(1, 4, 'Broken', 'Broken'),
(1, 5, 'Scraps', 'Scraps');

-- --------------------------------------------------------

--
-- Table structure for table `template_description_bind`
--

CREATE TABLE `template_description_bind` (
  `template_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `template_description_bind`
--

INSERT INTO `template_description_bind` (`template_id`, `prod_id`) VALUES
(1, 335),
(1, 336),
(1, 337),
(1, 338),
(1, 339),
(1, 340),
(1, 341),
(1, 342),
(1, 343),
(1, 344),
(1, 345),
(1, 346),
(1, 347),
(1, 348),
(1, 352),
(1, 353);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`color_id`),
  ADD UNIQUE KEY `color_name` (`color_name`);

--
-- Indexes for table `color_productbind`
--
ALTER TABLE `color_productbind`
  ADD PRIMARY KEY (`color_bindid`),
  ADD KEY `FK_color_id` (`color_id`),
  ADD KEY `FK_prod_id` (`product_id`);

--
-- Indexes for table `conditions`
--
ALTER TABLE `conditions`
  ADD PRIMARY KEY (`condition_id`),
  ADD UNIQUE KEY `conditions` (`conditions`),
  ADD UNIQUE KEY `condition_name` (`condition_name`);

--
-- Indexes for table `ltt_users`
--
ALTER TABLE `ltt_users`
  ADD PRIMARY KEY (`u_id`),
  ADD UNIQUE KEY `UQEMAIL` (`email`);

--
-- Indexes for table `meta_data_description`
--
ALTER TABLE `meta_data_description`
  ADD PRIMARY KEY (`meta_desc_id`),
  ADD UNIQUE KEY `UQDPage` (`dynamic_page`);

--
-- Indexes for table `networks`
--
ALTER TABLE `networks`
  ADD PRIMARY KEY (`network_id`),
  ADD UNIQUE KEY `network_name` (`network_name`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD UNIQUE KEY `modelname` (`modelname`),
  ADD KEY `FK_prod_desc_temp_id` (`recycle_desc_template_id`);

--
-- Indexes for table `product_capacity`
--
ALTER TABLE `product_capacity`
  ADD PRIMARY KEY (`specific_product_id`),
  ADD UNIQUE KEY `product_id` (`product_id`,`capacity`);

--
-- Indexes for table `product_description_template`
--
ALTER TABLE `product_description_template`
  ADD PRIMARY KEY (`template_id`),
  ADD UNIQUE KEY `template_name` (`template_name`);

--
-- Indexes for table `recyleapp_price`
--
ALTER TABLE `recyleapp_price`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `capacity_id` (`capacity_id`,`condition_id`,`network_id`) USING BTREE,
  ADD KEY `condition_ID_CONSTRAINT` (`condition_id`),
  ADD KEY `network_ID_CONSTRAINT` (`network_id`),
  ADD KEY `FKproductid` (`prod_id`);

--
-- Indexes for table `template_description`
--
ALTER TABLE `template_description`
  ADD UNIQUE KEY `template_id` (`template_id`,`condition_id`),
  ADD UNIQUE KEY `UQ_tempcond_id` (`template_id`,`condition_id`) USING BTREE,
  ADD KEY `FK_cond_id` (`condition_id`);

--
-- Indexes for table `template_description_bind`
--
ALTER TABLE `template_description_bind`
  ADD UNIQUE KEY `UQ_template_idprodid` (`template_id`,`prod_id`) USING BTREE,
  ADD KEY `FK_prod_bind_id` (`prod_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `color_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `color_productbind`
--
ALTER TABLE `color_productbind`
  MODIFY `color_bindid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=354;

--
-- AUTO_INCREMENT for table `conditions`
--
ALTER TABLE `conditions`
  MODIFY `condition_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ltt_users`
--
ALTER TABLE `ltt_users`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `meta_data_description`
--
ALTER TABLE `meta_data_description`
  MODIFY `meta_desc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `networks`
--
ALTER TABLE `networks`
  MODIFY `network_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=354;

--
-- AUTO_INCREMENT for table `product_capacity`
--
ALTER TABLE `product_capacity`
  MODIFY `specific_product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `product_description_template`
--
ALTER TABLE `product_description_template`
  MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `recyleapp_price`
--
ALTER TABLE `recyleapp_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `color_productbind`
--
ALTER TABLE `color_productbind`
  ADD CONSTRAINT `FK_color_id` FOREIGN KEY (`color_id`) REFERENCES `color` (`color_id`),
  ADD CONSTRAINT `FK_prod_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `FK_prod_desc_temp_id` FOREIGN KEY (`recycle_desc_template_id`) REFERENCES `product_description_template` (`template_id`);

--
-- Constraints for table `product_capacity`
--
ALTER TABLE `product_capacity`
  ADD CONSTRAINT `FK_prod_id_cap` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`);

--
-- Constraints for table `recyleapp_price`
--
ALTER TABLE `recyleapp_price`
  ADD CONSTRAINT `FKproductid` FOREIGN KEY (`prod_id`) REFERENCES `products` (`product_id`),
  ADD CONSTRAINT `capacity_ID_CONSTRAINT` FOREIGN KEY (`capacity_id`) REFERENCES `product_capacity` (`specific_product_id`),
  ADD CONSTRAINT `condition_ID_CONSTRAINT` FOREIGN KEY (`condition_id`) REFERENCES `conditions` (`condition_id`),
  ADD CONSTRAINT `network_ID_CONSTRAINT` FOREIGN KEY (`network_id`) REFERENCES `networks` (`network_id`);

--
-- Constraints for table `template_description`
--
ALTER TABLE `template_description`
  ADD CONSTRAINT `FK_cond_id` FOREIGN KEY (`condition_id`) REFERENCES `conditions` (`condition_id`),
  ADD CONSTRAINT `FK_temp_id` FOREIGN KEY (`template_id`) REFERENCES `product_description_template` (`template_id`);

--
-- Constraints for table `template_description_bind`
--
ALTER TABLE `template_description_bind`
  ADD CONSTRAINT `FK_prod_bind_id` FOREIGN KEY (`prod_id`) REFERENCES `products` (`product_id`),
  ADD CONSTRAINT `FK_templ_id` FOREIGN KEY (`template_id`) REFERENCES `product_description_template` (`template_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
